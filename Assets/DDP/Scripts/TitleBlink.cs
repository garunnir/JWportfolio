﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleBlink : MonoBehaviour
{
    public GameObject titleText;
    // Start is called before the first frame update
    void Start()
    {
        titleText.SetActive(false);
        StartCoroutine(BlinkText());
    }

    IEnumerator BlinkText()
    {
        int count = 0;
        while (count < 5)
        {
            titleText.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            titleText.SetActive(false);
            yield return new WaitForSeconds(0.2f);
            count++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
