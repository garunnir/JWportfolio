﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadManager : MonoBehaviour
{
    public GameObject titleText;
    public Slider loadingBar;
    public int maxLoadingTime = 4;
    float currentTime = 0f;
    public GameObject loadText;
    bool isDone;
    AsyncOperation asyncOp;

    // Start is called before the first frame update
    void Start()
    {
        loadingBar.maxValue = maxLoadingTime;
        titleText.SetActive(false);
        loadText.SetActive(false);
        StartCoroutine(BlinkText());
        StartCoroutine(LoadScene("EnterderR"));
    }
    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        loadingBar.value = currentTime;

        if(currentTime >= loadingBar.maxValue)
        {
            titleText.SetActive(false);
            loadText.SetActive(true);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                asyncOp.allowSceneActivation = true;
            }         
        }
    }

    IEnumerator BlinkText()
    {
        while (currentTime < loadingBar.maxValue)
        {
            titleText.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            titleText.SetActive(false);
            yield return new WaitForSeconds(0.2f);
        }
    }

    IEnumerator LoadScene(string sceneName)
    {
        asyncOp = SceneManager.LoadSceneAsync(sceneName);
        asyncOp.allowSceneActivation = false;

        if(isDone == false)
        {
            isDone = true;
            while(asyncOp.progress < 0.9f)
            {
                loadingBar.value = asyncOp.progress;

                yield return true;
            }
        }
    }
}
