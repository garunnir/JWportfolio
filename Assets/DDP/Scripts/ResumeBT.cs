﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResumeBT : MonoBehaviour
{
    Button resumeBT;
    JW_PlayerAction player;

    void Start()
    {
        resumeBT = this.transform.GetComponent<Button>();
        resumeBT.onClick.AddListener(rClick);
        player = GameObject.Find("Player").GetComponent<JW_PlayerAction>();
        
    }

    void rClick()
    {
        player.isPause = false;
    }
}
