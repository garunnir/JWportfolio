﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkManager : MonoBehaviour
{
    Dictionary<int, string[]> talkData;
    // Start is called before the first frame update
    void Awake()
    {
        talkData = new Dictionary<int, string[]>();
        GenerateData();
    }

    void GenerateData()
    {
        // Id = 500이면 보물상자
        talkData.Add(500, new string[] { "보물 상자로 보인다.", "열쇠 없이는 열 수 없는 것 같다."});
        // Id = 1000이면 컴퓨터 대화
        talkData.Add(1000, new string[] { "마우스 우클릭으로 회피 동작을 취할 수 있습니다.", "컴퓨터에서 E를 누르면 Tip을 볼 수 있습니다.", "적을 해치우고 얻는 에너지는 상점에서 화폐로 기능합니다."});
        // Id = 1500이면 상점주인 대화
        talkData.Add(1500, new string[] { "어서 오게. 좋은 물건들이 많이 있으니 한번 골라보시게." });
    }

    public string GetTalk(int id, int talkIndex)
    {
        if (talkIndex == talkData[id].Length)
            return null;
        else       
            return talkData[id][talkIndex];
    }
}
