﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Desk : MonoBehaviour
{
    public bool deskSwitch = false;
    public int deskHp;
    public Vector3 deskPos;
    // Start is called before the first frame update
    void Start()
    {
        deskPos = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(deskSwitch == true)
        {
            deskPos.y += 0.5f;
            transform.rotation = Quaternion.Euler(90,0,0);
            deskHp = 5;
        }
    }

    // 총알에 맞을 때마다 책상의 체력이 1씩 감소, 0이 되면 파괴
    private void OnCollisionEnter(Collision collision)
    {
        if(deskSwitch == true && deskHp >= 5)
        {
            if (collision.gameObject.tag == "PlayerBullet" || collision.gameObject.tag == "EnemyBullet")
            {
                deskHp -= 1;
            }
            if (deskHp == 0)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
