﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{
    // 바위 체력
    public int rockHp = 10;

    // 총알에 맞을 때마다 바위의 체력이 1씩 감소, 0이 되면 파괴
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "PlayerBullet" || collision.gameObject.tag == "EnemyBullet")
        {
            rockHp -= 1;
        }
        if(rockHp == 0)
        {
            Destroy(gameObject);
        }
    }
}
