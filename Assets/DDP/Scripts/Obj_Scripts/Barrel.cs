﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnCollisionEnter(Collision collision)
    {
        // 플레이어, 혹은 총알에 닿으면 파괴됨.
        if(collision.gameObject.tag == "Player" || collision.gameObject.tag == "PlayerBullet" || collision.gameObject.tag == "EnemyBullet")
        {
            Destroy(gameObject);
        }
    }
}
