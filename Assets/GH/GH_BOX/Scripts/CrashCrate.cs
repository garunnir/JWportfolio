﻿namespace ArionDigital
{
    using UnityEngine;

    public class CrashCrate : MonoBehaviour
    {
        [Header("Whole Create")]
        public MeshRenderer wholeCrate;
        public BoxCollider boxCollider;
        [Header("Fractured Create")]
        public GameObject fracturedCrate;
        [Header("Audio")]
        public AudioSource crashAudioClip;
        public int count;
        Rigidbody[] rbs;
        MeshCollider[] meshs;
        CrashCrate crashcrate;

        private void Start()
        {
            crashcrate = gameObject.GetComponent<CrashCrate>();
            rbs = gameObject.transform.GetChild(0).gameObject.GetComponentsInChildren<Rigidbody>();
            meshs = gameObject.transform.GetChild(0).gameObject.GetComponentsInChildren<MeshCollider>();
        }

        private void Update()
        {

        }

        void BreakBox()
        {
            wholeCrate.enabled = false;
            boxCollider.enabled = false;
            fracturedCrate.SetActive(true);
            crashAudioClip.Play();
            Invoke("RbsAndMeshcontroller", 3f);
            crashcrate.enabled = false;
        }

        void RbsAndMeshcontroller()
        {
            for (int i = 0; i < rbs.Length; i++)
            {
                rbs[i].isKinematic = true;
                meshs[i].isTrigger = true;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            BreakBox();
        }

        [ContextMenu("Test")]
        public void Test()
        {
            wholeCrate.enabled = false;
            boxCollider.enabled = false;
            fracturedCrate.SetActive(true);
        }
    }
}