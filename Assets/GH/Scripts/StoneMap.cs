﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneMap : MapEnemyCreator
{
    public bool clearCheck = false;
    public bool playerRoomComeIn = false;
    Transform[] childEnemy;
    public List<GameObject> stoneWoodObjects;
    public EnemyPrefabsManager StoneMapManager;
    public List<GameObject> allobj =new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        Vector3 size = gameObject.GetComponent<MeshRenderer>().bounds.size;
        Vector3 center = gameObject.GetComponent<MeshRenderer>().bounds.center;
        StoneMapManager = GameObject.Find("MapManager").GetComponent<EnemyPrefabsManager>();
        stoneWoodObjects = new List<GameObject>(StoneMapManager.woodObj);
        gameObject.tag = "Stone";
        LightCreator();
        ObjectSetting();
        EnemyCreator(allobj);
        childEnemy = new Transform[enemys.Count];
    }

    public void ObjectSetting()
    {
        Vector3 size = gameObject.GetComponent<MeshRenderer>().bounds.size;
        Vector3 center = gameObject.GetComponent<MeshRenderer>().bounds.center;


        RoomobjSetting(stoneWoodObjects, 1, 3, 7, 1, 1, 0, 0);
        if (size.x < size.z && size.z <= 20 && size.x <=20)
        {
           allobj.AddRange(RoomobjSetting(stoneWoodObjects[0], 5f, center + new Vector3(4, 0.25f, -5), 1, Quaternion.Euler(0,0,-90), 0, 1));
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[4], 7f, center + new Vector3(0,0,2), 1, Quaternion.identity, 0, 1, 0, 1));
        }
        else if (size.x > size.z && size.x <= 20 && size.z <=20)
        {
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[0], 5f, center + new Vector3(4, 0.25f, -5), 1, Quaternion.Euler(0, 0, -90), 0, 1));
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[4], 7f, center + new Vector3(2, 0, 0), 1, Quaternion.identity, 0, 1, 0, 1));
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[4], 7f, center + new Vector3(-2, 0, -6), 1, Quaternion.identity, 0, 1, 0, 1));
        }
        else if (size.x == size.z && size.x >= 15)
        {
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[0], 5f, center + new Vector3(4, 0.25f, -5), 1, Quaternion.Euler(0, 0, -90), 0, 1));
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[4], 7f, center + new Vector3(2, 0, 0), 1, Quaternion.identity, 0, 1, 0, 1));
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[4], 7f, center + new Vector3(-2, 0, -6), 1, Quaternion.identity, 0, 1, 0, 1));
        }
        else if (size.x == size.z && size.x < 15)
        {
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[0], 5f, center + new Vector3(4, 0.25f, -5), 1, Quaternion.Euler(0, 0, -90), 0, 1));
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[4], 7f, center + new Vector3(2, 0, 0), 1, Quaternion.identity, 0, 1, 0, 1));
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[4], 7f, center + new Vector3(-2, 0, -6), 1, Quaternion.identity, 0, 1, 0, 1));
        }
        else if (size.x < size.z && size.z > 20 && size.x <= 20)
        {
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[0], 5f, center + new Vector3(4, 0.25f, -5), 1, Quaternion.Euler(0, 0, -90), 0, 1));
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[4], 7f, center + new Vector3(0, 0, 2), 1, Quaternion.identity, 0, 1, 0, 1));
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[4], 7f, center + new Vector3(-2, 0, -6), 1, Quaternion.identity, 0, 1, 0, 1));
        }
        else if (size.x > size.z && size.x > 20 && size.z <= 20)
        {
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[0], 5f, center + new Vector3(4, 0.25f, -5), 1, Quaternion.Euler(0, 0, -90), 0, 1));
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[4], 7f, center + new Vector3(2, 0, 0), 1, Quaternion.identity, 0, 1, 0, 1));
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[4], 7f, center + new Vector3(-2, 0, -6), 1, Quaternion.identity, 0, 1, 0, 1));
        }
        else if (size.x > size.z && size.x > 20 && size.z > 20)
        {
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[0], 5f, center + new Vector3(4, 0.25f, -5), 1, Quaternion.Euler(0, 0, -90), 0, 1));
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[4], 7f, center + new Vector3(2, 0, 0), 1, Quaternion.identity, 0, 1, 0, 1));
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[4], 7f, center + new Vector3(-2, 0, -6), 1, Quaternion.identity, 0, 1, 0, 1));
        }
        else if(size.x < size.z && size.z > 20 && size.x > 20)
        {
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[0], 5f, center + new Vector3(4, 0.25f, -5), 1, Quaternion.Euler(0, 0, -90), 0, 1));
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[4], 7f, center + new Vector3(0, 0, 2), 1, Quaternion.identity, 0, 1, 0, 1));
            allobj.AddRange(RoomobjSetting(stoneWoodObjects[4], 7f, center + new Vector3(-2, 0, -6), 1, Quaternion.identity, 0, 1, 0, 1));
        }
        else
        {
            print("예외경우 발생");
        }
    }

    void Update()
    {
        if (gameObject.transform.childCount == 0)
        {
            EnemyPrefabsManager.doorCtr = false;
            GameObject clearobj = new GameObject("Clear");
            clearobj.transform.parent = gameObject.transform;
        }
    }

    /*private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerRoomComeIn = true;
            if (playerRoomComeIn == true && clearCheck == false)
            {
                Invoke("EnemyComing", 1f);
            }
        }
    }*/
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerRoomComeIn = true;
            if (playerRoomComeIn == true && clearCheck == false)
            {
                Invoke("EnemyComing", 1f);
            }
        }
    }

    void EnemyComing()
    {
        for (int i = 0; i < childEnemy.Length; i++)
        {
            childEnemy[i] = gameObject.transform.GetChild(i).transform;
            childEnemy[i].gameObject.SetActive(true);
        }
        EnemyPrefabsManager.battleBegins = true;
        clearCheck = true;
    }
}
