﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallScripts : MonoBehaviour
{
    public Transform Target;
    public float dist;
    private string[] tags = { "Boss", "Room", "Shop", "Spon" };
    GameObject[] taggedWalls = { };
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Room")
        {
            StartCoroutine("ColRoom");
        }
        if (collision.gameObject.tag == "Shop")
        {

        }
        if (collision.gameObject.tag == "Spon")
        {

        }
        if (collision.gameObject.tag == "Hidden")
        {

        }
        if (collision.gameObject.tag == "Stone")
        {

        }
        if (collision.gameObject.tag == "Treasure")
        {

        }
    }

    IEnumerator ColRoom()
    {
        yield return null;
    }
    IEnumerator ColShop()
    {
        yield return null;
    }
    IEnumerator ColSpon()
    {
        yield return null;
    }
    IEnumerator ColHidden()
    {
        yield return null;
    }
    IEnumerator ColStone()
    {
        yield return null;
    }
    IEnumerator ColTreasure()
    {
        yield return null;
    }
}
