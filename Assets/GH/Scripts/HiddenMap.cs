﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenMap : MapEnemyCreator
{
    public bool clearCheck = false;
    public bool playerRoomComeIn = false;
    Transform[] childEnemy;
    public List<GameObject> graobjects;
    EnemyPrefabsManager hiddenManager;
    public List<GameObject> PillarObj;
    public List<GameObject> carpetObj;
    public List<GameObject> allobj = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        hiddenManager = GameObject.Find("MapManager").GetComponent<EnemyPrefabsManager>();
        graobjects = new List<GameObject>(hiddenManager.graveObj);
        PillarObj = new List<GameObject>(hiddenManager.PillarObj);
        carpetObj = new List<GameObject>(hiddenManager.carpetObj);
        
        ObjectSetting();
        gameObject.tag = "Hidden";
        LightCreator();
        EnemyCreator(allobj);
        childEnemy = new Transform[enemys.Count];
    }

    public void ObjectSetting()
    {
        Vector3 size = gameObject.GetComponent<MeshRenderer>().bounds.size;
        Vector3 center = gameObject.GetComponent<MeshRenderer>().bounds.center;

        //print(gameObject.name.ToString() + ":" + size);


        if (size.x < size.z && size.z <= 20 && size.x <= 20)
        {
            allobj.AddRange(RoomobjSetting(graobjects[3], 4.5f, center + new Vector3(-4f, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(-4f, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(graobjects[2], 4.5f, center + new Vector3(4f, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(4f, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            //allobj.AddRange(RoomobjSetting(PillarObj[0], 4.5f, center + new Vector3(0, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            //print("생성됨");
        }
        else if (size.x > size.z && size.x <= 20 && size.z <= 20)
        {
            allobj.AddRange(RoomobjSetting(graobjects[2], 4.5f, center + new Vector3(-4f, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(graobjects[3], 4.5f, center + new Vector3(4f, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(-4f, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(4f, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            //allobj.AddRange(RoomobjSetting(PillarObj[0], 4.5f, center + new Vector3(0, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            //print("생성됨1");
        }
        else if (size.x == size.z && size.x >= 15)
        {
            allobj.AddRange(RoomobjSetting(graobjects[2], 4.5f, center + new Vector3(-4, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(graobjects[3], 4.5f, center + new Vector3(4, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(-4, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(4, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            //allobj.AddRange(RoomobjSetting(PillarObj[0], 4.5f, center + new Vector3(0, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            //print("생성됨2");
        }
        else if (size.x == size.z && size.x < 15)
        {
            allobj.AddRange(RoomobjSetting(graobjects[3], 4.5f, center + new Vector3(-4, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(graobjects[3], 4.5f, center + new Vector3(4, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(-4, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(4, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            //allobj.AddRange(RoomobjSetting(PillarObj[0], 4.5f, center + new Vector3(0, 0, -4.5f), 3, Quaternion.identity, 0, 1));
            //print("생성됨3");
        }
        else if (size.x < size.z && size.z > 20 && size.x <= 20)
        {
            allobj.AddRange(RoomobjSetting(graobjects[2], 4.5f, center + new Vector3(-4f, 0, -6f), 4, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(graobjects[2], 4.5f, center + new Vector3(4f, 0, -6f), 4, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(-4f, 0, -6f), 4, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(4f, 0, -6f), 4, Quaternion.identity, 0, 1));
            //allobj.AddRange(RoomobjSetting(PillarObj[0], 4.5f, center + new Vector3(0, 0, -6f), 4, Quaternion.identity, 0, 1));
            //print("생성됨4");
        }
        else if (size.x > size.z && size.x > 20 && size.z <= 20)
        {
            allobj.AddRange(RoomobjSetting(graobjects[3], 4.5f, center + new Vector3(-6, 0, -4f), 4, Quaternion.identity, 1, 0));
            allobj.AddRange(RoomobjSetting(graobjects[2], 4.5f, center + new Vector3(-6, 0, 4f), 4, Quaternion.identity, 1, 0));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(-6, 0, -4f), 4, Quaternion.identity, 1, 0));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(-6, 0, 4f), 4, Quaternion.identity, 1, 0));
            //allobj.AddRange(RoomobjSetting(PillarObj[0], 4.5f, center + new Vector3(-6, 0, 0f), 4, Quaternion.identity, 1, 0));
            //print("생성됨5");
        }
        else if (size.x > size.z && size.x > 20 && size.z > 20)
        {
            allobj.AddRange(RoomobjSetting(graobjects[2], 4.5f, center + new Vector3(-4, 0, -6f), 4, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(graobjects[3], 4.5f, center + new Vector3(4, 0, -6f), 4, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(-4, 0, -6f), 4, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(4, 0, -6f), 4, Quaternion.identity, 0, 1));
            //allobj.AddRange(RoomobjSetting(PillarObj[0], 4.5f, center + new Vector3(0, 0, -6), 4, Quaternion.identity, 0, 1));
            //print("생성됨6");
        }
        else if (size.x < size.z && size.z > 20 && size.x > 20)
        {
            allobj.AddRange(RoomobjSetting(graobjects[3], 4.5f, center + new Vector3(-4, 0, -6f), 4, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(graobjects[2], 4.5f, center + new Vector3(4, 0, -6f), 4, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(-4, 0, -6f), 4, Quaternion.identity, 0, 1));
            allobj.AddRange(RoomobjSetting(carpetObj[0], 4.5f, center + new Vector3(4, 0, -6f), 4, Quaternion.identity, 0, 1));
            //allobj.AddRange(RoomobjSetting(PillarObj[0], 4.5f, center + new Vector3(0, 0, -6), 4, Quaternion.identity, 0, 1));
            //print("생성됨7");
        }
        else
        {
            //print("예외경우 발생");
        }
    }

    void Update()
    {
        if (gameObject.transform.childCount == 0)
        {
            EnemyPrefabsManager.doorCtr = false;
            GameObject clearobj = new GameObject("Clear");
            clearobj.transform.parent = gameObject.transform;
        }
    }
    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.tag == "Player")
    //    {
    //        playerRoomComeIn = true;
    //        if (playerRoomComeIn == true && clearCheck == false)
    //        {
    //            Invoke("EnemyComing", 1f);
    //        }
    //    }
    //}
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerRoomComeIn = true;
            if (playerRoomComeIn == true && clearCheck == false)
            {
                Invoke("EnemyComing", 1f);
            }
        }
    }
    void EnemyComing()
    {
        for (int i = 0; i < childEnemy.Length; i++)
        {
            childEnemy[i] = gameObject.transform.GetChild(i).transform;
            childEnemy[i].gameObject.SetActive(true);
        }
        EnemyPrefabsManager.battleBegins = true;
        clearCheck = true;
    }
}
