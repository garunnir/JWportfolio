﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class RoomMap : MapEnemyCreator
{
    public bool clearCheck = false;
    public bool playerRoomComeIn = false;
    public Transform[] childEnemy;
    EnemyPrefabsManager roomMapmanager;
    public List<GameObject> objects;
    public List<GameObject> pillarobjs;
    public List<GameObject> carpetobjs;
    public List<GameObject> allObj = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        Vector3 size = gameObject.GetComponent<MeshRenderer>().bounds.size;
        Vector3 center = gameObject.GetComponent<MeshRenderer>().bounds.center;
        gameObject.AddComponent<MeshCollider>();
        roomMapmanager = GameObject.Find("MapManager").GetComponent<EnemyPrefabsManager>();
        objects = new List<GameObject>(roomMapmanager.libraryObj);
        pillarobjs = new List<GameObject>(roomMapmanager.PillarObj);
        carpetobjs = new List<GameObject>(roomMapmanager.carpetObj);
        gameObject.GetComponent<BoxCollider>().size += Vector3.up;
        gameObject.GetComponent<BoxCollider>().isTrigger = true;
        gameObject.tag = "Room";
        LightCreator();
        //print(gameObject.name.ToString() + ":" + size);
        ObjectSetting();
        EnemyCreator(allObj);
        childEnemy = new Transform[gameObject.transform.childCount];
    }

    public void ObjectSetting()
    {
        Vector3 size = gameObject.GetComponent<MeshRenderer>().bounds.size;
        Vector3 center = gameObject.GetComponent<MeshRenderer>().bounds.center;

       allObj.AddRange(RoomobjSetting(objects, 0, 2, 1, 0, 0, 0, 0));
        allObj.AddRange(RoomobjSetting(objects[2], 10, 0.5f, 0.5f, -8));
        if (size.x < size.z && size.x <= 20 && size.x >= 10 && size.z <= 20 && size.z >= 10)
        {
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 4.5f, center + new Vector3(-4, 0, -5), 3, Quaternion.identity, 0, 1));
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 4.5f, center + new Vector3(4, 0, -5), 3, Quaternion.identity, 0, 1));
            allObj.AddRange(RoomobjSetting(objects, 3, 6,0,2,4,1,1,0,0));
        }
        else if (size.x > size.z && size.x <= 20 && size.x >= 10 && size.z <= 20 && size.z >= 10)
        {
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 4.5f, center + new Vector3(-4, 0, -4), 3, Quaternion.identity, 1, 0));
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 4.5f, center + new Vector3(-4, 0, 4), 3, Quaternion.identity, 1, 0));
            allObj.AddRange(RoomobjSetting(objects, 3, 6, 0, 1.5f, 4, 1, 1, 0, 0));
        }
        else if (size.x == size.z && size.x >= 15 && size.z >= 15)
        {
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 4.5f, center + new Vector3(-4, 0, -4), 3, Quaternion.identity, 1, 0));
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 4.5f, center + new Vector3(-4, 0, 4), 3, Quaternion.identity, 1, 0));
            allObj.AddRange(RoomobjSetting(objects, 3, 6, 0, 1.5f, 4, 1, 1, 0, 0));
        }
        else if (size.x == size.z && size.x <= 15 && size.z <= 15)
        {
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 4.5f, center + new Vector3(-4, 0, -4), 3, Quaternion.identity, 1, 0));
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 4.5f, center + new Vector3(-4, 0, 4), 3, Quaternion.identity, 1, 0));
            allObj.AddRange(RoomobjSetting(objects, 3, 6, 0, 1.5f, 4, 1, 1, 0, 0));
        }
        else if (size.x < size.z && size.z >= 20 && size.x >= 10 && size.x <= 20)
        {
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 5f, center + new Vector3(-4, 0, -8), 4, Quaternion.identity, 0, 1));
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 5f, center + new Vector3(4, 0, -8), 4, Quaternion.identity, 0, 1));
            allObj.AddRange(RoomobjSetting(objects, 3, 6, 0, 1.5f, 4, 1, 1, 0, 0));
        }
        else if (size.x > size.z && size.x >= 20 && size.z >= 10 && size.z <= 20)
        {
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 5f, center + new Vector3(-8, 0, -4), 4, Quaternion.identity, 1, 0));
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 5f, center + new Vector3(-8, 0, 4), 4, Quaternion.identity, 1, 0));
            allObj.AddRange(RoomobjSetting(objects, 3, 6, 0, 1.5f, 4, 1, 1, 0, 0));
        }
        else if (size.x > size.z && size.x >= 20 && size.z >= 20)
        {
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 5f, center + new Vector3(-8, 0, -4), 4, Quaternion.identity, 1, 0));
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 5f, center + new Vector3(-8, 0, 4), 4, Quaternion.identity, 1, 0));
            allObj.AddRange(RoomobjSetting(objects, 3, 6, 0, 1.5f, 4, 1, 1, 0,0));
        }
        else if (size.x < size.z && size.z >= 20 && size.x >= 20)
        {
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 5f, center + new Vector3(-4, 0, -8), 4, Quaternion.identity, 0, 1));
            //allObj.AddRange(RoomobjSetting(pillarobjs[0], 5f, center + new Vector3(4, 0, -8), 4, Quaternion.identity, 0, 1));
            allObj.AddRange(RoomobjSetting(objects, 3, 6, 0, 1.5f, 4, 1,1, 0, 0));
        }
        else
        {
            print("예외경우 발생!");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.childCount == 0)
        {
            EnemyPrefabsManager.doorCtr = false;
            GameObject clearobj = new GameObject("Clear");
            clearobj.transform.parent = gameObject.transform;
        }
    }
    /*
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerRoomComeIn = true;
            if (playerRoomComeIn == true && clearCheck == false)
            {
                Invoke("EnemyComing", 1f);
            }
        }
    }
    */
    private void OnTriggerEnter(Collider other)
    {
        //print("충돌");
        if (other.gameObject.tag == "Player")
        {
            print("플레이어충돌");
            playerRoomComeIn = true;
            if (playerRoomComeIn == true && clearCheck == false)
            {
                Invoke("EnemyComing", 1f);
            }
        }
    }
    void EnemyComing()
    {
        for (int i = 0; i < childEnemy.Length; i++)
        {
            childEnemy[i] = gameObject.transform.GetChild(i).transform;
            childEnemy[i].gameObject.SetActive(true);
        }
        EnemyPrefabsManager.battleBegins = true;
        clearCheck = true;
    }
}
