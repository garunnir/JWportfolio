﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//프리팹 백업
public class EnemyPrefabsManager : MonoBehaviour
{
    public List<GameObject> enemysBackup;
    public Transform[] enemySponPos;
    public Transform bossEnemyBackup;
    public GameObject[] doorObjBackup = new GameObject[2];
    public static bool battleBegins = false;
    public static bool doorCtr = true;
    public GameObject[] objectPrefabs = new GameObject[4];
    public GameObject[] VerticalWall = new GameObject[6];
    public GameObject[] HorizontalWall = new GameObject[6];
    public GameObject pointLight;
    public GameObject[] libraryObj = new GameObject[6];
    public GameObject[] PillarObj = new GameObject[6];
    public GameObject[] carpetObj = new GameObject[2];
    public GameObject[] woodObj = new GameObject[5];
    public GameObject[] graveObj = new GameObject[4];
}
