﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;



public class DungeonCreator : MonoBehaviour
{
    //던전 폭, 던전 길이
    public int dungeonWidth, dungeonLength;
    //방 최소 폭, 방 최소 길이
    public int roomWidthMin, roomLengthMin;
    //최대 반복
    public int maxIterations;
    //복도 길이
    public int corridorWidth;
    //바닥 마테리얼
    public Material material;
    //public Mesh floorMesh;
    //복도 생성
    [Range(0.0f, 0.3f)]
    public float roomBottomCornerModifier;
    [Range(0.7f, 1.0f)]
    public float roomTopCornerMidifier;
    [Range(0, 2)]
    public int roomOffset;
    //벽 생성
    public GameObject wallVertical, wallHorizontal;
    public GameObject[] verticalWalls = new GameObject[8];
    public GameObject[] horizontalWalls = new GameObject[8];
    List<Vector3Int> possibleDoorVerticalPosition;
    List<Vector3Int> possibleDoorHorizontalPosition;
    List<Vector3Int> possibleWallHorizontalPosition;
    List<Vector3Int> possibleWallVerticalPosition;

    public List<GameObject> mapList;

    public List<GameObject> hallwayList;

    public string[] str;

    public GameObject playerPrfab;

    public GameObject[] objPrefabs = new GameObject[9];

    public List<float> distances;

    float max = 0;
    private void Awake()
    {
        CreateDungeon();
    }

    private void Start()
    {
        //playerPrfab = GameObject.Find("Player").gameObject;
        playerPrfab.transform.position = new Vector3(GameObject.Find("Spon").GetComponent<MeshRenderer>().bounds.center.x, 0 , GameObject.Find("Spon").GetComponent<MeshRenderer>().bounds.center.z);
        //print(new Vector3(GameObject.Find("Spon").GetComponent<MeshRenderer>().bounds.center.x, 0.5f, GameObject.Find("Spon").GetComponent<MeshRenderer>().bounds.center.z));
        //print(GameObject.Find("Spon").GetComponent<MeshRenderer>().bounds.center);
    }

    public void MapName2()
    {
        mapList[0].name = str[1];
        mapList[0].AddComponent<SponMap>();
        if (mapList[1].name == "")
        {
            mapList[1].name = str[2];
            mapList[1].AddComponent<ShopMap>();
        }
        else if (mapList[1].name != "")
        {
            mapList[2].name = str[2];
            mapList[2].AddComponent<ShopMap>();
        }
        for (int i =0; i < mapList.Count; i++)
        {
            distances[i] = Vector3.Distance(mapList[0].GetComponent<MeshRenderer>().bounds.center, mapList[i].GetComponent<MeshRenderer>().bounds.center);
        }
        for(int j = 0; j< mapList.Count; j++)
        {
            if(mapList[j].gameObject.name != "")
            {
                continue;
            }
            if(mapList[j].gameObject.name == "")
            {
                if(max < distances[j])
                {
                    max = distances[j];
                    print(j + "의맥스값" + max);
                }
            }
        }
        print("최종 max값 : " + max);
        float index = distances.BinarySearch(max);
        print("index :" + index);
        mapList[(int)index].name = str[0];
        mapList[(int)index].AddComponent<BossMap>();

        for(int i = 0; i<mapList.Count; i++)
        {
            if(mapList[i].name != "")
            {
                continue;
            }
            if(mapList[i].name == "")
            {
                mapList[i].name = str[UnityEngine.Random.Range(3, 7)];
                switch(mapList[i].name)
                {
                    case "Stone":
                        mapList[i].AddComponent<StoneMap>();
                        break;
                    case "Treasure":
                        mapList[i].AddComponent<TreasureMap>();
                        break;
                    case "Hidden":
                        mapList[i].AddComponent<HiddenMap>();
                        break;
                    case "Room":
                        mapList[i].AddComponent<RoomMap>();
                        break;
                }
            }
        }
    }

    public void MapName()
    {
        mapList[0].name = str[0];
        mapList[0].AddComponent<BossMap>();
        mapList[1].name = str[1];
        mapList[1].AddComponent<SponMap>();
        mapList[2].name = str[2];
        mapList[2].AddComponent<ShopMap>();
        mapList[3].name = str[3];
        mapList[3].AddComponent<StoneMap>();
        mapList[4].name = str[4];
        mapList[4].AddComponent<TreasureMap>();
        mapList[5].name = str[5];
        mapList[5].AddComponent<HiddenMap>();
        

        for (int i =0; i<mapList.Count;i++)
        {
            if(mapList[i].name != "")
            {
                continue;
            }
            if (mapList[i].name == "")
            {
                mapList[i].name = str[6] +i.ToString();
                mapList[i].AddComponent<RoomMap>();
            }
        }
    }
    public void CreateDungeon()
    {
        DestroyAllChildren();
        mapList.Clear();
        DugeonGenerator generator = new DugeonGenerator(dungeonWidth, dungeonLength);
        var listOfRooms = generator.CalculateDungeon(maxIterations,
            roomWidthMin,
            roomLengthMin,
            roomBottomCornerModifier,
            roomTopCornerMidifier,
            roomOffset,
            corridorWidth);
        GameObject wallParent = new GameObject("WallParent");
        wallParent.transform.parent = transform;
        possibleDoorVerticalPosition = new List<Vector3Int>();
        possibleDoorHorizontalPosition = new List<Vector3Int>();
        possibleWallHorizontalPosition = new List<Vector3Int>();
        possibleWallVerticalPosition = new List<Vector3Int>();
        for (int i = 0; i < listOfRooms.Count; i++)
        {
            //매쉬생성
            CreateMesh(listOfRooms[i].BottomLeftAreaCorner, listOfRooms[i].TopRightAreaCorner);
        }
        //벽 생성
        CreateWalls(wallParent);
        MapName();
    }

    private void CreateWalls(GameObject wallParent)
    {
        foreach (var wallPosition in possibleWallHorizontalPosition)
        {
            GameObject wall = CreateWall(wallParent, wallPosition, horizontalWalls[0]);
            wall.AddComponent<WallScripts>();
           // wall.AddComponent<Rigidbody>();
           // Rigidbody rg = wall.GetComponent<Rigidbody>();
           // rg.constraints = RigidbodyConstraints.FreezeAll;
            //wall.transform.GetChild(0).GetComponent<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            //wall.transform.GetChild(0).GetComponent<Renderer>().receiveShadows = false;
        }
        foreach (var wallPostion in possibleWallVerticalPosition)
        {
            GameObject wall = CreateWall(wallParent, wallPostion, verticalWalls[0]);
            wall.AddComponent<WallScripts>();
           // wall.AddComponent<Rigidbody>();
           // Rigidbody rg = wall.GetComponent<Rigidbody>();
           // rg.constraints = RigidbodyConstraints.FreezeAll;
            //wall.transform.GetChild(0).GetComponent<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            //wall.transform.GetChild(0).GetComponent<Renderer>().receiveShadows = false;
        }
    }

    private GameObject CreateWall(GameObject wallParent, Vector3Int wallPosition, GameObject wallPrefab)
    {
      GameObject wall = Instantiate(wallPrefab, wallPosition, Quaternion.identity, wallParent.transform);
        wall.transform.GetChild(0).gameObject.layer = 16;
        return wall;
    }
    private void CreateMesh(Vector2 bottomLeftCorner, Vector2 topRightCorner)
    {
        Vector3 bottomLeftV = new Vector3(bottomLeftCorner.x, 0, bottomLeftCorner.y);
        Vector3 bottomRightV = new Vector3(topRightCorner.x, 0, bottomLeftCorner.y);
        Vector3 topLeftV = new Vector3(bottomLeftCorner.x, 0, topRightCorner.y);
        Vector3 topRightV = new Vector3(topRightCorner.x, 0, topRightCorner.y);

        Vector3[] vertices = new Vector3[]
        {
            topLeftV,
            topRightV,
            bottomLeftV,
            bottomRightV
        };

        Vector2[] uvs = new Vector2[vertices.Length];
        for (int i = 0; i < uvs.Length; i++)
        {
            uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
        }

        int[] triangles = new int[]
        {
            0,
            1,
            2,
            2,
            1,
            3
        };
        Mesh mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.uv = uvs;
        mesh.triangles = triangles;

        //던전 플로어 생성
        GameObject dungeonFloor = new GameObject("Mesh" + bottomLeftCorner, typeof(MeshFilter), typeof(MeshRenderer));

        dungeonFloor.transform.position = Vector3.zero;
        dungeonFloor.transform.localScale = Vector3.one;
        dungeonFloor.GetComponent<MeshFilter>().mesh = mesh;
        dungeonFloor.GetComponent<MeshRenderer>().material = material;
        dungeonFloor.transform.parent = transform;
        int dungeonFloorX = (int)dungeonFloor.GetComponent<Renderer>().bounds.size.x;
        int dungeonFloorZ = (int)dungeonFloor.GetComponent<Renderer>().bounds.size.z;
        dungeonFloor.name = null;
        dungeonFloor.layer = 8;
        dungeonFloor.AddComponent<BoxCollider>();
        //dungeonFloor.transform.GetComponent<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        //dungeonFloor.GetComponent<Renderer>().receiveShadows = false;
        Vector3 vector;
        vector = dungeonFloor.GetComponent<BoxCollider>().size;
        vector.y = 0f;
        dungeonFloor.GetComponent<BoxCollider>().size = vector;
        mapList.Add(dungeonFloor);
        if(dungeonFloorX < 8)
        {
            dungeonFloor.name = "hallway";
            dungeonFloor.AddComponent<Hallway>();
            hallwayList.Add(dungeonFloor);
        }
        //벽 생성
        for (int row = (int)bottomLeftV.x; row < (int)bottomRightV.x; row++)
        {
            var wallPosition = new Vector3(row, 0, bottomLeftV.z);
            AddWallPositionToList(wallPosition, possibleWallHorizontalPosition, possibleDoorHorizontalPosition);
        }
        for (int row = (int)topLeftV.x; row < (int)topRightCorner.x; row++)
        {
            var wallPosition = new Vector3(row, 0, topRightV.z);
            AddWallPositionToList(wallPosition, possibleWallHorizontalPosition, possibleDoorHorizontalPosition);
        }
        for (int col = (int)bottomLeftV.z; col < (int)topLeftV.z; col++)
        {
            var wallPosition = new Vector3(bottomLeftV.x, 0, col);
            AddWallPositionToList(wallPosition, possibleWallVerticalPosition, possibleDoorVerticalPosition);
        }
        for (int col = (int)bottomRightV.z; col < (int)topRightV.z; col++)
        {
            var wallPosition = new Vector3(bottomRightV.x, 0, col);
            AddWallPositionToList(wallPosition, possibleWallVerticalPosition, possibleDoorVerticalPosition);
        }
    }

    //벽
    private void AddWallPositionToList(Vector3 wallPosition, List<Vector3Int> wallList, List<Vector3Int> doorList)
    {
        Vector3Int point = Vector3Int.CeilToInt(wallPosition);
        if (wallList.Contains(point))
        {
            doorList.Add(point);
            wallList.Remove(point);
        }
        else
        {
            wallList.Add(point);
        }
    }

    //리셋할 떄 마다 던전 제거
    private void DestroyAllChildren()
    {
        while(transform.childCount != 0)
        {
            foreach(Transform item in transform)
            {
                DestroyImmediate(item.gameObject);
            }
        }
    }
}
