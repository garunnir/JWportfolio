﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class BossMap : MapEnemyCreator
{
    public bool clearCheck = false;
    public bool playerRoomComeIn = false;
    public GameObject bossEnemyObj;
    EnemyPrefabsManager manager;
    public GameObject obj;

    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.Find("MapManager").GetComponent<EnemyPrefabsManager>();
        bossEnemyObj = manager.bossEnemyBackup.gameObject;
        BossCreator(bossEnemyObj);
        gameObject.tag = "Boss";
        LightCreator();
        obj = manager.libraryObj[0];
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.tag == "Player")
    //    {
    //        playerRoomComeIn = true;
    //        if (playerRoomComeIn == true && clearCheck == false)
    //        {
    //            Invoke("BossComing", 1);
    //        }
    //        clearCheck = true;
    //    }
    //}
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            print("충돌한 오브젝트 :"+collision.name);
            playerRoomComeIn = true;
            if (playerRoomComeIn == true && clearCheck == false)
            {
                Invoke("BossComing", 1);
            }
            clearCheck = true;
        }
    }

    void BossComing()
    {
        gameObject.transform.GetChild(0).gameObject.SetActive(true);
        EnemyPrefabsManager.battleBegins = true;
        clearCheck = true;
    }
}
