﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Books : MonoBehaviour
{
    public float power;
    Books mybooks;
    public AudioSource booksAudio;
    // Start is called before the first frame update
    void Start()
    {
        mybooks = gameObject.GetComponent<Books>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            booksAudio.Play();
            Vector3 direction = transform.position - other.gameObject.transform.position;
            direction = direction * 1000;
            direction = direction.normalized * power;

            Rigidbody[] obj = gameObject.GetComponentsInChildren<Rigidbody>();

            for (int i = 0; i < obj.Length; i++)
            {
                obj[i].AddForce(direction);
            }
            gameObject.GetComponent<BoxCollider>().enabled = false;
            mybooks.enabled = false;
        }
    }
}
