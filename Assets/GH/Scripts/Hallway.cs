﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hallway : MonoBehaviour
{
    public GameObject[] door = new GameObject[2];
    EnemyPrefabsManager mapmaneger;
    public Vector3 hallwayLength;
    public Transform[] allChildren = new Transform[2];
    public bool checkEnd;
    // Start is called before the first frame update
    void Start()
    {
        doorCreator();
        for (int i = 0; i < 2; i++)
        {
            allChildren[i] = gameObject.transform.GetChild(i).transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        DoorCtr();
    }

    void DoorCtr()
    {
        if (EnemyPrefabsManager.battleBegins == true && EnemyPrefabsManager.doorCtr  == true)
        {
            Invoke("CloseDoor", 0.5f);
        }
        if(EnemyPrefabsManager.doorCtr == false)
        {
            Invoke("OpenDoor", 0.5f);
        }
    }

    void OpenDoor()
    {
        //print("문열기!!");
        for (int i = 0; i < 2; i++)
        {
            allChildren[i].gameObject.transform.GetChild(0).gameObject.SetActive(false);
        }
        EnemyPrefabsManager.doorCtr = true;
    }

    void CloseDoor()
    {
        //print("문닫기!!");
        for (int i = 0; i < 2; i++)
        {
            allChildren[i].gameObject.transform.GetChild(0).gameObject.SetActive(true);
        }
        EnemyPrefabsManager.battleBegins = false;
    }
    void doorCreator()
    {
        mapmaneger = GameObject.Find("MapManager").GetComponent<EnemyPrefabsManager>();
        hallwayLength = gameObject.GetComponent<MeshRenderer>().bounds.size;
        door = mapmaneger.doorObjBackup;

        //print("center " + gameObject.GetComponent<MeshRenderer>().bounds.center);

        if (gameObject.GetComponent<MeshRenderer>().bounds.size.x > gameObject.GetComponent<MeshRenderer>().bounds.size.z)
        {
            Vector3[] doorPos = new Vector3[2];
            doorPos[0] = gameObject.GetComponent<MeshRenderer>().bounds.center + new Vector3(hallwayLength.x / 2, 0, 0);
            doorPos[1] = gameObject.GetComponent<MeshRenderer>().bounds.center + new Vector3(-hallwayLength.x / 2, 0, 0);

            for (int i = 0; i < 2; i++)
            {
                GameObject doorObj = Instantiate(door[0], gameObject.transform);
                doorObj.transform.position = doorPos[i];
                doorObj.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
        if (gameObject.GetComponent<MeshRenderer>().bounds.size.x < gameObject.GetComponent<MeshRenderer>().bounds.size.z)
        {
            Vector3[] doorPos = new Vector3[2];
            doorPos[0] = gameObject.GetComponent<MeshRenderer>().bounds.center + new Vector3(0, 0, hallwayLength.z / 2);
            doorPos[1] = gameObject.GetComponent<MeshRenderer>().bounds.center + new Vector3(0, 0, -hallwayLength.z / 2);

            for (int i = 0; i < 2; i++)
            {
                GameObject doorObj = Instantiate(door[1], gameObject.transform);
                doorObj.transform.position = doorPos[i];
                doorObj.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
    }
}
