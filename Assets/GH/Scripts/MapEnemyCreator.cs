﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class MapEnemyCreator : MonoBehaviour
{
    public List<GameObject> enemys;
    public float division = 3f;
    public float objdistance = 2;
    EnemyPrefabsManager mapmaneger;
    DungeonCreator dungeonCreator;
    public List<GameObject> hallwaylist;
    public GameObject lights;
    public List<GameObject> mapObjets;
    // Start is called before the first frame update

    /// <summary>
    /// 일반 맵 에너미
    /// </summary>
    public void EnemyCreator(List<GameObject> allobj)
    {
        
        mapmaneger = GameObject.Find("MapManager").GetComponent<EnemyPrefabsManager>();
        enemys = new List<GameObject>(mapmaneger.enemysBackup);
        GameObject enemy;
        List<GameObject> enemyList = new List<GameObject>();
        for (int k = 0; k < enemys.Count; k++)
        {
            enemy = Instantiate(enemys[k].gameObject, gameObject.transform);
            enemyList.Add(enemy);
            enemyList[k].SetActive(false);
        }
        enemyList.AddRange(allobj);
        for (int i = 0; i < enemys.Count; i++)
        {
            Vector3 pos = gameObject.GetComponent<MeshRenderer>().bounds.center +
                new Vector3
                (Random.Range(gameObject.GetComponent<MeshRenderer>().bounds.size.x / division, -gameObject.GetComponent<MeshRenderer>().bounds.size.x / division),
                0.5f,
                Random.Range(gameObject.GetComponent<MeshRenderer>().bounds.size.z / division, -gameObject.GetComponent<MeshRenderer>().bounds.size.z / division));
            enemyList[i].transform.position = pos;
            for (int j = 0; j < enemyList.Count; j++)
            {
                if (i == j)
                {
                    continue;
                }
                float distance = Vector3.Distance(pos, enemyList[j].transform.position);
                if (distance < objdistance)
                {
                    i--;
                    break;
                }
            }
        }
    }

    public void LightCreator()
    {
        mapmaneger = GameObject.Find("MapManager").GetComponent<EnemyPrefabsManager>();
        lights = mapmaneger.pointLight;
        float posx = Mathf.Abs(gameObject.GetComponent<MeshRenderer>().bounds.center.x);
        float posz = Mathf.Abs(gameObject.GetComponent<MeshRenderer>().bounds.center.z);
        Vector3[] location =
            { new Vector3((posx -1) + (gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 2.5f, (posz -1) + (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2)),
              new Vector3((posx +1) -(gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 2.5f, (posz -1) + (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2)),
              new Vector3((posx -1) + (gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 2.5f, (posz +1) - (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2)),
              new Vector3((posx +1) - (gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 2.5f, (posz +1) - (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2))};
        Transform lightParent = GameObject.Find("LightParent").gameObject.transform;
        for (int i = 0; i <4; i++)
        {
            if(i ==0)
            {
               GameObject obj = Instantiate(lights, location[0] - new Vector3(-0.76f,0,2), Quaternion.Euler(new Vector3(0, 0, 0)), lightParent);
                obj.name = "0";
            }
            if (i == 1)
            {
                GameObject obj = Instantiate(lights, location[1] - new Vector3(0.76f, 0, 2), Quaternion.Euler(new Vector3(0, 180, 0)), lightParent);
                obj.name = "1";
            }  
            if (i == 2)
            {
                GameObject obj = Instantiate(lights, location[2] - new Vector3(-0.76f, 0, -2), Quaternion.Euler(new Vector3(0, 0, 0)), lightParent);
                obj.name = "2";
            }

            if (i == 3)
            {
                GameObject obj = Instantiate(lights, location[3] - new Vector3(0.76f, 0, -2), Quaternion.Euler(new Vector3(0, 180, 0)), lightParent);
                obj.name = "3";
            }   

        }
    }

    public void BossCreator(GameObject bossEnemy)
    {
        GameObject boss = Instantiate(bossEnemy,gameObject.transform);
        Vector3 dir = new Vector3(gameObject.GetComponent<MeshRenderer>().bounds.center.x, 0.5f, gameObject.GetComponent<MeshRenderer>().bounds.center.z);
        dir.y = 0.3f;
        boss.transform.position = dir;
        boss.SetActive(false);
    }

    public void RoomSetting()
    {
        mapmaneger = GameObject.Find("MapManager").GetComponent<EnemyPrefabsManager>();
        mapObjets = new List<GameObject>(mapmaneger.libraryObj);

        int xSize = (int)gameObject.GetComponent<MeshRenderer>().bounds.size.x;
        int zSize = (int)gameObject.GetComponent<MeshRenderer>().bounds.size.z;
        Vector3 center = gameObject.GetComponent<MeshRenderer>().bounds.center;
        float posx = Mathf.Abs(gameObject.GetComponent<MeshRenderer>().bounds.center.x);
        float centerZ = Mathf.Abs(gameObject.GetComponent<MeshRenderer>().bounds.center.z);
        if (xSize%2 == 1 && zSize%2 ==1)
        {
            for(int i =0; i< xSize-1; i++)
            {
               
            }
            print("나머지 1인 사이즈값:" + gameObject.GetComponent<MeshRenderer>().bounds.size);
            print("나머지가 1임");
        }
        if(xSize%2 ==0 && zSize%2 ==1)
        {

        }
        if(xSize % 2 == 1 && zSize % 2 == 0)
        {

        }
        if(xSize % 2 == 0 && zSize % 2 == 0)
        {
            for (int i = 0; i < xSize; i++)
            {

            }
        }
    }


    public List<GameObject> RoomobjSetting(List<GameObject> objects, int randomMin,int randomMax , float interval = 1, float objectXpos = 0, float objectZpos = 0, float xRot = 0, float zRot = 0)
    {
        dungeonCreator = GameObject.Find("DungeonCreator").GetComponent<DungeonCreator>();
        hallwaylist = new List<GameObject>(dungeonCreator.hallwayList);
        float posx = Mathf.Abs(gameObject.GetComponent<MeshRenderer>().bounds.center.x);
        float posz = Mathf.Abs(gameObject.GetComponent<MeshRenderer>().bounds.center.z);
        float objSizeX = gameObject.GetComponent<MeshRenderer>().bounds.size.x;
        float objSizeZ = gameObject.GetComponent<MeshRenderer>().bounds.size.z;

        Vector3[] location =
    { new Vector3((posx -1.25f - objectXpos) + (gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 0, (posz -0.5f - objectZpos) + (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2)),
              new Vector3((posx +0.5f + objectXpos) -(gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 0, (posz -1.25f - objectZpos) + (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2)),
              new Vector3((posx -0.5f - objectXpos) + (gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 0, (posz +1.25f + objectZpos) - (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2)),
              new Vector3((posx +1.25f + objectXpos) - (gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 0, (posz +0.5f + objectZpos) - (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2))
            };

        GameObject objectparent = new GameObject();
        objectparent.name = "objs";
        List<GameObject> allobj = new List<GameObject>();
        for (int i = 0; i<4; i++)
        {
            if (i == 0)
            {
                for(int j = 0; j <(objSizeX-1)/interval; j++)
                {
                    GameObject obj = Instantiate(objects[Random.Range(randomMin, randomMax)], location[0] + new Vector3(-(j * interval), 0,0), Quaternion.Euler(new Vector3(xRot, 180, zRot)));
                    obj.transform.parent = objectparent.transform;
                    allobj.Add(obj);
                    if (j - (objSizeX - 1) / interval < 0 && j - (objSizeX - 1) / interval >= -1)
                    {
                        Destroy(obj);
                    }
                    for (int k = 0; k < hallwaylist.Count; k++)
                    {
                        float distance = Vector3.Distance(obj.transform.position, hallwaylist[k].GetComponent<MeshRenderer>().bounds.center);
                        if (distance < 5)
                        {
                            Destroy(obj);
                        }
                    }
                }
            }
            else if(i == 1)
            {
                for(int j = 0; j<((objSizeZ-1) / interval); j++)
                {
                    GameObject obj = Instantiate(objects[Random.Range(randomMin, randomMax)], location[1] + new Vector3(0, 0, -(j * interval)), Quaternion.Euler(new Vector3(xRot, 90, zRot)));
                    obj.transform.parent = objectparent.transform;
                    allobj.Add(obj);
                    if (j - (objSizeZ - 1) / interval < 0 && j - (objSizeZ - 1) / interval >= -1)
                    {
                        Destroy(obj);
                    }
                    for (int k = 0; k < hallwaylist.Count; k++)
                    {
                        float distance = Vector3.Distance(obj.transform.position, hallwaylist[k].GetComponent<MeshRenderer>().bounds.center);
                        if (distance < 5)
                        {
                            Destroy(obj);
                        }
                    }
                }
            }
            else if(i == 2)
            {
                for(int j = 0; j <(objSizeZ-1) / interval; j++)
                {
                    GameObject obj = Instantiate(objects[Random.Range(randomMin, randomMax)], location[2] + new Vector3(0, 0, (j * interval)), Quaternion.Euler(new Vector3(xRot, -90, zRot)));
                    obj.transform.parent = objectparent.transform;
                    allobj.Add(obj);
                    if (j - (objSizeZ - 1) / interval < 0 && j - (objSizeZ - 1) / interval >= -1)
                    {
                        Destroy(obj);
                    }
                    for (int k = 0; k < hallwaylist.Count; k++)
                    {
                        float distance = Vector3.Distance(obj.transform.position, hallwaylist[k].GetComponent<MeshRenderer>().bounds.center);
                        if (distance < 5)
                        {
                            Destroy(obj);
                        }
                    }
                }
            }
            else if(i == 3)
            {
                
                for (int j = 0; j <(objSizeX-1) / interval; j++)
                {
                    GameObject obj = Instantiate(objects[Random.Range(randomMin, randomMax)], location[3] + new Vector3((j * interval), 0, 0), Quaternion.Euler(new Vector3(xRot,0,zRot)));
                    obj.transform.parent = objectparent.transform;
                    allobj.Add(obj);
                    if (j - (objSizeX - 1) / interval < 0 && j - (objSizeX - 1) / interval >= -1)
                    {
                        Destroy(obj);
                    }
                    for (int k = 0; k < hallwaylist.Count; k++)
                    {
                        float distance = Vector3.Distance(obj.transform.position, hallwaylist[k].GetComponent<MeshRenderer>().bounds.center);
                        if (distance < 5)
                        {
                            Destroy(obj);
                        }
                    }
                }
            }
        }
        return allobj;
    }

    public List<GameObject> RoomobjSetting(GameObject objects, float interval = 1, float objectXpos = 0, float objectZpos = 0, float xRot = 0, float zRot = 0,float yPos = 0)
    {
        dungeonCreator = GameObject.Find("DungeonCreator").GetComponent<DungeonCreator>();
        hallwaylist = new List<GameObject>(dungeonCreator.hallwayList);
        float posx = Mathf.Abs(gameObject.GetComponent<MeshRenderer>().bounds.center.x);
        float posz = Mathf.Abs(gameObject.GetComponent<MeshRenderer>().bounds.center.z);
        float objSizeX = gameObject.GetComponent<MeshRenderer>().bounds.size.x;
        float objSizeZ = gameObject.GetComponent<MeshRenderer>().bounds.size.z;

        Vector3[] location =
    { new Vector3((posx -1.25f - objectXpos) + (gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 0, (posz -0.5f - objectZpos) + (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2)),
              new Vector3((posx +0.5f + objectXpos) -(gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 0, (posz -1.25f - objectZpos) + (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2)),
              new Vector3((posx -0.5f - objectXpos) + (gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 0, (posz +1.25f + objectZpos) - (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2)),
              new Vector3((posx +1.25f + objectXpos) - (gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 0, (posz +0.5f + objectZpos) - (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2))
            };

        GameObject objectparent = new GameObject();
        List<GameObject> allobj = new List<GameObject>();
        objectparent.name = "objs";
        for (int i = 0; i < 4; i++)
        {
            if (i == 0)
            {
                for (int j = 0; j < (objSizeX - 1) / interval; j++)
                {
                    GameObject obj = Instantiate(objects, location[0] + new Vector3(-(j * interval), yPos, 0), Quaternion.Euler(new Vector3(xRot, 180, zRot)));
                    obj.transform.parent = objectparent.transform;
                    allobj.Add(obj);
                    if (j - (objSizeX - 1) / interval < 0 && j - (objSizeX - 1) / interval >= -1)
                    {
                        Destroy(obj);
                    }
                    for (int k = 0; k < hallwaylist.Count; k++)
                    {
                        float distance = Vector3.Distance(obj.transform.position, hallwaylist[k].GetComponent<MeshRenderer>().bounds.center);
                        if (distance < 5)
                        {
                            Destroy(obj);
                        }
                    }
                }
                if (interval > 1)
                {

                }
            }
            else if (i == 1)
            {
                for (int j = 0; j < ((objSizeZ - 1) / interval); j++)
                {
                    GameObject obj = Instantiate(objects, location[1] + new Vector3(0, yPos, -(j * interval)), Quaternion.Euler(new Vector3(xRot, 90, zRot)));
                    obj.transform.parent = objectparent.transform;
                    allobj.Add(obj);
                    if (j - (objSizeZ - 1) / interval < 0 && j - (objSizeZ - 1) / interval >= -1)
                    {
                        Destroy(obj);
                    }
                    for (int k = 0; k < hallwaylist.Count; k++)
                    {
                        float distance = Vector3.Distance(obj.transform.position, hallwaylist[k].GetComponent<MeshRenderer>().bounds.center);
                        if (distance < 5)
                        {
                            Destroy(obj);
                        }
                    }
                }
            }
            else if (i == 2)
            {
                for (int j = 0; j < (objSizeZ - 1) / interval; j++)
                {
                    GameObject obj = Instantiate(objects, location[2] + new Vector3(0, yPos, (j * interval)), Quaternion.Euler(new Vector3(xRot, -90, zRot)));
                    obj.transform.parent = objectparent.transform;
                    allobj.Add(obj);
                    if (j - (objSizeZ - 1) / interval < 0 && j - (objSizeZ - 1) / interval >= -1)
                    {
                        Destroy(obj);
                    }
                    for (int k = 0; k < hallwaylist.Count; k++)
                    {
                        float distance = Vector3.Distance(obj.transform.position, hallwaylist[k].GetComponent<MeshRenderer>().bounds.center);
                        if (distance < 5)
                        {
                            Destroy(obj);
                        }
                    }
                }
            }
            else if (i == 3)
            {

                for (int j = 0; j < (objSizeX - 1) / interval; j++)
                {
                    GameObject obj = Instantiate(objects, location[3] + new Vector3((j * interval), yPos, 0), Quaternion.Euler(new Vector3(xRot, 0, zRot)));
                    obj.transform.parent = objectparent.transform;
                    allobj.Add(obj);
                    if (j - (objSizeX - 1) / interval < 0 && j - (objSizeX - 1) / interval >= -1)
                    {
                        Destroy(obj);
                    }
                    for (int k = 0; k < hallwaylist.Count; k++)
                    {
                        float distance = Vector3.Distance(obj.transform.position, hallwaylist[k].GetComponent<MeshRenderer>().bounds.center);
                        if (distance < 5)
                        {
                            Destroy(obj);
                        }
                    }
                }
            }
        }
        return allobj;
    }

    public List<GameObject> RoomobjSetting(List<GameObject> objects, int objRandomNumberMin, int objRandomNumberMax, 
        float objrandomPosMin = 0, float objrandomPosMax = 0, float interval = 1, float objectXpos = 0, float objectZpos = 0, float xRot = 0, float zRot = 0)
    {
        dungeonCreator = GameObject.Find("DungeonCreator").GetComponent<DungeonCreator>();
        hallwaylist = new List<GameObject>(dungeonCreator.hallwayList);
        float posx = Mathf.Abs(gameObject.GetComponent<MeshRenderer>().bounds.center.x);
        float posz = Mathf.Abs(gameObject.GetComponent<MeshRenderer>().bounds.center.z);
        float objSizeX = gameObject.GetComponent<MeshRenderer>().bounds.size.x;
        float objSizeZ = gameObject.GetComponent<MeshRenderer>().bounds.size.z;

        Vector3[] location =
    { new Vector3((posx -1.25f - objectXpos) + (gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 0, (posz -0.5f - objectZpos) + (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2)),
              new Vector3((posx +0.5f + objectXpos) -(gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 0, (posz -1.25f - objectZpos) + (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2)),
              new Vector3((posx -0.5f - objectXpos) + (gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 0, (posz +1.25f + objectZpos) - (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2)),
              new Vector3((posx +1.25f + objectXpos) - (gameObject.GetComponent<MeshRenderer>().bounds.size.x/2), 0, (posz +0.5f + objectZpos) - (gameObject.GetComponent<MeshRenderer>().bounds.size.z/2))
            };
        GameObject objectparent = new GameObject();
        List<GameObject> allobj = new List<GameObject>();
        objectparent.name = "objs";
        
        for (int i = 0; i < 4; i++)
        {
            if (i == 0)
            {
                for (int j = 0; j < (objSizeX - 1) / interval; j++)
                {
                    GameObject obj = 
                        Instantiate(objects[Random.Range(objRandomNumberMin, objRandomNumberMax)],
                        location[0] + new Vector3(-(j * interval + Random.Range(objrandomPosMin,objrandomPosMax)),
                        0,
                        Random.Range(objrandomPosMin, objrandomPosMax)), Quaternion.Euler(new Vector3(xRot, 180, zRot)));

                    obj.transform.parent = objectparent.transform;
                    allobj.Add(obj);
                    if (j - (objSizeX - 1) / interval < 0 && j - (objSizeX - 1) / interval >= -1)
                    {
                        Destroy(obj);
                    }
                    for (int k = 0; k < hallwaylist.Count; k++)
                    {
                        float distance = Vector3.Distance(obj.transform.position, hallwaylist[k].GetComponent<MeshRenderer>().bounds.center);
                        if (distance < 5)
                        {
                            Destroy(obj);
                        }
                    }
                }
            }
            else if (i == 1)
            {
                for (int j = 0; j < ((objSizeZ - 1) / interval); j++)
                {
                    GameObject obj = Instantiate(objects[Random.Range(objRandomNumberMin, objRandomNumberMax)], location[1] + new Vector3(Random.Range(objrandomPosMin, objrandomPosMax), 0, -(j * interval + Random.Range(objrandomPosMin, objrandomPosMax))), Quaternion.Euler(new Vector3(xRot, 90, zRot)));
                    obj.transform.parent = objectparent.transform;
                    allobj.Add(obj);
                    if (j - (objSizeZ - 1) / interval < 0 && j - (objSizeZ - 1) / interval >= -1)
                    {
                        Destroy(obj);
                    }
                    for (int k = 0; k < hallwaylist.Count; k++)
                    {
                        float distance = Vector3.Distance(obj.transform.position, hallwaylist[k].GetComponent<MeshRenderer>().bounds.center);
                        if (distance < 5)
                        {
                            Destroy(obj);
                        }
                    }
                }
            }
            else if (i == 2)
            {
                for (int j = 0; j < (objSizeZ - 1) / interval; j++)
                {
                    GameObject obj = Instantiate(objects[Random.Range(objRandomNumberMin, objRandomNumberMax)], location[2] + new Vector3(Random.Range(objrandomPosMin, objrandomPosMax), 0, (j * interval + Random.Range(objrandomPosMin, objrandomPosMax))), Quaternion.Euler(new Vector3(xRot, -90, zRot)));
                    obj.transform.parent = objectparent.transform;
                    allobj.Add(obj);
                    if (j - (objSizeZ - 1) / interval < 0 && j - (objSizeZ - 1) / interval >= -1)
                    {
                        Destroy(obj);
                    }
                    for (int k = 0; k < hallwaylist.Count; k++)
                    {
                        float distance = Vector3.Distance(obj.transform.position, hallwaylist[k].GetComponent<MeshRenderer>().bounds.center);
                        if (distance < 5)
                        {
                            Destroy(obj);
                        }
                    }
                }
            }
            else if (i == 3)
            {

                for (int j = 0; j < (objSizeX - 1) / interval; j++)
                {
                    GameObject obj = Instantiate(objects[Random.Range(objRandomNumberMin, objRandomNumberMax)], location[3] + new Vector3((j * interval + Random.Range(objrandomPosMin, objrandomPosMax)), 0, Random.Range(objrandomPosMin, objrandomPosMax)), Quaternion.Euler(new Vector3(xRot, 0, zRot)));
                    obj.transform.parent = objectparent.transform;
                    allobj.Add(obj);
                    if (j - (objSizeX - 1) / interval < 0 && j - (objSizeX - 1) / interval >= -1)
                    {
                        Destroy(obj);
                    }
                    for (int k = 0; k < hallwaylist.Count; k++)
                    {
                        float distance = Vector3.Distance(obj.transform.position, hallwaylist[k].GetComponent<MeshRenderer>().bounds.center);
                        if (distance < 5)
                        {
                            Destroy(obj);
                        }
                    }
                }
            }
        }
        return allobj;
    }

    /// <summary>
    /// 특정 좌표에서 X,Z축으로 variable값 만큼 생성함
    /// </summary>
    /// <param name="생성할 오브젝트"></param>
    /// <param name="오브젝트의 간격"></param>
    /// <param name="생성할 좌표"></param>
    /// <param name="생성할 갯수"></param>
    /// <param name="Quaternion값"></param>
    /// <param name="axisX 성할시에 axisZ값을 0으로"></param>
    /// <param name="axisZ 성할시에 axisX값을 0으로"></param>
    public List<GameObject> RoomobjSetting(GameObject objects, float interval, Vector3 pos, int number,  Quaternion qu, float axisX = 1,float axisZ = 1)
    {
        GameObject onjectparent = new GameObject();
        List<GameObject> allobj = new List<GameObject>();
        onjectparent.name = "objs";
        for (int variable = 0; variable < number; variable++)
        {
            if(variable == 0)
            {
                GameObject obj = Instantiate(objects, pos + new Vector3(variable * axisX, 0, variable * axisZ), qu);
                allobj.Add(obj);
            }
            else
            {
                GameObject obj = Instantiate(objects, pos + new Vector3((variable * interval) * axisX, 0, (variable * interval) * axisZ), qu);
                allobj.Add(obj);
            }
        }
        for(int j = 0; j < allobj.Count; j++)
        {
            allobj[j].transform.parent = onjectparent.transform;
        }

        return allobj;
    }
    public List<GameObject> RoomobjSetting(GameObject objects, float interval, Vector3 pos, int number, Quaternion qu, float axisX = 1, float axisZ = 1 ,float MinPos = 0,float MaxPos = 0)
    {
        GameObject onjectparent = new GameObject();
        onjectparent.name = "objs";
        List<GameObject> allobj = new List<GameObject>();
        for (int variable = 0; variable < number; variable++)
        {
            if (variable == 0)
            {
                GameObject obj = Instantiate(objects, pos + new Vector3(variable * axisX, 0, variable * axisZ), qu);
                obj.transform.parent = onjectparent.transform;
                allobj.Add(obj);
            }
            else
            {
                GameObject obj = Instantiate(objects, pos + new Vector3((variable * interval + Random.Range(MinPos,MaxPos)) * axisX, 0, (variable * interval + Random.Range(MinPos,MaxPos)) * axisZ), qu);
                obj.transform.parent = onjectparent.transform;
                allobj.Add(obj);
            }
        }
        return allobj;
    }
}