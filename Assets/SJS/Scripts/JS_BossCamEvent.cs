﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//카메라가 활성화 될 때 원하는 시간동안 원하는 방향으로 이동할 수 있게 한다.

// 1. 렉사이를 천천히 둘러보다가 

//필요 속성 : 보스, 타겟 
public class JS_BossCamEvent : MonoBehaviour
{
    //필요 속성 : 보스, 타겟, 타겟의 카메라
    public GameObject Boss;
    public GameObject Target;
    public GameObject Target_Cam;


    // Start is called before the first frame update
    void Start()
    {
        //평소에 카메라를 비활성화 시키고, 카메라가 켜질 때.(플레이어가 보스방 콜라이더를 건드릴 때)
        //이벤트 씬을 코루틴 또는 애니메이션을 사용해서 구현해보자
    }
}
