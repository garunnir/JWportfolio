﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JS_Enemy_Common_003_Cassiopeia : JS_Enemy
{
    public GameObject Model;
    public GameObject effect;

    protected override IEnumerator Idle()
    {
        if (isSpawned == false)
        {
            //이펙트 나오고
            yield return new WaitForSeconds(0.5f);
            effect.SetActive(true);
            //부모 버리면서 크기 변동을 피함
            effect.transform.parent = null;
            yield return new WaitForSeconds(0.1f);
            Model.SetActive(true);
            //메쉬 킴
            StartCoroutine(BouncySpawn());
            yield return new WaitForSeconds(0.5f);
            isSpawned = true;
            //방문을 열기 전까진 쉼(임시로 쉼)
            yield return new WaitForSeconds(2.0f);
        }
        yield return new WaitForSeconds(0.7f);
        //JS_Animation.CrossFade(playerAnim.move.name, 0.3f);
        if (Vector3.Distance(transform.position, target.position) < eInfo.attackRange)
        {
            //상태를 공격으로 바꾼다
            eFSM = eState.Attack;
        }
        else
        {
            JS_Animator.SetTrigger("Move");
            eFSM = eState.Move;
        }
    }

    protected override IEnumerator Move()
    {
        JS_Animator.SetTrigger("Move");
        yield return null;
    }

    bool attackIgnore = false;
    protected override IEnumerator Attack()
    {
        //설정한 패턴을 코루틴으로 호출한다
        JS_Animator.SetTrigger("Attack");
        //1.공격 중임을 설정
        yield return new WaitForSeconds(0.2f);
        print("기본 공격 시작!");
        while (isDone == false)
        {
            yield return null;
            if (eFSM == eState.Damage)
            {
                isDone = true;
                attackIgnore = true;
            }
        }
        if (attackIgnore)
        {
            yield return new WaitForSeconds(0.2f);
            attackIgnore = false;
        }
        else
        {
            yield return new WaitForSeconds(0.2f);
            print("기본 공격 끝!");
            eFSM = eState.Idle;
        }
    }
    //에너미 스테이터스를 설정
    protected override IEnumerator Damage(Vector3 bulletDir)
    {

        //trigger 거르기
        OnDamage = true;
        eFSM = eState.Damage;
        //하던 행동 끝내기
        JS_Animator.SetTrigger("Damage");
        knockbackEffect.transform.position = transform.position;
        knockbackEffect.GetComponent<ParticleSystem>().Play();
        yield return null;
        //맞으면 총알 방향으로 밀림(벡터 크기/무게). 미구현
        print("Damage");
        //타입이 보스이고, 1페이즈 상태에서 일정 체력 이하로 떨어질 경우 다음 페이즈를 넘김
        //나중에 보스를 더 추가할 경우 함수로 빼낼 예정
        //0 이하면 죽임
        if (currentHp <= 0)
        {
            StopAllCoroutines();
            JS_Animator.SetTrigger("Die");
            eFSM = eState.Die;
            StartCoroutine(eFSM.ToString());
        }
        //0보다 크면 원래 했던 행동을 계속 함
        else
        {
            JS_Animator.SetTrigger("Damage");
            yield return new WaitForSeconds(knockbackTime);
            JS_Animator.SetTrigger("EndDamage");
            eFSM = eState.Idle;
            OnDamage = false;
        }
    }
    protected override IEnumerator Die()
    {
        //피가 까이면 죽음
        //하던 동작을 멈추고
        //JS_Animation.Stop();
        //다이로 바꾼다
        //JS_Animation.Play(playerAnim.die.name);
        yield return null;
        print("die~");
        GetComponent<CharacterController>().enabled = false;
        if (GetComponent<CapsuleCollider>())
        {
            GetComponent<CapsuleCollider>().enabled = false;
        }
        if (GetComponent<MeshCollider>())
        {
            GetComponent<MeshCollider>().enabled = false;
        }
        Invoke("DieEvent", 3f);
        StopAllCoroutines();
    }
    protected override void DieEvent()
    {
        StartCoroutine(enemyDown());
    }

    IEnumerator enemyDown()
    {
        float downTime = 2f;
        float currentTime = 0f;
        while (currentTime < downTime)
        {
            yield return null;
            currentTime += Time.deltaTime;
            transform.position += transform.up * (-2f) * Time.deltaTime;
        }
        Destroy(gameObject);
    }
    void CassiAttack()
    {
        StartCoroutine(shotBullet(firePos[0]));
    }
    void Start()
    {
        //김길훈이 추가한 스크립트
        target = GameObject.Find("Player").transform;
        //기본 세팅
        //캐릭터 컨트롤러
        cc = GetComponent<CharacterController>();
        JS_Animator = GetComponent<Animator>();
        //플레이어를 바라보게 설정
        StartCoroutine(lookAtTarget());
        //fsm 시작
        Enemy_Spawn();

        Model.SetActive(false);
    }
    private void Update()
    {
        if (eFSM == eState.Move)
        {
            //방문이 열리면 플레이어 방향으로 이동한다
            //플레이어 방향으로 이동한다
            Vector3 dir = target.position - transform.position;
            cc.SimpleMove(dir.normalized * eInfo.enemySpeed);
            //만약 플레이어가 공격 범위 안에 있으면
            if (Vector3.Distance(transform.position, target.position) < eInfo.attackRange)
            {
                //상태를 공격으로 바꾼다
                eFSM = eState.Attack;
            }
        }
    }
    protected override void EndDelay()
    {
        print("EndDelay");
        isDone = true;
        JS_Animator.SetTrigger("Idle");
    }
}
