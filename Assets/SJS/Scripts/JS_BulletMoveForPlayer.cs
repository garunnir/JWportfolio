﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JS_BulletMoveForPlayer : MonoBehaviour
{
public float speed = 4f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
        Invoke("destroyThis", 5);
    }
    void destroyThis()
    {
        Destroy(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        //부딪치는 대상의 레이어가 Bullet이 아니고, Enemy가 아니면
        //print("other.gameObject.layer : " + (other.gameObject.layer));
        //총알이 파괴된다
        if (other.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {

            Destroy(gameObject);
        }
    }
}
