﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JS_Enemy_Common_002_Rammus : JS_Enemy
{
    //자식 모델
    public GameObject Model;
    public GameObject RollingAttack;
    public GameObject effect;

    float attackMotionDelay =0.4f;
    public float bulletScale = 1f;

    protected override IEnumerator enemyFSM()
    {
        while (eFSM != eState.Die)
        {
            print(transform.name + " 의 상태 : " + eFSM);
            /*
            case eState.Idle:
            case eState.Move:
            case eState.Attack:
            case eState.Damage:
            case eState.Die:
            */
            yield return StartCoroutine(eFSM.ToString());
        }
    }

    protected override IEnumerator Idle()
    {
        yield return new WaitForSeconds(0.5f);
        effect.SetActive(true);
        yield return new WaitForSeconds(0.02f);
        Model.SetActive(true);
        //부모 버리면서 크기 변동을 피함
        effect.transform.parent = null;
        StartCoroutine(BouncySpawn());
        yield return new WaitForSeconds(1f);
        effect.SetActive(false);
        isSpawned = true;


        //적이 공격 범위 안에 들어올 때까지 대기한다
        while (Vector3.Distance(target.transform.position, transform.position)>eInfo.attackDistance)
        {
            yield return new WaitForSeconds(0.3f);
            print("alive");
        }
        //만약 적을 조우할 때 체력이 0보다 크면
        if(currentHp>0)
        {
            //타겟을 바라봄
            lookAtTheTarget();
            //공격상태로 전환한다
            eFSM = eState.Move;
        }
        else
        {
            eFSM = eState.Die;
        }
    }
    protected override IEnumerator Move()
    {
        //나를 바라본다
        StartCoroutine(lookAtTarget());
        //바로 구른다
        JS_Animation.CrossFade(playerAnim.move.name, 0.2f);
        //애니메이션만큼 기다려줌
        yield return new WaitForSeconds(attackMotionDelay);
        //메쉬 콜라이더의 메쉬를 attack2의 메쉬 콜라이더의 메쉬로 교체
        GetComponent<MeshCollider>().sharedMesh = RollingAttack.GetComponent<MeshCollider>().sharedMesh;
        //자식의 model의 skinnedMesh를 비활성화
        Model.GetComponent<SkinnedMeshRenderer>().enabled = false;
        //attack2 활성화
        RollingAttack.SetActive(true);
        JS_Animation.CrossFadeQueued(playerAnim.attack.name, 0.2f);
        //잠깐 기다리다 돌진함
        yield return new WaitForSeconds(1f);
        eFSM = eState.Attack;
    }
    protected override IEnumerator Attack()
    {
        while (eInfo.hp >= 0 && eFSM == eState.Attack)
        {
            //설정한 패턴을 코루틴으로 호출한다
            yield return StartCoroutine(BaseAttack());
            //공격 범위 밖으로 나가면 
            if (Vector3.Distance(transform.position, target.position) > eInfo.attackDistance)
            {
                //상태를 move로 바꾼다
                JS_Animation.CrossFade(playerAnim.move.name, 0.3f);
                eFSM = eState.Move;
            }
        }
    }
    IEnumerator BaseAttack()
    {
        while(true)
        {
            yield return null;
            cc.SimpleMove(transform.forward * eInfo.enemySpeed);
        }
    }
    protected override IEnumerator Damage(Vector3 bulletDir)
    {
        if (isSpawned)
        {
            yield return null;
            //맞으면 총알 방향으로 밀림(벡터 크기/무게). 미구현
            print("Damage");
            //타입이 보스이고, 1페이즈 상태에서 일정 체력 이하로 떨어질 경우 다음 페이즈를 넘김
            //나중에 보스를 더 추가할 경우 함수로 빼낼 예정
            if (currentHp <= 0)
            {
                print("dieeee");
                JS_Animation.CrossFade(playerAnim.die.name, 0.3f);
                eFSM = eState.Die;
                StartCoroutine(eFSM.ToString());
            }
        }
    }
    protected override IEnumerator Die()
    {
        //가만히 있는 상태에서 죽으면 die모션
        if(RollingAttack.activeSelf == false)
        {
            //하던 동작을 멈추고
            JS_Animation.Stop();
            //다이로 바꾼다
            JS_Animation.Play(playerAnim.die.name);
            Invoke("DieEvent1", 1f);

        }
        //굴러서 죽으면 폭발
        else
        {
            Invoke("DieEvent2", 0.05f);
        }
        yield return null;
        GetComponent<CharacterController>().enabled = false;
        if (GetComponent<CapsuleCollider>())
        {
            GetComponent<CapsuleCollider>().enabled = false;
        }
        if (GetComponent<MeshCollider>())
        {
            GetComponent<MeshCollider>().enabled = false;
        }
        print("DIE!!!");
        StopAllCoroutines();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(RollingAttack.activeSelf == true && collision.gameObject.tag == "Player" && collision.gameObject.layer == LayerMask.NameToLayer( "Player"))
        {
            eFSM = eState.Die;
            StartCoroutine(Die());
        }
    }
    //스폰이 끝나야 대미지를 줄 수 있음
    //가만히 있는 상태에서 죽을 경우
    void DieEvent1()
    {
        StartCoroutine(enemyDown());
    }
    bool isDie = false;
    //구르다 폭발할 경우
    void DieEvent2()
    {
        //한번만 발동하게끔 함
        if(isDie)
        {
            return;
        }
        isDie = true; 
        StartCoroutine(explosion());
    }
    IEnumerator explosion()
    {
        //죽으면서 엄청 크게 폭발함(이펙트 추가)
        //bulletFactory를 폭발 이펙트로 바꾸면 됨
        StartCoroutine(shotBullet(firePos[0], 0, 0, bulletScale));
        yield return null;
        Destroy(gameObject);
    }
    //땅으로 사라짐
    IEnumerator enemyDown()
    {
        float downTime = 4f;
        float currentTime = 0f;
        while (currentTime < downTime)
        {
            yield return null;
            currentTime += Time.deltaTime;
            transform.position += transform.up * (-0.3f) * Time.deltaTime;
        }
        Destroy(gameObject);
    }
    // 1발만 쏜다

    void Start()
    {
        //김길훈이 추가한 스크립트
        target = GameObject.Find("Player").transform;
        //기본 세팅
        //적의 데이터를 저장
        //캐릭터 컨트롤러
        cc = GetComponent<CharacterController>();
        JS_Animation = GetComponent<Animation>();
        //애니메이션 설정
        JS_Animation.clip = playerAnim.idle;
        //기본적으로 플레이어를 바라보는 방향에서 정찰하는 느낌으로 서있음
        lookAtTheTarget();
        //fsm 시작
        Enemy_Spawn();
    }
    //타겟을 바라봄
    private void lookAtTheTarget()
    {
        transform.forward = new Vector3(target.position.x - transform.position.x, 0, target.position.z - transform.position.z);
    }
}
