﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//유니티에디터에서만 적용
#if UNITY_EDITOR
public class JS_EnemyEditorUtility :Editor
{
    public void DrawBaseUI(EnemyInfo enemy)
    {
        GUILayout.Space(15);
        EditorGUILayout.LabelField("적의 상태창 (Enemy Status)");
        
        enemy.eType= (EnemyType)EditorGUILayout.EnumPopup("등급",enemy.eType);
        enemy.hp = EditorGUILayout.FloatField("체력", enemy.hp);
        enemy.weight = EditorGUILayout.FloatField("무게", enemy.weight);
        enemy.enemySpeed = EditorGUILayout.FloatField("이동속도", enemy.enemySpeed);
        enemy.attackDistance = EditorGUILayout.FloatField("인식 범위(Blue)", enemy.attackDistance);
        enemy.attackRange = EditorGUILayout.FloatField("공격 범위(Red)", enemy.attackRange);

        GUIChange();
        GUILayout.Space(15);

    }
    // 인스펙터의 값이 변하면 씬 정보도 갱신하고 싶다.
    void GUIChange()
    { 
        if (GUI.changed)
        {
            EditorUtility.SetDirty(target);
        }
    }
    void GUIStatus(EnemyInfo enemy)
    {
        //eType을 설명하고 싶다.
        //enemy.eInfo.eType = EditorGUILayout.LayerField(0, JS_Enemy.EnemyType);
        EditorGUILayout.LabelField("등급", enemy.eType.ToString());
        EditorGUILayout.LabelField("체력", enemy.hp.ToString());
        EditorGUILayout.LabelField("무게", enemy.weight.ToString());
        EditorGUILayout.LabelField("이동속도", enemy.enemySpeed.ToString());
        EditorGUILayout.LabelField("공격 범위", enemy.attackRange.ToString());

    }
}
#endif
