﻿public enum EnemyType
{
    Common,
    Boss,
    Named,
}
[System.Serializable]
public class EnemyInfo 
{
    //enemy 기본 정보
    public EnemyType eType;
    public float hp;
    public float weight;
    public float enemySpeed;
    public float attackDistance;
    public float attackRange;

}
