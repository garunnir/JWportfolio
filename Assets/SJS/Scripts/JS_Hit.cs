﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//앞으로 쓸 코드, enemyBullet에 함께 쓸 예정
//bulletDamagef로
public class JS_Hit : MonoBehaviour
{
    //적의 공격 대미지
    public int bulletDamage = 1;
    
    //다단히트 조정. 기본값 1(1대만 맞게 한다)
    [Range(1,Mathf.Infinity)]
    public int maxHit = 1;
    int hitCount = 0;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player" && collision.gameObject.layer == LayerMask.NameToLayer("Player")
            && hitCount < maxHit)
        {
            hitCount++;
            //플레이어 대미지 코드 넣기
            JW_PlayerAction.Damage(bulletDamage);
        }
    }
}
