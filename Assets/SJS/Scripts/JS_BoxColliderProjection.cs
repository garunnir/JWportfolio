﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 모든 콜라이더의 월드좌표 y값을 0으로 바꾸고 싶다 
// 모든 자식의 모든 박스 콜라이더를 받는다
// 모든 자식의 콜라이더의 y의 월드 위치값을 0으로 고정한다 
public class JS_BoxColliderProjection : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Transform[] allChildren = GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            BoxCollider bc;
            bc = child.GetComponent<BoxCollider>();
            if( bc !=null)
            {
                // 박스 콜라이더의 y값을 월드좌표 기준으로 0으로 만들고 싶다.
                // 박스 콜라이더의 월드 좌표의 y값 만큼 Vector3.down으로 내리고 싶다.
                // 임의의 게임 오브젝트를 만들어서 
                GameObject worldPos = new GameObject();
                worldPos.layer = 11;
                //게임오브젝트의 transform을 복사한 뒤에
                worldPos.name = child.name + "'s child";
                print(worldPos.name + "  pos : " + worldPos.transform.localPosition);
                //같은 부모를 둔다
                worldPos.transform.parent = child.parent;
                print(worldPos.name + "  pos : " + worldPos.transform.localPosition);
                //위치,크기,회전값을 자식과 같게 함
                worldPos.transform.localRotation = child.localRotation;
                worldPos.transform.localScale = child.localScale;
                worldPos.transform.localPosition = child.localPosition;
                // 게임 오브젝트의 transform값을 child의 bc.center의 위치로 만든다
                print(worldPos.name+ "  pos : " + worldPos.transform.localPosition);
                Quaternion q = child.localRotation;
                worldPos.transform.localPosition += q*bc.center;
                //여기까진 잘 됨

                print(worldPos.name+ " after pos : " + worldPos.transform.localPosition);
                print(worldPos.name);
                //bc.center을 로컬 좌표로 변환해줘야 된다.
                // 글로벌 y값이 0인 글로벌 포지션을 구한다
                worldPos.transform.position =new Vector3(worldPos.transform.position.x,0.2f, worldPos.transform.position.z);
                //얘한테 박스 콜라이더를 만들어주고
                worldPos.AddComponent<BoxCollider>();
                //박스콜라이더의 사이즈를 복붙한다
                worldPos.GetComponent<BoxCollider>().size = bc.size;
                //자식의 박스 콜라이더는 지운다
                bc.enabled = false;

            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
