﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JS_Enemy_Boss_001_Reksai : JS_Enemy
{
    public GameObject Stalactite;
    public float nextPhaseHpPercent = 0.5f;
    public GameObject root1;
    public GameObject root2;
    public GameObject root3;

    //시네머신일 땐 idle 데드락 체크 안함
    bool isCinemachine = true;
    protected override IEnumerator enemyFSM()
    {
        eCount = 0;
        IdleCount = 0;
        currentState = eState.Move;
        eCount++;
        while (eFSM != eState.Die)
        {
            //idle이 deadLock이 걸리면 이렇게 해결함
            if (IdleCount > 500)
            {
                //reset
                StartCoroutine(eFSM.ToString());
                break;
            }
            yield return null;
            //상태가 변하지 않으면 넘김
            if (currentState == eFSM || eFSM == eState.Damage)
            {
                yield return null;
                print("eFSM : " + eFSM + " , count : " + eCount);
                if (eFSM == eState.Idle && isCinemachine == false)
                {
                    IdleCount++;

                }
                continue;
            }
            IdleCount = 0;
            currentState = eFSM;
            print(transform.name + " 의 상태 : " + eFSM);
            /*
            case eState.Idle:
            case eState.Move:
            case eState.Attack:
            case eState.Damage:
            case eState.Die:
            */
            StartCoroutine(eFSM.ToString());
        }
        print("outoutoutoutoutoutoutoutout");
        eCount--;
    }


    bool idle2Wait = true;
    protected override IEnumerator Idle() 
    {
        //인트로 애니메이션 스킵 : true
        bool test = false;
        if(!test)
        { 
            //idle1,2를 순서대로 보여주고 카메라를 플레이어에게 다시 돌려준 다음에
            //Phase1를 발동시켜 카메라를 흔든다.
            //카메라를 흔들면서 종유석이 떨어지고 Attack으로 간다

            //방문을 열기 전까진 쉼(임시로 1초)
            yield return new WaitForSeconds(1f);
            // J키 누르기 전까지 반응 안함
            bool idleTest=true;
            while(idleTest)
            {
                yield return null;
                if(Input.GetKeyUp(KeyCode.J))
                {
                    idleTest = false;
                }
            }

            //idle1을 보여줌
            JS_Animator.SetTrigger("Idle2");
            //애니메이션이 끝날 때까지 기다림
            idle2Wait= true;
            //샤우팅함
            while (idle2Wait)
            {
                yield return null;
                if (isShake==false)
                {
                    idle2Wait = false;
                }
            }
            //캠 흔드는 거 꺼줌
            yield return new WaitForSeconds(2f);
            isShake = true;
            //운석 생성
            Stalactite.SetActive(true);
            print("drop stalactite");
            //패턴 생성
            randomPattern();
            BossUI();
        }
        //움직인다
        eFSM = eState.Move;
        isSpawned = true;
        isCinemachine = false;
        StartCoroutine(lookAtTarget());
    }

    void FindPlayer()
    {
        StartCoroutine(lookAtSmooth());
    }
    void StartShout()
    {
        StartCoroutine(shakeCam());
    }
    void EndShout()
    {
        isShake = false;
    }


    IEnumerator lookAtSmooth()
    {
        // looktime동안 lerp하게 바라봄
        float lookTime = 2f;
        float currentTime = 0;
        Vector3 dir = new Vector3(target.position.x - transform.position.x, 0, target.position.z - transform.position.z);
        while (currentTime < lookTime)
        {
            //print("forward :" + transform.forward);
            yield return null;
            //자신의 forward를 타겟으로 바꾼다
            transform.forward = Vector3.Lerp(transform.forward, dir.normalized, currentTime / lookTime);
            currentTime += Time.deltaTime;
        }
    }
    protected override IEnumerator Move()
    {

        //거리 안에 들어올 때까지 움직인다
        //움직일 때는 -1을 사용해볼 것
        while (Vector3.Distance(transform.position,target.transform.position)>eInfo.attackRange)
        {
            if (randPattern == 8)
            {
                //위에 코드에서 무조건 randPattern이 8로 됨
                print("RandPattern : " + randPattern);
                //phase2 이벤트가 끝날 때까지 코루틴을 벗어나지 않는다
                yield return StartCoroutine(Phase2());
            }
            cc.SimpleMove(transform.forward * eInfo.enemySpeed);
            yield return null;
            //움직이다가 총을 맞아서 페이즈가 넘어가게 되면
        }
        //공격 패턴 결정해서 공격하기
        randomPattern();
        eFSM = eState.Attack;

    }

    //2페이즈가 끝날 때까지 다른 행동은 안하도록 한다.
    IEnumerator Phase2()
    {
        JS_Animator.SetInteger("Attack", (int)BossPattern.Phase2);
        print("Phase222222222222222222");
        while(playPhase2 ==true)
        {
            yield return null;
        }
        playPhase2 = true;
    }

    private IEnumerator shakeCam()
    {
        //캠의 위치에서 프레임 단위로 흔들어준다
        float range = 0.5f;
        //처음 위치 기억
        Vector3 oriCamPos = playerCam.transform.position;
        //흔드는 것을 멈추고싶을 때까지
        while (isShake)
        {
            //매 프레임마다
            yield return null;
            //위치를 랜덤으로 배치
            playerCam.transform.position = oriCamPos + new Vector3(UnityEngine.Random.Range(-range, range), UnityEngine.Random.Range(-range, range), UnityEngine.Random.Range(-range, range));

        }
        playerCam.transform.position = oriCamPos;
    }

    protected override IEnumerator Attack()
    {
        //체력이 다 하기 전까진 공격한다.
        //공격 상태를 유지할 때까지 공격한다.
        while (eInfo.hp >= 0 && eFSM == eState.Attack)
        {
            JS_Animator.SetBool("AttackDelay", true);
            //페이즈2가 아닐 경우
            if (randPattern != 8)
            {
                //설정한 패턴을 코루틴으로 호출한다
                yield return StartCoroutine(BaseAttack());
                //범위 밖이면 움직이고
                print("Base End, rand :" + randPattern);
                print("Base End, eFSM :" + eFSM);
                if (randPattern != 8 && Vector3.Distance(transform.position, target.transform.position) > eInfo.attackRange)
                {
                    randPattern = -1;
                    eFSM = eState.Move;
                    print("?? eFSM :" + eFSM);
                }
                //범위 안이면 계속 공격
                else if (randPattern != 8 && Vector3.Distance(transform.position, target.transform.position) <= eInfo.attackRange)
                {
                    randomPattern();
                    eFSM = eState.Attack;
                }
            }
            //페이즈가 2일 경우
            else
            {
                //위에 코드에서 무조건 randPattern이 8로 됨
                print("RandPattern : " + randPattern);
                //phase2 이벤트가 끝날 때까지 attack코루틴을 벗어나지 않는다
                yield return StartCoroutine(Phase2());
            }
        }
    }
    //random값을 배치.
    //패턴을 랜덤으로 뿌린다
    public enum BossPattern
    {
        Fake,
        LeftPunch,
        RightPunch,
        LeftUpper,
        RightUpper,
        RightTail,
        LeftTail,
        Turn,
        Phase2,
        Landing
    }
    //-1 = Move;
    int randPattern = -1;
    int phase1_Pattern = 4;
    int phase2_Pattern = 7;
    void randomPattern()
    {
        //1페면
        if(nextPhase ==false)
        {
            //패턴 4개만
            randPattern = UnityEngine.Random.Range(0, phase1_Pattern);
        }
        //2페면 더 많이
        else
        {
            randPattern = UnityEngine.Random.Range(0, phase2_Pattern);
        }
    }
    IEnumerator BaseAttack()
    {
        //패턴을 랜덤으로 뿌린다
        randomPattern();
        //강제 패턴, 테스트용.
        if(JS_Cheats.fixedPattern)
        {
            randPattern = JS_Cheats.randPattern;
        }
        //패턴에 따른 이벤트 함수 호출
        print("pattern : " + (BossPattern)randPattern + " , num : " + randPattern);
        //페이즈가 넘어갔지만 2페이즈를 보여주지 않았을 경우
        //처음엔 n = f, p = f상태 -> n =t가 되면 이벤트가 끝날 때까지 p=f -> 이벤트가 끝나면 n= t, p = t
        if(nextPhase == true && playPhase2==false)
        {
            //패턴을 주기 전에 페이즈가 넘어가고, 패턴을 준 후 진입을 할 경우 attack애니메이션을 스킵한다.
            //이 구간은 1번만 지나간다.
            print("wait!");
            randPattern = 8;
            yield return null;
        }
        else
        {
            // nextPhase == false && playPhase2 == false  
            // nextPhase == true  && playPhase2 == true
            JS_Animator.SetInteger("Attack", randPattern);
        }
        //텀을 준다
        yield return new WaitForSeconds(0.2f);
        //공격 패턴이 끝날 때까지 멈춘다
        while (isDone ==false)
        {
            yield return null;
        }
        //텀을 준다
        yield return new WaitForSeconds(0.2f);

    }

    #region Attack Pattern
    private void LeftPunch()
    {
        for(int i=0; i<24; i++)
        {
            StartCoroutine(shotBullet(firePos[0], 0, -(150 - i * 10), 1, 0));
        }

    }
    private void RightPunch()
    {
        for (int i = 0; i < 24; i++)
        {
            StartCoroutine(shotBullet(firePos[1],0, 150 - i * 10, 1, 0));
        }

    }
    private void LeftUpper()
    {
        for (int i = 0; i < 4; i++)
        {
            //시작지점부터  앞으로 전진하면서 터짐
            StartCoroutine(shotBullet(firePos[0], i * 0.2f, -20, 1, 0));
            StartCoroutine(shotBullet(firePos[0], i * 0.2f, -10, 1, 0));
            StartCoroutine(shotBullet(firePos[0], i * 0.2f, 0, 1, 0));
            StartCoroutine(shotBullet(firePos[0], i * 0.2f, 10, 1, 0));
            StartCoroutine(shotBullet(firePos[0], i * 0.2f, 20, 1, 0));
        }

    }
    private void RightUpper()
    {
        for (int i = 0; i < 4; i++)
        {
            //시작지점부터  앞으로 전진하면서 터짐
            StartCoroutine(shotBullet(firePos[1],i * 0.2f, -20, 1, 0));
            StartCoroutine(shotBullet(firePos[1],i * 0.2f, -10, 1, 0));
            StartCoroutine(shotBullet(firePos[1],i * 0.2f, 0, 1, 0));
            StartCoroutine(shotBullet(firePos[1],i * 0.2f, 10, 1, 0));
            StartCoroutine(shotBullet(firePos[1],i * 0.2f, 20, 1, 0));
        }

    }
    private void RightTail()
    {
        StartCoroutine(shotBullet(firePos[1], 0, -5, 2.5f, 0));
    }
    private void LeftTail()
    {
        StartCoroutine(shotBullet(firePos[0], 0, 5, 2.5f, 0));
    }
    private void Turn()
    {
        for (int i =0; i<24;i++)
        {
            StartCoroutine(shotBullet(firePos[0], 0, i*15));
        }
    }
    //버로우 시작과 끝
    void StartBurrow()
    {
        JS_Animator.SetInteger("Attack", -1);
    }
    //버로우 할 때 움직이기 시작함
    void StartBurrowMove()
    {
        print("가즈아ㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏ");
        StartCoroutine(BurrowMove());
    }
    IEnumerator BurrowMove()
    {
        float currentTime = 0f;
        float LandingTime = 2f;
        while (Vector3.Distance(transform.position, target.transform.position) > eInfo.attackRange)
        {
            cc.SimpleMove(transform.forward * eInfo.enemySpeed);
            yield return null;
            //움직이다가 총을 맞아서 페이즈가 넘어가게 되면
        }
        //범위 안에 들어오면 뛰쳐나감
        print("이얍!!!");
        JS_Animator.SetInteger("Attack", (int)BossPattern.Landing);
        Vector3 pos = target.position;
        Vector3 dir = target.position - transform.position;
        //거리가 너무 가깝거나 2초에 가깝게 날았다면
        while (Vector3.Distance(transform.position, pos) >3f && currentTime<LandingTime*0.9f )
        {
            yield return null;
            currentTime += Time.deltaTime;
            print("currentTime / LandingTime : " + currentTime / LandingTime);
            transform.position = Vector3.Lerp(transform.position, pos, 0.15f * currentTime / LandingTime);
        }
        transform.position = pos - dir.normalized*2.5f;
        JS_Animator.SetBool("Landing", true);
        print("랜딩 끝!");
        randomPattern();
        yield return new WaitForSeconds(0.2f);
        playPhase2 = false;

    }


    #endregion
    bool nextPhase = false;
    protected override IEnumerator Damage(Vector3 bulletDir)
    {
        yield return null;
        //idle상태면 피 안까임
        //플레이어를 제어할 생각이다.
        //체력이 0이 넘어가면 체력이 깎임
        print("Damage");
        if (currentHp <= 0)
        {
            OnDamage = true;
            eFSM = eState.Die;
            StartCoroutine(eFSM.ToString());
        }
        //페이즈를 넘기려면 코루틴을 부름
        if (nextPhase == false && (currentHp / eInfo.hp) < nextPhaseHpPercent)
        {
            nextPhase = true;
            playPhase2 = true;
            print("NEXT!NEXT!NEXT!NEXT!NEXT!NEXT!NEXT!NEXT!NEXT!NEXT!NEXT!");
            //페이즈를 넘기는 신호를 보냄
            randPattern = (int)(BossPattern.Phase2);
            JS_Animator.SetBool("Phase_2", true);
        }
    }
    bool playPhase2=false;
    
    protected override IEnumerator Die()
    {
        //피가 까이면 죽음
        //일단 돌을 멈추게 함
        Spawn_bullet.stopStalactite=true;
        Stalactite.SetActive(false);
        //하던 동작을 멈추고
        JS_Animator.SetTrigger("Die");
        //다이로 바꾼다
        yield return null;
        print("die~");

        //콜라이더를 꺼준다   
        GetComponent<CharacterController>().enabled = false;
        //GetComponentsInChildren<BoxCollider>()

        if (GetComponent<MeshCollider>())
        {
            GetComponent<MeshCollider>().enabled = false;
        }
        root1.SetActive(false);
        root2.SetActive(false);
        root3.SetActive(false);
        StopAllCoroutines();
        Invoke("DieEvent", 3f);
    }

    /// <summary>
    /// 죽고나서 할 행동을 보여줌
    /// </summary>
    protected override void DieEvent()
    {
        StartCoroutine(smallSize());
    }
    //작아지면서 사라짐
    IEnumerator smallSize()
    {
        //원래 크기
        Vector3 oriScale = transform.localScale;
        //시간 관리
        float currentTime = 0;
        //목표 시간
        float finishTime = 2f;
        while (currentTime / finishTime < 1)
        {
            currentTime += Time.deltaTime;
            // Lerp 이용하자
            transform.localScale = Vector3.Lerp(oriScale, oriScale * 0.05f, currentTime / finishTime);
            // 중심점 문제인지 모르겠는데 죽을 때 이상하게 기어다님
            //transform.position += -Time.deltaTime * finishTime * transform.forward;
            yield return null;
        }
        Destroy(gameObject);
    }


    private bool isShake= true;

    // Start is called before the first frame update
    void Start()
    {
        //김길훈이 추가한 스크립트
        target = GameObject.Find("Player").transform;
        playerCam = GameObject.Find("Main Camera").GetComponent<Camera>();
        //기본 세팅
        Stalactite.SetActive(false);
        //캐릭터 컨트롤러
        cc = GetComponent<CharacterController>();
        JS_Animator= GetComponent<Animator>();
        //fsm 시작
        Enemy_Spawn();
    }

}

