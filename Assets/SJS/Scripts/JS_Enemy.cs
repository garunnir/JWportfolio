﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



//Enemy 부모 클래스를 만든다
//하고싶은 것 
//이 클래스를 상속시키는 것을 목적으로 함
//상속받은 애들의 기본 속성을 여기서 info함수에 대입하여 동작시키게 하고싶다
// info 스트럭쳐를 만든다
// 딜레이 함수 제작, 적의 정보 저장
//자식 클래스에서는 기본 정보를 입력, idle,move,attack,damage,die 기본 동작을 선택하게 하고싶다


/// <summary>
/// Enemy의 FSM을 상속한다
/// </summary>
public class JS_Enemy : MonoBehaviour
{
    /// <summary>
    /// 자식 클래스에서 적을 소환할 때 사용할 함수.
    /// idle상태로 enemyFSM()코루틴 호출.
    /// </summary>
    protected virtual void Enemy_Spawn()
    {
        //적을 움직이게 함
        eFSM = eState.Idle;
        currentHp = eInfo.hp;
        //보스일 경우 랜덤 패턴함수 생성
        StartCoroutine(enemyFSM());
    }

    [HideInInspector]
    public EnemyInfo eInfo = new EnemyInfo();
    /*
    //에너미 타입을 정함
    public enum EnemyType
    {
        Common,
        Boss,
        Named,
    }
    [System.Serializable]
    /// <summary>
    /// 적의 정보를 구조체로 설정함.
    /// </summary>
    public struct EnemyInfo
    {
        //enemy 기본 정보
        public EnemyType eType;     
        [Range(0f,1000f)]
        public float hp;
        [Range(0f,100f)]
        public float weight;
        [Range(0f, 1000f)]
        public float enemySpeed;
        [Range(0f,1000f)]
        public float attackRange;
       
    }
    */
    //타겟 설정
    public Transform target;
    //총구 설정
    public Transform[] firePos;
    //총알 설정
    public GameObject[] bulletFactory;
    //애니메이터 설정
    public Animator JS_Animator;
    public Animation JS_Animation;
    public PlayAnimClip playerAnim;
    //코루틴의 while문을 체크하기 위한 boolean
    protected bool isDone = true;
    //기본값 struct 생성 
    //public EnemyInfo eInfo;

    //2페이즈를 가기 위한 체력 퍼센트
    protected float setBossPhase2Hp = 0.5f;
    //현재 체력
    public float currentHp = 10f;
    //HpUI Canvas
    public Canvas HpUI;
    //적의 hp를 그릴 이미지
    public Image enemyHp;
    //적의 이름을 보여줄 텍스트
    public Text BossName;
    public Text bossCurrentHp;
    //적의 hp를 보여줄 플레이어 카메라
    public Camera playerCam;
    public bool isKnockback;
    //넉백 이펙트
    public GameObject knockbackEffect;

    public string bossName="";
    //기본적인 fsm을 따름
    // FSM : 정지, 이동, 공격, 피격, 죽음
    protected enum eState
    {
        Idle,
        Move,
        Attack,
        Damage,
        Die
    }
    protected eState eFSM = eState.Idle;
    //캐릭터 컨트롤러
    protected CharacterController cc;
    /*  기본 세팅
        void Start()
        {
            //기본 세팅
            SetEnemyInit();
            Init_AttackPattern();
            Enemy_Status(enemyInfo);
            //캐릭터 설정
            cc = GetComponent<CharacterController>();

            //fsm 시작
            Enemy_Spawn();
        }
     */

    protected eState currentState;
    protected int eCount = 0;
    protected int IdleCount = 0;
    /// <summary>
    /// enemy의 FSM을 돌리는 코루틴. 자신이 죽을 때까지 계속 fsm상태가 된다.
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator enemyFSM()
    {
        eCount = 0;
        IdleCount = 0;
        currentState = eState.Move;
        eCount++;
        while (eFSM != eState.Die)
        {
            //idle이 걸리면 이렇게 해결함
            if (IdleCount > 500)
            {
                //reset
                StartCoroutine(eFSM.ToString());
                break;
            }
            yield return null;
            //상태가 변하지 않으면 넘김
            if (currentState == eFSM || eFSM == eState.Damage)
            {
                yield return null;
                print("eFSM : " + eFSM + " , count : " + eCount);
                if(eFSM == eState.Idle)
                {
                    IdleCount++;

                }
                continue;
            }
            IdleCount = 0;
            currentState = eFSM;
            print(transform.name + " 의 상태 : " + eFSM);
            /*
            case eState.Idle:
            case eState.Move:
            case eState.Attack:
            case eState.Damage:
            case eState.Die:
            */
            StartCoroutine(eFSM.ToString());
        }
        print("outoutoutoutoutoutoutoutout");
        eCount--;
    }
    protected bool stopLookAt = false;
    /// <summary>
    /// 타겟을 바라보게 하는 함수.
    /// </summary>
    /// <returns></returns>
    protected IEnumerator lookAtTarget()
    {
        //죽을 때까지 바라봄
        while(eFSM != eState.Die)
        {
            if(stopLookAt == false)
            {
            //y를 빼고 계산
            transform.forward = new Vector3(target.position.x - transform.position.x,0, target.position.z - transform.position.z);
            }
            yield return null;
        }
    }
    /// <summary>
    /// 정지 상태일 때의 함수.
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator Idle()
    {
        yield return null;
        eFSM = eState.Move;
        /*
        while (eFSM == eState.Idle)
        {
            //방문을 열기 전까진 쉼(임시로 3초간 쉼)
            yield return new WaitForSeconds(UnityEngine.Random.Range(1.5f,3f));
            JS_Animator.SetTrigger("Idle");
            eFSM = eState.Move;
        }
        */
    }
    /// <summary>
    /// 움직일 때 사용하는 함수.
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator Move()
    {
        while (eFSM == eState.Move)
        {
            //방문이 열리면 플레이어 방향으로 이동한다
            //플레이어 방향으로 이동한다
            Vector3 dir = target.position - transform.position;
            cc.SimpleMove(dir.normalized * eInfo.enemySpeed);
            //만약 플레이어가 공격 범위 안에 있으면
            if (Vector3.Distance(transform.position, target.position) < eInfo.attackRange)
            {
                //상태를 공격으로 바꾼다
                eFSM = eState.Attack;
            }
            yield return null;
        }
    }

    /// <summary>
    /// 보스 패턴을 열거함. Init_AttackPattern()에서 정의할 수 있는 패턴을 나열함.
    /// </summary>
    // 내가 정해준 패턴에 맞춰 공격한다. 
    // 1. 패턴의 개수를 입력한다
    // 2. 패턴 순서에 bossPattern 값을 대입한다
    // 3. 그 값을 attack 상태에서 호출한다

    //여기에 공격 패턴을 추가하고, 같은 이름으로 public enum bossPattern에 추가해야 됨.
    //추후에 repeat랑 delay를 struct에 빼냄.
    protected float BaseAttackDelay = 1f;
    protected float BaseAttackRepeat = 1f;
    protected float BaseAttackWait = 1f;
    /// <summary>
    /// 공격 상태일 때의 함수. 체력을 다 하거나 공격 상태가 유지될 때까지 공격한다.
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator Attack()
    {
        //체력이 다 하기 전까진 공격한다.
        //공격 상태를 유지할 때까지 공격한다.
        while (eInfo.hp >= 0 && eFSM == eState.Attack)
        {
            //1.공격 중임을 설정
            isDone = false;
            int i = 0;
            while (i < BaseAttackRepeat)
            {
                //공격하는 타이밍에 맞춰 함수를 호출하고 싶다.
                StartCoroutine(shotBullet(firePos[0], 0.2f));
                i++;
                yield return new WaitForSeconds(BaseAttackDelay);

            }
            //공격을 끝냄
            print("기본 공격 끝!");
            isDone = true;
            //다음 공격까지 기다림
            yield return new WaitForSeconds(BaseAttackWait);
            //공격 범위 밖으로 나가면 
            if (Vector3.Distance(transform.position, target.position) > eInfo.attackDistance)
            {
                //상태를 move로 바꾼다
                //JS_Animation.CrossFade(playerAnim.move.name, 0.3f);
                JS_Animator.SetTrigger("Move");
                eFSM = eState.Move;
            }
        }
    }
    
    /// <summary>
    /// 총을 발사할 때 사용하는 함수
    /// </summary>
    /// <param name="firePos">총구의 위치</param>
    /// <param name="attackDelay">총이 발사할 때까지 걸리는 시간</param>
    /// <param name="_angle">총의 각도</param>
    /// <returns></returns>
    protected IEnumerator shotBullet(Transform firePos,float attackDelay =0f, float _angle = 0f, float bulletScale = 1f, int bulletType=0)
    {
        //대기시간만큼 기다리고
        yield return new WaitForSeconds(attackDelay);
        print("fire");
        //생성한다
        GameObject bullet = Instantiate(bulletFactory[bulletType]);
        //크기를 조절하고
        bullet.transform.localScale *= bulletScale;
        //각도를 정의하고
        Vector3 angle= Quaternion.AngleAxis(_angle, Vector3.up) * firePos.forward;
        //총알의 정면을  총구의 정면에서 _angle만큼 틀어버리게 배치하고
        bullet.transform.forward = angle.normalized;
        //총알의 위치는 임의의 총구의 위치로 배치한다
        bullet.transform.position = firePos.position;
        print("bullet done");
    }
    /// <summary>
    /// 공격 범위를 원형으로 표시attackDistance
    /// </summary>
    private void OnDrawGizmos()
    {
        //범위 설정할 때 어느정도 크기가 비슷해지면 한가지 색상만 표시
        if(Mathf.Abs(eInfo.attackDistance-eInfo.attackRange)<0.2f)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, eInfo.attackDistance);
        }
        else
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, eInfo.attackDistance);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, eInfo.attackRange);
        }
    }


    /// <summary>
    /// 피격 시 호출되는 Damage 함수. bulletDamage의 체력이 깎이면 bulletDir방향으로 밀리면서
    /// 체력이 감소한다
    /// </summary>
    /// <param name="bulletDir"></param>
    /// <param name="bulletDamage"></param>
    /// <returns></returns>
    protected virtual IEnumerator Damage(Vector3 bulletDir)
    {
        OnDamage = true;
        yield return null;
        //맞으면 총알 방향으로 밀림(벡터 크기/무게). 미구현
        //피가 까임
        JS_Animator.SetTrigger("Damage");
        print(currentHp);
        //타입이 보스이고, 1페이즈 상태에서 일정 체력 이하로 떨어질 경우 다음 페이즈를 넘김
        //나중에 보스를 더 추가할 경우 함수로 빼낼 예정
        if (currentHp <= 0)
        {
            JS_Animator.SetTrigger("Die");
            eFSM = eState.Die;
            StartCoroutine(eFSM.ToString());
        }
    }

    /// <summary>
    /// 죽으면 애니메이션을 die로 바꾸고, 콜라이더를 끈 다음에 DieEvent가 발동된다
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator Die()
    {
        //피가 까이면 죽음
        //하던 동작을 멈추고
        //JS_Animation.Stop();
        //다이로 바꾼다
        //JS_Animation.Play(playerAnim.die.name);
        yield return null;
        print("die~");
        //콜라이더를 꺼준다        
        GetComponent<CharacterController>().enabled = false;
        if (GetComponent<MeshCollider>())
        {
            GetComponent<MeshCollider>().enabled = false;
        }
        StopAllCoroutines();
        //Invoke("DieEvent", 발동 시간);
    }

    /// <summary>
    /// 죽고나서 할 행동을 보여줌
    /// </summary>
    protected virtual void DieEvent()
    {
        Destroy(gameObject);
    }

    //적의 총알에 맞았을 때 방향값, 총의 대미지를 받는 Damage 코루틴 함수를 호출
    //맞았을 때 피가 까이고, 여기서 맞을 때 바로 Damage이벤트를 실행
    protected bool isSpawned = false;
    protected bool OnDamage=false;
    private void OnTriggerEnter(Collider other)
    {
        customCollision(other);
        // 스폰된 상태(안쓸 거면 스타트 함수에서 true로 만듦)
        // 플레이어 총알에 맞을 경우
        if (isSpawned && other.gameObject.tag == "Player" && other.gameObject.layer==LayerMask.NameToLayer("Bullet"))
        {
            // 밀릴 수 있으면 밀리고
            if (isKnockback && OnDamage == false)
            {
                //넉백 함수를 호출
                Vector3 dir = other.gameObject.transform.forward;
                dir.y = 0;
                dir.Normalize();
                StartCoroutine(Knockback(dir));
            }
            //다단히트 막기
            other.gameObject.layer = 11;
            // 여기서 대미지 코루틴 호출
            if (JS_PlayerTest.JS_TestMode)
            {
                currentHp -= 1;
                // 대미지 코루틴이 끝났는지 체크
                if (OnDamage == false)
                {
                    StartCoroutine(Damage(other.transform.position));
                }
            }
            else
            {
                float bulletDamage = other.gameObject.GetComponent<JW_Bullet>().damage;
                currentHp -= bulletDamage;
                // 대미지 코루틴이 끝났는지 체크
                if (OnDamage == false)
                {
                    StartCoroutine(Damage(other.transform.position));
                }
                //총알 비활성화
                //other.gameObject.SetActive(false);
            }

        }
    }
    //넉백 시간
    public float knockbackTime = 0.45f;
    //넉백 계수
    public float kC = 0.08f;
    IEnumerator Knockback(Vector3 bulletDir)
    {
        isKnockback = false;
        //일정 시간동안 뒤로 Lerp하게 밀림
        float currentTime = 0;
        float duration=0f;
        cc.enabled = false;
        //경과 시간의 95%가 도달할 때까지
        while(duration < 1.3f)
        {
            yield return null;
            currentTime+=Time.deltaTime;
            //경과 시간 = 현재 시간/ 넉백 시간
            duration = (currentTime / knockbackTime);
            // 이동 거리 = 이동 방향 * 넉백계수/무게 * 코사인함수
            transform.position += bulletDir* (kC / eInfo.weight)*(Mathf.Cos((Mathf.PI/2)*(duration)) );
        }
        isKnockback = true;
        cc.enabled = true;

    }


    /// <summary>
    /// 나중에 추가로 충돌 이벤트를 발생시키고 싶으면 customCollision 사용
    /// </summary>
    /// <param name="other"></param>
    protected virtual void customCollision(Collider other)
    {

    }
    //보스전일 때 보스 UI를 보여주고싶다.
    public void BossUI()
    {
        print(" tpye : " + eInfo.eType);
        //보스가 아니면 리턴
        if(eInfo.eType != EnemyType.Boss)
        {
            //보스가 아니면 ui 끔
            HpUI.gameObject.SetActive(false);
            return;
        }
        
        //텍스트를 보스 이름으로 바꿈
        BossName.text = bossName;
        //hp세팅함수 호출
        StartCoroutine(enemyHpbar());
    }
    IEnumerator enemyHpbar()
    {
        //ui on
        HpUI.gameObject.SetActive(true);
        //체력이 0이 되기 전까지 현재 체력/전체 체력 표시
        while (currentHp>0)
        {
            yield return null;
            bossCurrentHp.text = currentHp.ToString("F1");
            enemyHp.fillAmount = currentHp / eInfo.hp;
        }
        //죽는 시간 기다려줌
        yield return new WaitForSeconds(2f);
        //ui off
        HpUI.gameObject.SetActive(false);

    }

    protected IEnumerator BouncySpawn()
    {
        //2초동안 scale Y값을 변화하면서 흔들게 함
        float currentTime = 0;
        //바운스 횟수를 지정한다
        int maxBounce = 6;
        int bouncCount = 0;
        bool checkBounce = false;//sin값이 0보다 작으면 false 0보다 크면 true;
        //원래 크기값
        Vector3 oriScale = transform.localScale;
        while (bouncCount < maxBounce)
        {
            //튕기는 크기의 가변값
            float downBouncy = ((float)(maxBounce - bouncCount) / (float)maxBounce);
            yield return null;
            transform.localScale = new Vector3(oriScale.x, oriScale.y * (1 + 0.3f * Mathf.Sin(15 * currentTime) * downBouncy), oriScale.z);
            //튕김의 기준 = oriscale.y값 기준으로 두 값이 커짐과 작아짐의 토글로 비교함
            //양수(true)인데 사인값이 0보다 작으면 false
            if (checkBounce == true && Mathf.Sin(15 * currentTime) < 0)
            {
                checkBounce = false;
                bouncCount++;
            }
            //음수인데 사인값이 0보다 크면 true
            if (checkBounce == false && Mathf.Sin(15 * currentTime) > 0)
            {
                checkBounce = true;
                bouncCount++;
            }
            //print("scale : " + transform.localScale.ToString("F05"));

            currentTime += Time.deltaTime;
            //끝나면 1배기준 0.1 sin(time)
        }
        //끝자락에 원래 스케일로 한다
        transform.localScale = oriScale;
    }

    //애니메이션 어택의 시작과 끝
    protected virtual void StartDelay()
    {
        print("StartDelay");
        isDone = false;
    }
    protected virtual void EndDelay()
    {
        print("EndDelay");
        JS_Animator.SetBool("AttackDelay", false);
        isDone = true;
    }
    public void StateFSM()
    {
        print("eFSM : " + eFSM);
    }
}
[System.Serializable]
public class PlayAnimClip
{
    public AnimationClip idle;
    public AnimationClip move;
    public AnimationClip attack;
    public AnimationClip damage;
    public AnimationClip die;
}