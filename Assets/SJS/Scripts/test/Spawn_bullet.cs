﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_bullet : MonoBehaviour
{
    //랜덤으로 열 곳에 돌을 소환하고 싶다
    //0.5초마다 하나씩 소환한다 
    public GameObject[] bulletFactory;
    public float range = 1;
    public float createTime =1f;
    public static bool stopStalactite= false;
    public GameObject target;

    public Vector3 dorpOffset = new Vector3(0, 10, 0);

    // Start is called before the first frame update
    void Start()
    {
        //타겟은 플레이어 
        target = GameObject.Find("Player");
        StartCoroutine("DropStalactite");
    }
    // Update is called once per frame
    IEnumerator DropStalactite()
    {
        yield return new WaitForSeconds(1f);
        while (true)  
        {
            //플레이어 머리 위에 항상 쫒아다닌다
            transform.position = target.transform.position + dorpOffset;
            //주기적으로 종유석을 스폰한다
            //만약 종유석을 멈추지 않을 경우엔 진행하고
            if (stopStalactite == false)
            {
                bulletDown();
                yield return new WaitForSeconds(createTime);
            }
            else
            {
                //멈출 때마다 0.3초마다 다시 물어본다.
                yield return new WaitForSeconds(0.3f);
            }
        }
    }

    private void bulletDown()
    {
        int rand = UnityEngine.Random.Range(0, 2);
        GameObject bullet = Instantiate(bulletFactory[rand]);
        bullet.transform.position = transform.position + new Vector3(UnityEngine.Random.Range(-range, range), 0, UnityEngine.Random.Range(-range, range));
    }

}
