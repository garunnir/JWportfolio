﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletDown : MonoBehaviour
{
    public float speed = 5f;
    public GameObject circle;
    public float gravity = 2f;
    public float circleAppear = 10f;

    public GameObject effect;
    private bool checkHit = false;
    private bool hitFloor = false;

    // Update is called once per frame
    private void Start()
    {
        checkHit = false;
        hitFloor = false;
    }
    void Update()
    {
        Vector3 dir = transform.up;
        speed += gravity*Time.deltaTime;
        transform.position += dir * speed * (Time.deltaTime);
        if(transform.position.y<-10f)
        {
            Destroy(gameObject);
        }
        drawCircle();
    }

    void drawCircle()
    {
        //y=5이하면 사라짐
        //레이를 쏴서 바닥에 sprite그림을 보여준다
        Ray ray = new Ray(transform.position , transform.up);
        RaycastHit hitinfo;
        int floorLayer = 1 << LayerMask.NameToLayer("Floor");
        if(Physics.Raycast(ray,out hitinfo,1000,floorLayer))
        {
            if(transform.position.y> circleAppear)
            {
                circle.transform.position = hitinfo.point;
            }
            if (hitFloor)
            {
                hitFloor = false;
                effect.SetActive(true);
                effect.transform.position = hitinfo.point+Vector3.up*0.2f;
                effect.transform.forward = hitinfo.normal;
                effect.transform.parent = null;
            }
        }
        //레이를 쏠 때 레이어가 Floor면 sprite를 보여준다
        //레이의 길이가 5 이하면 sprite를 끔

    }

    private void OnCollisionEnter(Collision collision)
    {
        //어디에 부딪히든 없앰
        print("col name : "+ collision.gameObject.name);
        Invoke("destroyed",0.5f);
        if (collision.gameObject.layer == LayerMask.NameToLayer("Floor") && checkHit == false)
        {
            checkHit = true;
            hitFloor = true;
        }
    }
    void destroyed()
    {
        Destroy(gameObject);
    }

}
