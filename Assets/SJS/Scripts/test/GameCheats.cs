﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameCheats : MonoBehaviour
{
    //씬을 자유자재로 넘길 수 있게 한다
    //게임을 강제로 종료할 수 있는 코드를 작성한다
    // 1.플레이어가 죽으면 Try Again
    // 또는 Menu
    // 또는 Exit Game 만든다
    public GameObject dead_UI;
    public static int cnt = 2;

    // Start is called before the first frame update
    void Awake()
    {
        Time.timeScale = 1;
    }
    void Start()
    {
        Time.timeScale = 1;
        dead_UI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //print("TimeScale : " + Time.timeScale);
        //시작 씬
        if (Input.GetKey(KeyCode.LeftAlt))
        {
            if (Input.GetKeyUp(KeyCode.Alpha1))
            {
                SceneManager.LoadScene(0);
            }
            //로딩 씬
            if (Input.GetKeyUp(KeyCode.Alpha2))
            {
                SceneManager.LoadScene(1);
            }
            //게임 씬
            if (Input.GetKeyUp(KeyCode.Alpha3))
            {
                GameManager.Instance.RestartGame();
                SceneManager.LoadScene(2);
            }
            if (Input.GetKeyUp(KeyCode.X))
            {
                cnt++;
                print("cnt : " + cnt);
            }
        }
        //플레이어가 죽으면 u키로 메뉴를 띄움
        if (Input.GetKey(KeyCode.U))
        {
            Time.timeScale = 0;
            dead_UI.SetActive(true);
        }
    }
}
