﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class __SpawnControl : MonoBehaviour
{
    public GameObject[] TestSpawn;
    string[] str;
    private void Awake()
    {
        foreach(GameObject g in TestSpawn)
        {
            g.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.LeftAlt))
        {
            if (Input.GetKeyUp(KeyCode.Alpha1))
            {
                TestSpawn[0].SetActive(true);
            }
            if (Input.GetKeyUp(KeyCode.Alpha2))
            {
                TestSpawn[1].SetActive(true);
            }
            if (Input.GetKeyUp(KeyCode.Alpha3))
            {
                TestSpawn[2].SetActive(true);
            }
            if (Input.GetKeyUp(KeyCode.Alpha4))
            {
                TestSpawn[3].SetActive(true);
            }
        }
        
    }
}
