﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


//유니티 에디터에서만 적용되게 한다
#if UNITY_EDITOR
//sample
/*
[CustomEditor(typeof(JS_test_enemy))]
public class SubEditor : JS_EnemyEditorUtility
{
    JS_test_enemy subEnemy;

    private void OnEnable()
    {
        subEnemy = target as JS_test_enemy;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        DrawBaseUI(subEnemy.GetComponentInParent<JS_Enemy>().eInfo);
    }
}
*/
//JS_Enemy_Common_001_Golem
[CustomEditor(typeof(JS_Enemy_Common_001_Golem))]
public class SubEditor1 : JS_EnemyEditorUtility
{
    JS_Enemy_Common_001_Golem subEnemy;

    private void OnEnable()
    {
        subEnemy = target as JS_Enemy_Common_001_Golem;
    }

    public override void OnInspectorGUI()
    {

        DrawBaseUI(subEnemy.GetComponentInParent<JS_Enemy>().eInfo);
        base.OnInspectorGUI();
    }
}
//JS_Enemy_Common_002_Rammus
[CustomEditor(typeof(JS_Enemy_Common_002_Rammus))]
public class SubEditor2 : JS_EnemyEditorUtility
{
    JS_Enemy_Common_002_Rammus subEnemy;

    private void OnEnable()
    {
        subEnemy = target as JS_Enemy_Common_002_Rammus;
    }

    public override void OnInspectorGUI()
    {
        DrawBaseUI(subEnemy.GetComponentInParent<JS_Enemy>().eInfo);
        base.OnInspectorGUI();
    }
}

//JS_Enemy_Boss_001_Reksai
[CustomEditor(typeof(JS_Enemy_Boss_001_Reksai))]
public class SubEditorBoss : JS_EnemyEditorUtility
{
    JS_Enemy_Boss_001_Reksai subEnemy;

    private void OnEnable()
    {
        subEnemy = target as JS_Enemy_Boss_001_Reksai;
    }

    public override void OnInspectorGUI()
    {
        DrawBaseUI(subEnemy.GetComponentInParent<JS_Enemy>().eInfo);
        base.OnInspectorGUI();
    }
}

[CustomEditor(typeof(JS_Enemy_Common_003_Cassiopeia))]
public class SubEditor3 : JS_EnemyEditorUtility
{
    JS_Enemy_Common_003_Cassiopeia subEnemy;

    private void OnEnable()
    {
        subEnemy = target as JS_Enemy_Common_003_Cassiopeia;
    }

    public override void OnInspectorGUI()
    {
        DrawBaseUI(subEnemy.GetComponentInParent<JS_Enemy>().eInfo);
        base.OnInspectorGUI();
    }
}
#endif
