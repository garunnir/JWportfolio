﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JS_Cheats : MonoBehaviour
{
    public static bool isCheats = false;
    //패턴 고정 여부
    public static bool fixedPattern = false;
    //패턴 번호
    public static int randPattern=0;

    public GameObject Enemy;
    private float ts=1f;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //치트가 꺼져있으면 작동 안하게 함
        if(isCheats == false)
        {
            if (Input.GetKey(KeyCode.Alpha1))
            {
                Enemy.GetComponent<JS_Enemy>().StateFSM();
            }
            return;
        }

        //치트가 켜져있으면 아래와 같은 커맨드로 특정 상황을 만든다
        //
        if (Input.GetKey(KeyCode.Alpha1))
        {
            randPattern = 0;
        }
        if (Input.GetKey(KeyCode.Alpha2))
        {
            randPattern = 1;
        }
        if (Input.GetKey(KeyCode.Alpha3))
        {
            randPattern = 2;
        }
        if (Input.GetKey(KeyCode.Alpha4))
        {
            randPattern = 3;
        }
        if (Input.GetKey(KeyCode.Alpha5))
        {
            randPattern = 4;
        }
        if (Input.GetKey(KeyCode.Alpha6))
        {
            randPattern = 5;
        }
        if (Input.GetKey(KeyCode.Alpha7))
        {
            randPattern = 6;
        }
        if (Input.GetKey(KeyCode.Alpha8))
        {
            randPattern = 7;
        }

        if (Input.GetKey(KeyCode.LeftAlt))
        {
            //alt + k 누르면 스케일 감소
            if (Input.GetKeyUp(KeyCode.K))
            {
                ts -= 0.05f;
                Time.timeScale = ts;
                print("TimeScale : " + Time.timeScale);
                //StartCoroutine(Enemy.GetComponentInChildren<JS_Boss_test>().Damage(Vector3.zero,1));
            }
            //alt + k 누르면 스케일 크게 함
            if (Input.GetKeyUp(KeyCode.L))
            {
                ts += 0.05f;
                Time.timeScale = ts;
                print("TimeScale : " + Time.timeScale);
                //StartCoroutine(Enemy.GetComponentInChildren<JS_Boss_test>().Damage(Vector3.zero,1));
            }
            //원래 시간으로
            if (Input.GetKeyUp(KeyCode.P))
            {
                ts =1f;
                Time.timeScale = ts;
                print("TimeScale : " + Time.timeScale);
                //StartCoroutine(Enemy.GetComponentInChildren<JS_Boss_test>().Damage(Vector3.zero,1));
            }
        }

    }
}
