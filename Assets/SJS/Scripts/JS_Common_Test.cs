﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JS_Common_Test : JS_Enemy
{
    // enemyInfo struct 창을 유니티에 띄우고싶다
    public EnemyInfo enemyInfo;
    //에너미 스테이터스를 설정
    void Start()
    {
        //김길훈이 추가한 스크립트
        target = GameObject.Find("Player").transform;
        //기본 세팅
        //적 상태 세팅
        //적 상태 설정
        //Enemy_Status(enemyInfo);
        //캐릭터 컨트롤러, 애니메이션 설정
        cc = GetComponent<CharacterController>();

        //isAnimator = true;
        //JS_Animator = GetComponentInChildren<Animator>();
        //플레이어를 바라보게 설정
        StartCoroutine(lookAtTarget());
        //fsm 시작
        Enemy_Spawn();
    }
}
