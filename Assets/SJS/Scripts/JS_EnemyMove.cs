﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JS_EnemyMove : MonoBehaviour
{
    float currentTime = 0;
    // idle 필요 속성 : 대기 시간
    public float idleTime = 3f;
    // Attack 필요 속성 : 재장전 시간, 공격 시간
    public float attackDelay=1f;
    public float reload=4f;

    //enemy 기본 정보
    public Transform target; //플레이어
    public float hp = 50f;   //체력
    public float weight = 1f;
    public float enemySpeed = 5f;
    public float attackRange = 10f;

    public GameObject bulletFactory;

    //애니메이터 호출
    Animator anim;

    // FSM : 정지, 이동, 공격, 피격, 죽음
    public enum eState
    {
        Idle,
        Move,
        Attack,
        Damage,
        Die
    }
    public eState eFSM = eState.Idle;

    // Start is called before the first frame update
    void Start()
    {
        eFSM = eState.Idle;
        //enemy의 자식(모델)에서 animator가져오기
        anim = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //죽지 않는 이상 플레이어를 항상 바라본다
        if(eFSM!= eState.Die)
        {
            transform.forward = target.transform.position - transform.position;
        }
        print(transform.name + " 의 상태 : " + eFSM);
        print(hp);
        switch(eFSM)
        {
            case eState.Idle:
                Idle();
                break;
            case eState.Move:
                Move();
                break;
            case eState.Attack:
                Attack();
                break;
            case eState.Damage:
                //Damage();
                break;
            case eState.Die:
                Die();
                break;
        }
        
    }

    private void Idle()
    {
        //방문을 열기 전까진 쉼(임시로 3초간 쉼)
        currentTime += Time.deltaTime;
        if(currentTime>idleTime)
        {
            currentTime = 0;
            eFSM = eState.Move;
            //애니메이션의 트리거를 move로 바꾼다
            anim.SetTrigger("Move");
            
        }
    }

    private void Move()
    {
        //방문이 열리면 플레이어 방향으로 이동한다
        //플레이어 방향으로 이동한다
        Vector3 dir = target.position - transform.position;
        print("dir : "+dir);
        transform.position += dir.normalized * enemySpeed * Time.deltaTime;
        //만약 플레이어가 공격 범위 안에 있으면
        if (Vector3.Distance(transform.position, target.position) < attackRange)
        {
            //상태를 공격으로 바꾼다
            eFSM = eState.Attack;

        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }


    private void Attack()
    {
        //공격 사정거리 안에 들어오면 4초마다 공격한다
        if(currentTime ==0)
        {
            print("attack");
            anim.SetTrigger("Attack");
            shotBullet();
            anim.SetTrigger("AttackDelay");
        }
        //공격 시간동안 멈춰있는다
        currentTime += Time.deltaTime;
        if(currentTime<attackDelay)
        {
            return;
        }
        //공격 딜레이가 끝나고 재장전까지 움직인다
        else if(currentTime< reload)
        {
            //일단 멈춤
            /*
            //공격 딜레이가 끝나고 재장전까지 움직인다
            Vector3 dir = target.position - transform.position;
            transform.position += dir.normalized * enemySpeed * Time.deltaTime;
            */
        }
        else
        {
            //재장전 시간이 끝나고, 
            currentTime = 0;
            //적이 공격범위 밖에 있으면 이동한다
            if (Vector3.Distance(transform.position, target.position) > attackRange)
            {
                eFSM = eState.Move;
                anim.SetTrigger("Move");
            }
        }
    }

    private void shotBullet()
    {
        GameObject bullet = Instantiate(bulletFactory);
        bullet.transform.position = transform.position;
        print("forward : "+bullet.transform.forward);
        Vector3 dir = target.position - transform.position;
        bullet.transform.forward = dir.normalized;
        print("after : " + bullet.transform.forward);

    }

    private IEnumerator Damage(Vector3 bulletDir, float bulletDamage)
    {
        yield return null;
        //맞으면 총알 방향으로 밀림(벡터 크기/무게)
        print("Damage");
        print(hp);
        //피가 까임
        hp -= bulletDamage;
        if(hp<=0)
        {
            eFSM = eState.Die;
            anim.SetTrigger("Die");
        }
        else
        {
            eFSM = eState.Move;
        }
    }

    private void Die()
    {
        //피가 까이면 죽음
        print("die~");
        //콜라이더를 모두 꺼줌   둘 다 콜라이더가 있음
        GetComponent<CharacterController>().enabled = false;
        GetComponent<CapsuleCollider>().enabled = false;
        //2D모델의 경우엔 알아서 처리
    }

    //적의 총알에 맞았을 때 방향값, 총의 대미지를 받고 Damage호출
    private void OnTriggerEnter(Collider other)
    {
        print("other.gameObject.tag : "+ other.gameObject.tag);
        if (other.gameObject.tag == "Player")
        {
            StartCoroutine(Damage(other.transform.position, 1));
            Destroy(other.gameObject);
        }
    }

}
