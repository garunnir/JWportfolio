﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JS_PlayerTest : MonoBehaviour
{
    public static bool JS_TestMode=false;
    public float speed = 5f;
    public GameObject bulletFactory;
    public float TimeScale=1;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Time.timeScale = TimeScale;

        //움직임
        playerMove();
        //발사
        fire();
    }

    private void playerMove()
    {
        Vector3 dir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        transform.position += dir * speed * Time.deltaTime;
    }

    private void fire()
    {
        if(Input.GetKeyUp(KeyCode.Space))
        {
            GameObject bullet = Instantiate(bulletFactory);
            bullet.transform.position = transform.position;
            bullet.transform.forward = transform.forward;
        }
    }
}
