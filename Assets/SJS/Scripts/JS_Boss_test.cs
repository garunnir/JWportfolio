﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 자식 기본 클래스 샘플 코드.
/// 아래의 코드 방식을 참고해서 만듦
/// 1. protected override void SetEnemyInit()로 에너미 설정을 함
/// 2. protected override void Init_AttackPattern()로 에너미 공격 패턴을 정해줌
/// </summary>
public class JS_Boss_test : JS_Enemy
{
    // enemyInfo struct 창을 유니티에 띄우고싶다
    public EnemyInfo enemyInfo;
    //에너미 스테이터스를 설정

    void Start()
    {
        //김길훈이 추가한 코드
        target = GameObject.Find("Player").transform;
        playerCam = Camera.main;
        //기본 세팅
        //적 상태 세팅
        //공격 패턴 세팅
        //적 상태 설정
        //Enemy_Status(enemyInfo);
        //보스 UI그리기
        BossUI();
        //캐릭터 컨트롤러, 애니메이션 설정
        cc = GetComponent<CharacterController>();

        //isAnimator = true;
        //JS_Animator = GetComponentInChildren<Animator>();
        //플레이어를 바라보게 설정
        StartCoroutine(lookAtTarget());
        //fsm 시작
        Enemy_Spawn();
        
    }
}
