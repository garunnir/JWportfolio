﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//충돌했을 때 shperer collider를 (파티클 라이프 사이클)초 뒤에사라지게 함
//맞았을 경우엔 적에게 대미지를 부여
public class Rammus_Bullet : MonoBehaviour
{
    public GameObject exAttack;
    public float ptTime=0.4f;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyExAttack", ptTime);
    }

    void DestroyExAttack()
    {
        Destroy(exAttack.gameObject);
    }

}
