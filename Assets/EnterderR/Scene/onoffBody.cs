﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onoffBody : MonoBehaviour
{
    public GameObject body;
    // Start is called before the first frame update
    CharacterController cc;
    BoxCollider bc;
    private void Awake()
    {
        cc = GetComponent<CharacterController>();
        bc = GetComponent<BoxCollider>();
        cc.enabled = false;
        bc.enabled = false;
    }
    void Start()
    {
        StartCoroutine(onBody());
    }

    IEnumerator onBody()
    {
        yield return new WaitForSeconds(0.5f);
        body.SetActive(true); 
        cc.enabled = true;
        bc.enabled = true;
    }
}
