﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


/// <summary>
/// 이 클래스는 공용이므로 사용할때 기재한 사람의 이름을 적고 주석을 꼼꼼히 적을것.
/// 인스팩터창에 노출할 필요없는것은 하이드인스팩터를 씁시다
/// </summary>
public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    #region 신종완 전역변수
    public Camera cam;
    [HideInInspector]
    public static Transform crosshair;
    Transform firePos;
    [HideInInspector]
    public AudioClip[] cache_AudioClips;
    [HideInInspector]
    public ParticleSystem[] cache_bulletImpacts;
    /// <summary>
    /// 0=총쏨 1=통상
    /// </summary>
    [HideInInspector]
    public RuntimeAnimatorController[] cache_Anims;
    [HideInInspector]
    Plane plane;
    /// <summary>
    /// 마우스 커서
    /// </summary>
    public enum CrossHairState
    {
        OnTheTarget,
        OnTheScreen
    }
    public static CrossHairState state;
    [HideInInspector]
    public static Vector3 indicatorDir;
    #endregion

    #region 박도담 전역변수
    public GameObject talkPanel;
    public Text talkUIText;
    public GameObject scannedObj;
    public bool isAction = false;

    public TalkManager talkManager;
    public int talkIndex;

    GameObject desk;

    public enum GameState
    {
        Ready,
        Run,
        Pause,
        GameOver
    }

    public GameState gstate;

    public GameObject gameOption;

    public GameObject stateLabel;

    Text stateText;


    #endregion

    // Start is called before the first frame update
    void Awake()
    {
        #region 싱글톤
        if (null == instance)
        {
            //이 클래스 인스턴스가 탄생했을 때 전역변수 instance에 게임매니저 인스턴스가 담겨있지 않다면, 자신을 넣어준다.
            instance = this;

            //씬 전환이 되더라도 파괴되지 않게 한다.
            //gameObject만으로도 이 스크립트가 컴포넌트로서 붙어있는 Hierarchy상의 게임오브젝트라는 뜻이지만, 
            //나는 헷갈림 방지를 위해 this를 붙여주기도 한다.
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            //만약 씬 이동이 되었는데 그 씬에도 Hierarchy에 GameMgr이 존재할 수도 있다.
            //그럴 경우엔 이전 씬에서 사용하던 인스턴스를 계속 사용해주는 경우가 많은 것 같다.
            //그래서 이미 전역변수인 instance에 인스턴스가 존재한다면 자신(새로운 씬의 GameMgr)을 삭제해준다.
            Destroy(this.gameObject);
        }
        #endregion
    }

    //게임 매니저 인스턴스에 접근할 수 있는 프로퍼티. static이므로 다른 클래스에서 맘껏 호출할 수 있다.
    public static GameManager Instance
    {
        get
        {
            if (null == instance)
            {
                return null;
            }
            return instance;
        }
    }

    void Start()
    {

        #region 신종완 start
        state = CrossHairState.OnTheTarget;
        state = CrossHairState.OnTheScreen;
        //cam = GameObject.Find("Player").transform.Find("Main Camera").GetComponent<Camera>();
        cam = Camera.main;
        firePos = GameObject.Find("Player").transform.Find("PlayerBody").Find("FirePos");
        if (!crosshair)
        {
            crosshair =Instantiate(Resources.Load<GameObject>("CrossHair")).transform;
        }
        //rd = gameObject.GetComponent<MeshRenderer>();
        //rd.material.renderQueue=1000;
        //오디오클립 캐시
        cache_AudioClips = new AudioClip[3];
        cache_AudioClips[0]= Resources.Load<AudioClip>("Sound/GunSound/Hit 1");
        cache_AudioClips[1]= Resources.Load<AudioClip>("Sound/GunSound/Hit 2"); 
        cache_AudioClips[2]= Resources.Load<AudioClip>("Sound/GunSound/Hit 3");
        cache_bulletImpacts = new ParticleSystem[1];
        cache_bulletImpacts[0]= Resources.Load<ParticleSystem>("Fx/P_Minigun_Impact");
        cache_Anims = new RuntimeAnimatorController[2];
        cache_Anims[0]= Resources.Load<RuntimeAnimatorController>("Anim/GunPlayAnim");
        cache_Anims[1]= Resources.Load<RuntimeAnimatorController>("Anim/PlayerAnim");
        
        #endregion
        #region 박도담 start
        desk = GameObject.FindGameObjectWithTag("Desk");
        if(talkPanel)
        talkPanel.SetActive(false);

        gameOption.SetActive(false);
        gstate = GameState.Ready;
        stateText = stateLabel.GetComponent<Text>();
        stateText.text = "공허의 나락";
        stateText.color = new Color32(255, 185, 0, 150);

        StartCoroutine(GameStart());

        #endregion

        StartCoroutine(GameStart());
    }



    // Update is called once per frame
    void Update()
    {
        #region 신종완 update
        CrossHair(state);
        Dir();
        #endregion

        #region 박도담 update



        #endregion


    }


    #region 신종완 함수
    private void CrossHair(CrossHairState state)
    {
        switch (state)
        {
            case CrossHairState.OnTheScreen:
                //스크린만을 받는다.
                //크로스해어의 위치 높이 0 마우스포지션
                plane = new Plane(Vector3.up, Vector3.zero);
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                float rayDistance;
                if (plane.Raycast(ray, out rayDistance))
                {
                    Vector3 tempv;
                    tempv = ray.GetPoint(rayDistance);
                    tempv.y = firePos.position.y;
                    crosshair.position = tempv;
                }


                

                break;

            //case CrossHairState.OnTheTarget:

            //    RaycastHit hitinfo;
            //    Vector3 n = Input.mousePosition;
            //    n.z = cam.nearClipPlane;
            //    //print(n);
            //    n = cam.ScreenToWorldPoint(n);
            //    Ray ray = new Ray(cam.transform.position, n - cam.transform.position);
            //    //Debug.DrawRay(cam.transform.position, n- cam.transform.position);
            //    bool result = Physics.Raycast(ray, out hitinfo, 1000, 1 << LayerMask.NameToLayer("Floor"));


            //    crosshair.position = hitinfo.point;
            //    crosshair.forward = -cam.transform.forward;
            //    if (firePos)
            //    {
            //        float tempfloat = firePos.transform.position.y / cam.transform.position.y;
            //        crosshair.position = Vector3.Lerp(hitinfo.point, cam.transform.position, tempfloat);
            //    }
            //    else
            //    {
            //        crosshair.position = crosshair.position + Vector3.up * 0.1f;

            //    }
            //    //print(hitinfo.point);



            //    //이 크로스헤어는 스크린상에만 존재하는 방식과 레이에 닿은 위치에 붙는 방식 두가지 모드를 변경할수 있어야 한다.

            //    break;
        }


    }
    private void Dir()
    {
        indicatorDir = crosshair.transform.position - firePos.transform.position;
    }
    #endregion

    #region 송재성 전역변수
    //하고싶은 일
    //몬스터를 배치하고싶다.
    //보스 방에 몬스터를 배치하고싶다.
    #endregion
    #region 송재성 함수
    #endregion
    #region 박도담 함수
    public void Action(GameObject scanObject)
    {
        scannedObj = scanObject;
        // 스캔한 오브젝트의 Id와 isNPC 정보를 가져와야 한다.
        ObjData objData = scannedObj.GetComponent<ObjData>();
        
        if (scannedObj.tag == "Desk")
        {
            GameObject.Find("Desk").GetComponent<Desk>().deskSwitch = true;
        }
        else
        {
            // objData의 Id와 NPC 정보를 매개변수로 넘긴다.          
            Talk(objData.id, objData.isNPC);
        }
        talkPanel.SetActive(isAction);
    }

    // 실제 대사를을 UI에 출력하는 함수
    void Talk(int id, bool isNPC)
    {
        // TalkManager에 생성한 함수들을 다시 반환하는 함수를 이용하여 대사를 입력받음
        string talkData = talkManager.GetTalk(id, talkIndex);

        // null이면 더 이상 남은 대사가 없으므로 action을 false로 변경한다.
        if (talkData == null)
        {
            isAction = false;
            // talkIndex 초기화
            talkIndex = 0;
            return;
        }
        // 입력받은 대사를 출력
        if (isNPC)
        {
            talkUIText.text = talkData;
        }
        else
        {
            talkUIText.text = talkData;
        }

        // 다음 문장을 가져오기 위해 talkData의 인덱스를 늘림
        isAction = true;
        talkIndex++;
    }

    IEnumerator GameStart()
    {
        int count = 0;
        while (count < 5)
        {
            stateLabel.SetActive(true);
            yield return new WaitForSeconds(0.4f);
            stateLabel.SetActive(false);
            yield return new WaitForSeconds(0.4f);
            count++;
        }
        gstate = GameState.Run;
    }

    public void OpenOptionWindow()
    {
        gameOption.SetActive(true);
        Time.timeScale = 0f;
        gstate = GameState.Pause;
    }

    public void CloseOptionWindow()
    {
        gameOption.SetActive(false);
        Time.timeScale = 1f;
        gstate = GameState.Run;
    }

    public void RestartGame()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
    #endregion

}
