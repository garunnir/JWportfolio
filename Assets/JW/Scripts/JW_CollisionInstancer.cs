﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random= UnityEngine.Random;

// ReSharper disable once CheckNamespace

    [RequireComponent(typeof(ParticleSystem))]
public class JW_CollisionInstancer : MonoBehaviour
{
    public ParticleSystem CollisionFx;
    public float LifeTime = 1.5f;
    public Vector3 RotationOffset;
    SphereCollider col;
    public float damage=0;//댐지전달

    private ParticleSystem _mainPs;
    private readonly List<ParticleCollisionEvent> _collisionEvents = new List<ParticleCollisionEvent>();

    AudioSource audio;
    GameManager gm;

    private void Awake()
    {
        _mainPs = GetComponent<ParticleSystem>();
        
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    private void OnParticleCollision(GameObject other)
    {
        int collisionEventsCount = _mainPs.GetCollisionEvents(other, _collisionEvents);

        if (collisionEventsCount <= 0)
            return;
        print(other);
        var collisionFx = Instantiate(CollisionFx, _collisionEvents[0].intersection, Quaternion.identity);

        collisionFx.transform.LookAt(_collisionEvents[0].intersection + _collisionEvents[0].normal);
        collisionFx.transform.rotation *= Quaternion.Euler(RotationOffset);

        GameObject a = collisionFx.gameObject;
        a.tag = "Player";
        a.layer = LayerMask.NameToLayer("Bullet");
        col=a.AddComponent<SphereCollider>();
        col.isTrigger = true;
        col.radius = 0.1f;
        int i=Random.Range(1, 3);
        audio = a.GetComponent<AudioSource>();
        audio.clip = gm.cache_AudioClips[i];
        audio.Play();
        
        JW_Bullet bullet= a.AddComponent<JW_Bullet>();
        bullet.dirOfAttack = -transform.up;
        bullet.state = JW_Bullet.ShellType.Explosion;
        bullet.damage = damage;

        Destroy(col,1f);
        Destroy(collisionFx.gameObject, LifeTime);
    }
}

