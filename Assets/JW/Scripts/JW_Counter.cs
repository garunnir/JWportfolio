﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JW_Counter : MonoBehaviour
{


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EnemyBullet" && other.gameObject.layer == LayerMask.NameToLayer("Bullet"))
        {
            other.transform.forward = GameManager.indicatorDir;
            other.gameObject.tag = "Player";
            other.gameObject.layer = LayerMask.NameToLayer("Bullet");
            JW_Bullet a = other.gameObject.AddComponent<JW_Bullet>();
            a.damage = 3;
            a.state = JW_Bullet.ShellType.DamageOnly;
        }
    }
}
