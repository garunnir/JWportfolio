﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JW_TestDebug : MonoBehaviour
{
    Weapon w;
    // Start is called before the first frame update
    void Start()
    {
        //itemTest();

    }

    // Update is called once per frame
    void Update()
    {

    }
    void itemTest()
    {
        GameObject n = new GameObject("Item");
        SphereCollider sp = n.AddComponent<SphereCollider>();
        sp.isTrigger = true;
        JW_NonMonobehavior a = n.AddComponent<JW_NonMonobehavior>();
        a.item = new Weapon_Granadeluncher();
        n.tag = "Item";
    }
    void DamageTest()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        print(other.name);
        Component[] a = other.GetComponents<Component>();
        foreach(Component c in a)
        {
            print(c.name);
        }
        JW_Bullet b = other.GetComponent<JW_Bullet>();
        print(b.damage);

    }
    private void OnCollisionEnter(Collision collision)
    {
        GameObject other = collision.gameObject;
        print(other.name);
        Component[] a = other.GetComponents<Component>();
        foreach (Component c in a)
        {
            print(c.name);
        }
        JW_Bullet b = other.GetComponent<JW_Bullet>();
        print(b.damage);
    }
}
