﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//아이템오브젝트에 이 스크립트를 단다.
public class JW_ItemBox : MonoBehaviour
{
    public Item item;
    // Start is called before the first frame update
    void Start()
    {
        item = new Weapon_Granadeluncher();
        gameObject.layer = LayerMask.NameToLayer("Item");
        if (item as Weapon != null)
        {
            gameObject.tag = "Weapon";
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
