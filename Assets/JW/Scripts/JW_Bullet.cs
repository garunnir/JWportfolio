﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class JW_Bullet : MonoBehaviour
{
    public ParticleSystem boomfx;
    public float damage=0;//무기로부터 받음
    public float damage1 = 0;//다음피해로 전달
    public float explosionRange = 1;
    public float knockBackRate = 0;//적으로 전달
    public float knockBackTime = 0;//적으로 전달
    [HideInInspector]
    public Vector3 dirOfAttack;//피격방향
    AudioSource bulletAudio;
    public enum ShellType
    {
        dumbdumb,
        Pierce,
        Grenade,
        Explosion,
        DamageOnly
    }
    public ShellType state=ShellType.dumbdumb;
    public float knockBack=1;
    public float bulletSpeed=1;
    // Start is called before the first frame update
    void Start()
    {
        bulletAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case ShellType.dumbdumb:
                ShellMove();
                break;

            case ShellType.Grenade:
                ShellMove();
                break;

            case ShellType.Pierce:
                ShellMove();
                break;

            case ShellType.Explosion:

                break;
        }

        
    }

    private void ShellMove()
    {
        transform.position += transform.forward * bulletSpeed * 3 * Time.deltaTime;
    }


    public void OnTriggerEnter(Collider collision)
    {
        
        //dirOfAttack = transform.forward;
        //print(collision.gameObject.name);
        //닿은것에 데미지를 부여한다

        //대충 에너미 스크립트에 접근하는 코드
        //float enemyHp=10;
        //enemyHp=enemyHp-damage;

        //닿으면 피격 이미지를 생성한다
        //닿은것을 밀어낸다.
        //적을 러프로 맞은방향으로 밀어낸다
        //구현1 적을 총알의 자식으로 일정시간 만든다
        //어려움 이유는 총알이 꺼지면 자식도 사라지기때문
        //구현2 넉백은 총알에서 전달하고 밀려나는구문은 적 스크립트쪽에서 구현한다
        //좀더 스마트한 방법이 있을것 같다.
        //구현3 예를들면 생성되었다 소멸하는 스크립트를 붙인다던가..?
        //


        //재성에게 데미지 판정은 총알에서 알려줄것이라고 하기
        //재성에게 에너미의 hp를 설정해달라하기
        //if (collision.gameObject.layer == LayerMask.NameToLayer("Enemy") << 1)
        //{
        //    instanceClass a=collision.gameObject.AddComponent<instanceClass>();
        //    a.knockBackRate = knockBackRate;
        //    a.knockBackTime = knockBackTime;

        //}

        //유탄의 경우 충돌하거나 일정시간후 이 오브젝트를 끄고 (원형의 콜라이더를 생성한다 그 크기만큼의 이팩트를 인스턴스한다)의 프리팹을 생성한다.
        //if (!(collision.tag == "Player"))
        //{
        //    switch (state)
        //    {
        //        case ShellType.dumbdumb:
        //            gameObject.SetActive(false);
        //            //GameObject a = Instantiate(GameManager.Instance.cache_bulletImpacts[0].gameObject,transform.position,collision.);
        //            Collider dd=collision.GetComponent<Collider>();
                    
        //            //AudioSource b =a.GetComponent<AudioSource>();
        //            //b.clip = GameManager.Instance.cache_AudioClips[0];
        //            //b.Play();
        //            break;

        //        case ShellType.Grenade:
        //            gameObject.SetActive(false);
        //            GameObject temobj = Instantiate(boomfx, transform.position, Quaternion.identity).gameObject;
        //            temobj.transform.localScale = Vector3.one * explosionRange;
        //            JW_SelfDestroyer sd = temobj.AddComponent<JW_SelfDestroyer>();
        //            ParticleSystem ps = temobj.GetComponent<ParticleSystem>();
        //            sd.lifeTime = ps.main.duration;
        //            JW_Bullet bullet=temobj.AddComponent<JW_Bullet>();
        //            bullet.state = ShellType.Explosion;
        //            bullet.damage = damage1;//폭발대미지
                    

        //            //생성된 친구의 콜라이더를 일정타이밍에 켜주고싶다.

        //            //print("duration" + sd.lifeTime);

        //            //생성후 일정시간후 제거
        //            break;

        //        case ShellType.Pierce:

        //            break;
        //    }
        //}

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (!(collision.gameObject.tag == "Player"))
        {
            switch (state)
            {
                case ShellType.dumbdumb:
                    gameObject.SetActive(false);
                    GameObject a = Instantiate(GameManager.Instance.cache_bulletImpacts[0].gameObject,transform.position,Quaternion.identity);
                    a.transform.up = collision.contacts[0].normal;
                    a.transform.position += -a.transform.up*0.3f;
                    Collider dd = collision.gameObject.GetComponent<Collider>();

                    AudioSource b =a.GetComponent<AudioSource>();
                    b.clip = GameManager.Instance.cache_AudioClips[0];
                    b.Play();
                    break;

                case ShellType.Grenade:
                    gameObject.SetActive(false);
                    GameObject temobj = Instantiate(boomfx, transform.position, Quaternion.identity).gameObject;
                    temobj.transform.localScale = Vector3.one * explosionRange;
                    JW_SelfDestroyer sd = temobj.AddComponent<JW_SelfDestroyer>();
                    ParticleSystem ps = temobj.GetComponent<ParticleSystem>();
                    sd.lifeTime = ps.main.duration;
                    JW_Bullet bullet = temobj.AddComponent<JW_Bullet>();
                    bullet.state = ShellType.Explosion;
                    bullet.damage = damage1;//폭발대미지


                    //생성된 친구의 콜라이더를 일정타이밍에 켜주고싶다.

                    //print("duration" + sd.lifeTime);

                    //생성후 일정시간후 제거
                    break;

                case ShellType.Pierce:

                    break;

                case ShellType.DamageOnly:

                    break;
            }
        }
    }
}
public class instanceClass:MonoBehaviour
{
    public float knockBackRate=1;
    public float knockBackTime = 1;
    public Vector3 knockBackDir;
     void Start()
    {
        //다른 움직임 관련 스크립트 끄기
        //대충 적 무브 스크립트
    }
     void Update()
    {
        KnockBack(knockBackDir);
    }
    //넉백되는 친구에게 붙는 임시 스크립트
    void KnockBack(Vector3 knockBackDir)
    {
        transform.position += knockBackDir * knockBackRate * Time.deltaTime;
        Invoke("DestroyClass", knockBackTime);
    }

    IEnumerator DestroyClass()
    {
        //적 무브 다시 켜주기

        //이 클래스 삭제

        Destroy(gameObject.GetComponent<instanceClass>());
        yield return new WaitForFixedUpdate();
    }

}
