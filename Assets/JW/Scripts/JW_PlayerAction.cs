﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class JW_PlayerAction : MonoBehaviour
{
    public static int HP=6;

    public int maxHaveWeapons = 6;
    public List<Weapon> weaponList;
    public float moveSpeed=3;
    public float camMoveSpeed=2;
    public Camera cam;
    public float uiYPos=2;

    public GameObject rightHand;
    public GameObject leftHand;
    public Transform firePos;
    [HideInInspector]
    public float fowardMatchRate;
    [HideInInspector]
    public float RightMatchRate;
    public Weapon weapon;
    M2Action PlayerRoll;
    GameObject playerCanvas;
    GameObject runtimeCanvas;
    GameObject weaponID;
    static GameObject playerHpUI;
    GameObject bulletRemainInBoxHUD;
    Text shellAmountTxt;
    static Animator anim;
    [HideInInspector]
    public bool isPause;

    public Vector3 camOffset;
    public Vector3 camRotateOffset;

    public Vector3 playerMoveDir;
    public Vector3 attackedPos;
    public GameObject Img_GameOver;
    public enum WeaponState
    {
        Melee,
        Pistol
    }
    WeaponState state;

    GameObject playerBody;
    Vector3 camOriginLocalPos;
    GameManager GM;
    int weaponIndex=0;

    Image img_weapon;
    Image img_bullet;

    AudioSource playerWeaponSound;
    
    Rigidbody rb;
    private float detectRange = 1f;
    private int layerMask;
    private GameObject scannedObj;
    private Renderer mr;
    public ParticleSystem exclamationMark;

    [HideInInspector]
    public bool stopPlayerRot = false;
    [HideInInspector]
    public bool stopPlayerMove = false;
    [HideInInspector]
    public Collider[] playerCol;
    Material norMat;
    Material shadeMat;


    public List<GameObject> observedTriggerObjects;

    static public bool isOnTrigger = false;

    public delegate void Mydel(MouseLeftClickState state);
    public Mydel Attack;

    public delegate void DamageDel(int damage);
    public static DamageDel Damage;
    public int observedOBJ=0;
    // Start is called before the first frame update
    void Start()
    {
        Img_GameOver = GameObject.Find("Img_GameOver");
        Img_GameOver.SetActive( false);
        observedTriggerObjects = new List<GameObject>();
        anim = gameObject.GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        layerMask = 1 << LayerMask.NameToLayer("Object");
        weaponList = new List<Weapon>();
        Weapon a = new Weapon_Pistol();//기본무기
        weaponList.Add(a);
        Weapon b = new Weapon_Granadeluncher();
        weaponList.Add(b);
        Weapon c = new Weapon_Minigun();
        weaponList.Add(c);
        weapon = weaponList[1];

        exclamationMark = GameObject.Find("Player").transform.Find("PlayerCanvas").Find("Fx_!").GetComponent<ParticleSystem>();

        playerBody = GameObject.Find("Player").transform.Find("PlayerBody").gameObject;
        //카메라
        cam = GameObject.Find("Player").transform.Find("Main Camera").GetComponent<Camera>();
        camOriginLocalPos = cam.transform.localPosition;
        camRotateOffset = cam.transform.localEulerAngles;
        cam.transform.parent = null;

        GM = GameObject.Find("GameManager").GetComponent<GameManager>();
        print(GM.name);
        if (!firePos)
        {
            firePos=transform.Find("PlayerBody").Find("FirePos");
        }
        if (!playerCanvas)
        {
            playerCanvas = transform.Find("PlayerCanvas").gameObject;
            playerCanvas.transform.localPosition =playerBody.transform.localPosition+Vector3.up*uiYPos;
        }
        if (!runtimeCanvas)
        {
            runtimeCanvas = GameObject.Find("RuntimeCanvas");
            weaponID = runtimeCanvas.transform.Find("ID:Weapon_Name").gameObject;
            //bulletRemainInBoxHUD = weaponID.transform.Find("Grp_WeaponUI").Find("Grp_ShellRemain").gameObject;
            bulletRemainInBoxHUD = GameObject.Find("RuntimeCanvas").transform.Find("ID:Weapon_Name").Find("Grp_WeaponUI").Find("Grp_ShellRemain").gameObject;
            if (!playerHpUI)
            {
                playerHpUI = runtimeCanvas.transform.Find("Grp_PlayerHP").gameObject;
            }
            if (!shellAmountTxt)
            {
                shellAmountTxt = weaponID.transform.Find("Grp_ShellRemainTxt").Find("Text").GetComponent<UnityEngine.UI.Text>();
            }
            if (!img_weapon)
            {
                img_weapon = GameObject.Find("RuntimeCanvas").transform.Find("ID:Weapon_Name").Find("Grp_WeaponUI").Find("WeaponSelect").Find("Grp_CurrentWeapon").Find("Img_Weapon").GetComponent<Image>();
            }
        }


        for(int i=0; i < bulletRemainInBoxHUD.transform.childCount; i++)
        {
        bulletRemainInBoxHUD.transform.GetChild(i).gameObject.SetActive(false);
        }
        for (int i = 0; i < playerHpUI.transform.childCount; i++)
        {
            playerHpUI.transform.GetChild(i).gameObject.SetActive(false);
        }
        CallPlayerHpUI();
        CallShellAmountInBox();

        playerCol =playerBody.transform.Find("PlayerCollider").GetComponents<Collider>();

        norMat = Resources.Load<Material>("Materials/Assasine");
        shadeMat = Resources.Load<Material>("Materials/invincible");

        //사운드
        playerWeaponSound = gameObject.GetComponent<AudioSource>();


        Attack = weapon.Attack;
        //무기를 들고있을때 그 총알갯수를 읽고 그만큼 활성화해준다.

        //weapon= new Weapon_Pistol(firePos);
        //weapon = new Weapon_Granadeluncher(this,firePos);
        //weapon = new Weapon_Minigun(firePos);
        PlayerRoll = new M2Action();
        //테스트용 임시 초기값
        isPause = false;
        state = WeaponState.Pistol;
        mr = GetComponentInChildren<SkinnedMeshRenderer>();

        Damage = PlayerDamage;
    }

    // Update is called once per frame
    void Update()
    {
        AutoPlayerLock();
        PlayerMove();
        PlayerAttack();
        PlayerKeyAction();
        invincibleTimer(1);
        PlayerCanvas();
        PlayerCamMove();


    }
    public static bool invincible = false;
    /// <summary>
    ///여기에 데미지를 넣으면 계산해서 플레이어가 데미지를 입는다
    /// </summary>
    /// <param name="damage">몹의 공격력</param>
    public void PlayerDamage(int damage)
    {
        //반감을넣고 데미지감소를 넣는다. 최소 1의 데미지를 받는다.
        if (invincibleSelf|| invincible)
            return;
        HP -= damage;
        invincibleSelf = true;
        NockBack();
        if (HP <= 0)
        {
            Die();
        }
        CallPlayerHpUI();
        
        //무적상태에선 반격이 발동하지 않는다.
        
        
    }

    private void Die()
    {
        //플레이어 사망
        anim.runtimeAnimatorController = GM.cache_Anims[1];
        playerCol[0].enabled = false;
        playerCol[1].enabled = false;
        anim.SetTrigger("Die");
        ActionStateStatic.IsAction[3] = true;
        Img_GameOver.SetActive(true);
    }

    public void NockBack()
    {
        anim.SetTrigger("Hit");

        //무적시간안에 플레이어가 밀려난다
        StartCoroutine(NockBackCo());

    }
    
    IEnumerator NockBackCo()
    {
        //이동안 조종은 불가하다
        ActionStateStatic.IsAction[2] = true;
        //그 충돌체와 자신의 위치를 비교해 방향을 산출한다
        Vector3 tempos= attackedPos - transform.position;
        tempos.y = 0;
        //피격 방향으로 회전한다
        transform.forward = tempos;
        //피격 반대방향으로 이동
        //시작속도
        float tempspeed=5;
        //시작속도는 줄어든다
        while (invincibleSelf)
        {
        transform.position -= tempos.normalized* Mathf.Max(tempspeed,0) * Time.deltaTime;
            tempspeed -= 0.3f;

            //지속시간후 탈출
        yield return new WaitForEndOfFrame();
        }
        ActionStateStatic.IsAction[2] = false;
    }


    float tempTime=0;
    void invincibleTimer(float duration)
    {
        //일정시간이지나면 자동으로 무적이 꺼진다.
        if (invincibleSelf)
        {
            //플레이어가 흐릿해진다.
            mr.material = shadeMat;
            tempTime += Time.deltaTime;
            if (tempTime > duration)
            {
                invincibleSelf = false;
                mr.material = norMat;
                tempTime = 0;
            }
        }


    }
    /// <summary>
    /// 뎀지입을때 혹은 회복했을때 같이 호출할것
    /// </summary>
    public static void CallPlayerHpUI()
    {
        for (int i = 0; i < playerHpUI.transform.childCount; i++)
            if (i < HP)
            {
                playerHpUI.transform.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                playerHpUI.transform.GetChild(i).gameObject.SetActive(false);
            }
    }
    static public bool invincibleSelf = false;
    
    /// <summary>
    /// 무기스위칭
    /// </summary>
    /// <param name="intiger">0은 up 1은 down</param>
    private void PlayerWeaponSwitch(int intiger)
    {
        if (weaponList.Count == weaponIndex)
        {
            weaponIndex = 0;
        }
        if(weaponIndex == -1)
        {
            weaponIndex = weaponList.Count-1;
        }
        if (intiger == 0)
        {
            weapon.weapon.SetActive(false);
            weapon = weaponList[weaponIndex];
            Attack = weapon.Attack;
            weapon.weapon.SetActive(true);
            weaponIndex++;
            CallShellAmountInBox();
        }
        else if (intiger == 1)
        {
            weapon.weapon.SetActive(false);
            weapon = weaponList[weaponIndex];
            Attack = weapon.Attack;
            weapon.weapon.SetActive(true);
            weaponIndex--;
            CallShellAmountInBox();
        }
    }

    private void PlayerCanvas()
    {
        //플레이어 위에 표시 월드유아이
        playerCanvas.transform.forward = cam.transform.forward;

        //스크린 유아이
        //ui 총알개수표시 총알 갯수가 달라졌을때만 읽어들인다 요청될때 읽는다.
        

        
    }
    /// <summary>
    /// 플레이어 무기 캔버스의 변경이 있었을때 탄을 사용했을때 혹은 탄창을 채웠을때 같이 호출할것
    /// </summary>
    public void CallShellAmountInBox()
    {
        for (int i = 0; i < bulletRemainInBoxHUD.transform.childCount; i++)
            if (i < weapon.remainShellsInBox)
            {
                bulletRemainInBoxHUD.transform.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                bulletRemainInBoxHUD.transform.GetChild(i).gameObject.SetActive(false);
            }
        shellAmountTxt.text = weapon.remainShells + "/" + weapon.remainShellsTotal;
        img_weapon.sprite = weapon.weaponSprite;

        
    }



    private void PlayerKeyAction()//키액션 제어
    {
        if (stopPlayerAllActions)
            return;

        if (Input.GetButtonUp("Fire1"))
        {
            Attack(MouseLeftClickState.Click);
        }
        else if (Input.GetButton("Fire1"))
        {
            Attack(MouseLeftClickState.Hold);
            //마우스 지속
        }
        else if (Input.GetButtonDown("Fire1"))
        {
            Attack(MouseLeftClickState.Click);
        }
        if (Input.GetButtonDown("Fire2"))
        {
            PlayerRoll.M2Act(MouseRightClickState.Click);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            weapon.Reload();
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            //가장 가까운 물체와 상호작용한다.
            if(scannedObj != null)
            {
                PlayerObj();
            }
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (isPause == false)
            {
                GM.OpenOptionWindow();
                isPause = true;
                return;
            }
            if (isPause != false)
            {
                GM.CloseOptionWindow();
                isPause = false;
                return;
            }
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            PlayerWeaponSwitch(0);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            PlayerWeaponSwitch(1);
        }

    }
    bool stopPlayerAllActions = false;
    void AutoPlayerLock()
    {
        if(ActionStateStatic.IsAction[0]|| ActionStateStatic.IsAction[1]|| ActionStateStatic.IsAction[2]|| ActionStateStatic.IsAction[3])
        {
            stopPlayerRot = true;
            stopPlayerMove = true;
            if (ActionStateStatic.IsAction[3])
            {
                stopPlayerAllActions = true;
            }
        }
        else
        {
            stopPlayerRot = false;
            stopPlayerMove = false;
        }
    }

    float attackTime=0;
    float currentTime = 0;
    private void PlayerAttack()//플레이어 모션과 무기아이템 제어
    {
        currentTime += Time.deltaTime;
        switch(state)
        {
            case WeaponState.Melee:

                break;
            case WeaponState.Pistol:
                //필요요소
                //발사속도
                //탄속
                //넉백
                //탄잔량
                //탄창크기
                //재장전속도

                if (currentTime > attackTime)
                {
                    if (Input.GetButtonUp("Fire1"))
                    {

                    }
                    else if (Input.GetButton("Fire1"))
                    {
                        //마우스 지속
                    }
                    else if (Input.GetButtonDown("Fire1"))
                    {

                    }
                }

                break;


        }

    }
    private void PlayerMove()
    {

        // 대화가 진행중이면 플레이어 이동을 막는다.
        if(GM.isAction == true||stopPlayerMove)
            return;

        playerMoveDir.x = Input.GetAxis("Horizontal");
        playerMoveDir.z = Input.GetAxis("Vertical");
        playerMoveDir.y = 0;
        anim.SetFloat("Speed", playerMoveDir.magnitude);
        anim.SetFloat("forward", Vector3.Dot(playerMoveDir, playerBody.transform.forward));
        anim.SetFloat("right", Vector3.Dot(playerMoveDir, playerBody.transform.right));
            transform.position += playerMoveDir.normalized * moveSpeed * Time.deltaTime;
        
    }

    void PlayerCamMove()
    {
        Vector3 tempos;
        tempos.z = MousePosCenter.y;
        tempos.x = MousePosCenter.x;
        //tempos.y = camOriginLocalPos.y;
        tempos.y = 0;
        camOriginLocalPos.x = transform.position.x;
        camOriginLocalPos.z = transform.position.z;
        // 대화가 진행중이면 회전을 막는다.
        if(GM.isAction == true)
            return;

        //특정수치만큼 천천히 마우스방향을 따라 방향을 옮긴다
        //일정범위내라면 회전을 멈춘다
            //마우스 입력한쪽을 바디가 바라본다
            cam.transform.localPosition = camOriginLocalPos + tempos * camMoveSpeed + camOffset;
            cam.transform.eulerAngles = camRotateOffset;
            Vector3 tmpos = GameManager.crosshair.position;
            tmpos.y = transform.position.y;

        //일치율
        if (!stopPlayerRot)
        {
            if (false)
            {
                float correctRate = 0.99f;
                if (Vector3.Dot(transform.forward, (tmpos - transform.position).normalized) < correctRate)
                {
                    float rightRot = Vector3.Dot(transform.right, (tmpos - transform.position).normalized);
                    float leftRot = Vector3.Dot(-transform.right, (tmpos - transform.position).normalized);
                    //일정범위 오차 안이 아니라면
                    if (rightRot > leftRot)
                    {
                        //오른쪽으로돈다
                        transform.rotation = Quaternion.AngleAxis(0.3f, transform.up) * transform.rotation;
                    }
                    else
                    {
                        //왼쪽으로돈다.
                        transform.rotation = Quaternion.AngleAxis(-0.3f, transform.up) * transform.rotation;
                    }
                    anim.SetFloat("rightTurn", rightRot - leftRot);
                }
                else
                {
                    anim.SetFloat("rightTurn", 0);
                }
            }
            else
            {
                float correctRate = 0.99f;
                transform.forward = tmpos - transform.position;
                if (Vector3.Dot(transform.forward, (tmpos - transform.position).normalized) < correctRate)
                {
                    float rightRot = Vector3.Dot(transform.right, (tmpos - transform.position).normalized);
                    float leftRot = Vector3.Dot(-transform.right, (tmpos - transform.position).normalized);
                    //일정범위 오차 안이 아니라면

                    anim.SetFloat("rightTurn", rightRot - leftRot);
                }
                else
                {
                    //anim.SetFloat("rightTurn", 0);
                }
            }




        }

    }

    private void FixedUpdate()
    {
        //// Layer가 Object인 물체만 raycasthit 감지
        //RaycastHit hitInfo = new RaycastHit();
        //Physics.Raycast(rb.transform.position, rb.transform.TransformDirection(Vector3.forward), out hitInfo, detectRange, layerMask);

        //// 감지되면 scannedObj에 오브젝트 저장
        //if(hitInfo.collider != null)
        //{
        //    scannedObj = hitInfo.collider.gameObject;
        //}
        //else
        //{
        //    scannedObj = null;
        //}
    }

    void PlayerObj()
    {
        if(scannedObj != null)
        {
            GM.Action(scannedObj);
        }
    }

    //이것을 호출하면 마우스의 중심을 기준으로 v2 xy 위치를 알려준다
    Vector2 MousePosCenter
    {
        get
        {
            Vector2 answer;
            Vector2 mouse = Input.mousePosition;

            answer.x = (mouse.x -Screen.width/2)/Screen.width;
            answer.y = (mouse.y - Screen.height/2)/Screen.height;

            return answer;

        }
    }
    //mousposcenter 프로퍼티를 월드공간에 맞게 바꿔준다
    Vector3 MouseCenterPosInWorld
    {
        get
        {
        Vector3 answer;
        answer.x = MousePosCenter.x;
        answer.z = MousePosCenter.y;
        answer.y = 0;
        return answer;
        }
    }

    

    #region 아이템 자동습득 함수
    public void AutoItemGet(Collider other)
    {
        if (other.tag == "Item")
        {
            JW_ItemBox nb = other.GetComponent<JW_ItemBox>();
            Weapon wp = nb.item as Weapon;
            if (wp != null)
            {
                if (weaponList.Count < maxHaveWeapons)
                {
                    weaponList.Add(wp);
                }
            }

            //print(wp.damage);
        }
    }
    public void AutoItemGet(Collision other)
    {
        if (other.gameObject.tag == "Item")
        {
            JW_ItemBox nb = other.gameObject.GetComponent<JW_ItemBox>();
            Weapon wp = nb.item as Weapon;
            if (wp != null)
            {
                if (weaponList.Count < maxHaveWeapons)
                {
                    weaponList.Add(wp);
                }
            }

            //print(wp.damage);
        }
    }
    #endregion
    public void Slash()
    {
        print("v파파팍");
    }

}
