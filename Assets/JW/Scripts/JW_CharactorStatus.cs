﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharactorStatus
{
    /// <summary>
    /// 데미지
    /// </summary>
    public float damage=0;
    /// <summary>
    /// 발사속도
    /// </summary>
    public float attackSpeed=0;
    /// <summary>
    /// 탄속
    /// </summary>
    public float bulletSpeed=0;
    /// <summary>
    /// 넉백
    /// </summary>
    public float knockBack=0;
    /// <summary>
    /// 탄총량
    /// </summary>
    public int remainShellsTotal=0;
    /// <summary>
    /// 탄창크기
    /// </summary>
    public int shellBoxSize=0;
    /// <summary>
    /// 재장전속도
    /// </summary>
    public float reloadTime=0;
    /// <summary>
    /// 집탄율
    /// </summary>
    public float shellCollectionRate=0;
    /// <summary>
    /// 폭발반경
    /// </summary>
    public float explosionRange=0;

    /// <summary>
    /// 데미지
    /// </summary>
    public float damage_Per = 0;
    /// <summary>
    /// 발사속도
    /// </summary>
    public float attackSpeed_Per = 0;
    /// <summary>
    /// 탄속
    /// </summary>
    public float bulletSpeed_Per = 0;
    /// <summary>
    /// 넉백
    /// </summary>
    public float knockBack_Per = 0;
    /// <summary>
    /// 탄총량
    /// </summary>
    public int remainShellsTotal_Per = 0;
    /// <summary>
    /// 탄창크기
    /// </summary>
    public int shellBoxSize_Per = 0;
    /// <summary>
    /// 재장전속도
    /// </summary>
    public float reloadTime_Per = 0;
    /// <summary>
    /// 집탄율
    /// </summary>
    public float shellCollectionRate_Per = 0;
    /// <summary>
    /// 폭발반경
    /// </summary>
    public float explosionRange_Per = 0;
}
public class Shooter : CharactorStatus
{
    public Shooter()
    {
        damage = 1;
        attackSpeed = 1;
    }
}
