﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JW_PlayerCollider : MonoBehaviour
{
    GameObject player;
    JW_PlayerAction playerScript;
    // Start is called before the first frame update
    void Start()
    {
        player = transform.parent.parent.gameObject;
        playerScript = player.GetComponent<JW_PlayerAction>();
    }

    // Update is called once per frame

    private void OnTriggerEnter(Collider other)
    {
        playerScript.AutoItemGet(other);
        JW_PlayerAction.CallPlayerHpUI();

        if (other.gameObject.layer == LayerMask.NameToLayer("Bullet") || other.gameObject.tag == "EnemyBullet")
        {
            JW_PlayerAction.isOnTrigger = true;
            playerScript. observedTriggerObjects.Add(other.gameObject);
            //트리거 일정범위안에 불릿이 들어와있고 그 불릿이 콜라이더에 닿기전에 회피 액션을 취하면 반격한다
            playerScript.observedOBJ++;
            playerScript.attackedPos = other.transform.position;
            //피격정보와 객체 전달

        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Bullet") || other.gameObject.tag == "EnemyBullet")
        {
            playerScript.observedTriggerObjects.Remove(other.gameObject);
            //트리거 일정범위안에 불릿이 들어와있고 그 불릿이 콜라이더에 닿기전에 회피 액션을 취하면 반격한다
            playerScript.observedOBJ--;
        }
        //부딫힌 불랫도 리스트에서 지워주어야 한다.
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Bullet") || other.gameObject.tag == "EnemyBullet")
        {
            playerScript.observedTriggerObjects.Remove(other.gameObject);
            playerScript.observedOBJ--;
        }
    }
}
