﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum MouseLeftClickState
{
    Click,
    Hold,
    EndClick
}
public class Item
{
    public GameManager GM;
    public MonoBehaviour mono;
    public GameObject player;
    public JW_PlayerAction playerScript;
    public Animator playerAnim;
    public AudioSource playerAudioSource;
    public GameObject playerBody;

    //필요요소
    /// <summary>
    /// 데미지
    /// </summary>
    public float damage;
    /// <summary>
    /// 발사속도
    /// </summary>
    public float attackSpeed;
    /// <summary>
    /// 탄속
    /// </summary>
    public float bulletSpeed;
    /// <summary>
    /// 넉백
    /// </summary>
    public float knockBack;
    /// <summary>
    /// 탄총량
    /// </summary>
    public int remainShellsTotal;
    /// <summary>
    /// 탄창크기
    /// </summary>
    public int shellBoxSize;
    /// <summary>
    /// 재장전속도
    /// </summary>
    public float reloadTime;
    /// <summary>
    /// 집탄율
    /// </summary>
    public float shellCollectionRate;
    /// <summary>
    /// 폭발반경
    /// </summary>
    public float explosionRange;
    /// <summary>
    /// 함수가 실행되었을때 시간지점
    /// </summary>
    public float savedTime1;
    /// <summary>
    /// 함수가 실행되기전에 시간지점
    /// </summary>
    public float currentTime;
    public float savedTime2;
    /// <summary>
    /// 현재 탄잔량
    /// </summary>
    public int remainShells;
    /// <summary>
    /// 현재 탄창안의 탄잔량
    /// </summary>
    public int remainShellsInBox;

    public Item()
    {
        GM = GameObject.Find("GameManager").GetComponent<GameManager>();

        if (!player)
        {
            player = GameObject.Find("Player");
        }
        if (!mono)
        {
            mono = GM;
        }
        if (!playerScript)
        {
            playerScript = player.GetComponent<JW_PlayerAction>();
        }
        if (!playerAnim)
        {
            playerAnim = player.GetComponent<Animator>();
        }
        if (!playerAudioSource)
        {
            playerAudioSource = player.GetComponent<AudioSource>();
        }
        if (!playerBody)
        {
            playerBody = player.transform.Find("PlayerBody").gameObject;
        }
        
    }


}
#region 무기
public class Weapon:Item
{


    /// <summary>
    /// 무기 스프라이트
    /// </summary>
    public Sprite weaponSprite;
    /// <summary>
    /// 총알 스프라이트
    /// </summary>
    public Sprite bulletSprite;
    /// <summary>
    /// 발사음
    /// </summary>
    public AudioClip fireBlast;
    /// <summary>
    /// 장전음
    /// </summary>
    public AudioClip reloadSound;
    /// <summary>
    /// 빈총소리
    /// </summary>
    public AudioClip clickingSound;

    public bool isBulletMaked=false;
    public bool reloadDisturb=false;

    public UnityEngine.UI.Image reloadImg;
    public GameObject playerUI;
    public Transform firepos;
    public GameObject weapon;
    public GameObject bullet;
    public JW_Bullet bulletStatus;

    public MouseLeftClickState clickState;

    public List<GameObject> bulletlist = new List<GameObject>();







    public Weapon()
    {
        WeaponInitalize();
#region 개별파라미터
        damage = 1;
        attackSpeed = 1f;
        bulletSpeed = 1;
        knockBack = 1;
        shellBoxSize = 6;
        reloadTime = 1f;
        shellCollectionRate = 1;

        remainShellsTotal = 999;//기본딱총은 무한
        remainShells = remainShellsTotal;
        remainShellsInBox = shellBoxSize;

        weaponSprite = Resources.Load<Sprite>("Sprites/WeaponSprite/Pistol");
        bulletSprite = Resources.Load<Sprite>("Sprites/WeaponSprite/Bullet");
        fireBlast = Resources.Load<AudioClip>("Sound/GunSound/Hand Gun 1");
        reloadSound= Resources.Load<AudioClip>("Sound/GunSound/ReloadWeapons7");
        clickingSound= Resources.Load<AudioClip>("Sound/GunSound/Clicking");
        weapon = Resources.Load<GameObject>("Weapons/LaserPistol");
        //weapon.SetActive(false);
        //weapon.transform.parent = playerScript.rightHand.transform;
        //Vector3 dir;
        //dir.x = 8.3f;
        //dir.y = 15.9f;
        //dir.z = -1.9f;
        //Vector3 rot;
        //rot.x = -17.945f;
        //rot.y = -81.371f;
        //rot.z = 267.038f;
#endregion
    }
    /// <summary>
    /// 공통파라미터
    /// </summary>
    public void WeaponInitalize()
    {

        if (bullet == null)
        {
            bullet = Resources.Load<GameObject>("bullet");
        }
        if (!firepos)
        {
            firepos = player.transform.Find("PlayerBody").Find("FirePos");
        }
        if (!playerUI)
        {
            playerUI = player.transform.Find("PlayerCanvas").gameObject;
        }



        reloadImg = playerUI.transform.Find("Img_ReloadGage").GetComponent<UnityEngine.UI.Image>();
        reloadImg.gameObject.SetActive(false);
        savedTime1 = Time.time;
        savedTime2 = Time.time;


    }
    public void DefaultAttackParamater()
    {
        CheckGunPlayMotion();


        if (remainShellsInBox == 0)
        {
            //발사가 안되어 딸각이는소리

            playerAudioSource.clip = clickingSound;
            playerAudioSource.volume = 0.5f;
            playerAudioSource.pitch = 1;
            playerAudioSource.Play();
            return;
        }
        currentTime = Time.time;
        isBulletMaked = false;
        reloadDisturb = true;
    }
    public void CheckGunPlayMotion()
    {
        playerAnim.runtimeAnimatorController = GM.cache_Anims[0];
    }

    public virtual void Attack(MouseLeftClickState clickState)
    {
        DefaultAttackParamater();
        switch (clickState)
        {
            case MouseLeftClickState.Click:
                if ((currentTime - savedTime1 < attackSpeed)||remainShellsInBox<=0)
                    return;

                for (int i = 0; i < bulletlist.Count; i++)
                {
                    if (!bulletlist[i].activeSelf)
                    {
                        GameObject temp = bulletlist[i];
                        temp.SetActive(true);



                        //Vector3 tempvec= GameManager.crosshair.position - firepos.position;
                        //tempvec.y = 0;
                        //포인터를 향해서 방향을 틀고싶다.
                        temp.transform.position = firepos.transform.position;
                        temp.transform.forward = GameManager.crosshair.position - firepos.position;
                        //좌우로 일정량만큼 흔들어주고싶다
                        float angle = Random.Range(-shellCollectionRate, shellCollectionRate);
                        temp.transform.rotation = Quaternion.AngleAxis(angle, temp.transform.up) * temp.transform.rotation;
                        isBulletMaked = true;
                        break;
                    }
                }
                if (!isBulletMaked)
                {
                    //탄환생성
                    GameObject temp = GameObject.Instantiate(bullet);
                    //탄환 위치
                    bulletlist.Add(temp);

                    //탄환의 데미지
                    JW_Bullet shell = temp.GetComponent<JW_Bullet>();
                    shell.damage = damage;


                    temp.transform.position = firepos.transform.position;
                    temp.transform.forward = GameManager.crosshair.position - firepos.position;
                    float angle = Random.Range(-shellCollectionRate, shellCollectionRate);
                    temp.transform.rotation = Quaternion.AngleAxis(angle, temp.transform.up) * temp.transform.rotation;


                    //탄환속도
                    //총알프리펩 설정에 접근한다(?)
                    //그 프리펩의 컴포넌트가저온다




                    JW_Bullet tempbullet = temp.GetComponent<JW_Bullet>();
                    tempbullet.state = JW_Bullet.ShellType.dumbdumb;
                    tempbullet.bulletSpeed = 1;
                }



                //꺼져있는 탄환이 없으면 생성

                remainShells--;
                remainShellsInBox--;
                playerScript.CallShellAmountInBox();
                savedTime1 = currentTime;//시간조정
                playerAnim.SetTrigger("Fire");
                


                break;
            case MouseLeftClickState.EndClick:

                break;
            case MouseLeftClickState.Hold:
                if ((currentTime - savedTime1 < attackSpeed)||remainShellsInBox<=0)
                    return;
                for (int i = 0; i < bulletlist.Count; i++)
                {
                    if (!bulletlist[i].activeSelf)
                    {
                        GameObject temp = bulletlist[i];
                        temp.SetActive(true);



                        //Vector3 tempvec= GameManager.crosshair.position - firepos.position;
                        //tempvec.y = 0;
                        //포인터를 향해서 방향을 틀고싶다.
                        temp.transform.position = firepos.transform.position;
                        temp.transform.forward = GameManager.crosshair.position - firepos.position;
                        //좌우로 일정량만큼 흔들어주고싶다
                        float angle = Random.Range(-shellCollectionRate, shellCollectionRate);
                        temp.transform.rotation = Quaternion.AngleAxis(angle, temp.transform.up) * temp.transform.rotation;
                        isBulletMaked = true;
                        break;
                    }
                }
                if (!isBulletMaked)
                {
                    //탄환생성
                    GameObject temp = GameObject.Instantiate(bullet);
                    //탄환 위치
                    bulletlist.Add(temp);

                    //탄환의 데미지
                    JW_Bullet shell = temp.GetComponent<JW_Bullet>();
                    shell.damage = damage;//총알에 데미지 값 전달


                    temp.transform.position = firepos.transform.position;
                    temp.transform.forward = GameManager.crosshair.position - firepos.position;
                    float angle = Random.Range(-shellCollectionRate, shellCollectionRate);
                    temp.transform.rotation = Quaternion.AngleAxis(angle, temp.transform.up) * temp.transform.rotation;


                    //탄환속도
                    //총알프리펩 설정에 접근한다(?)
                    //그 프리펩의 컴포넌트가저온다




                    JW_Bullet tempbullet = temp.GetComponent<JW_Bullet>();
                    tempbullet.state = JW_Bullet.ShellType.dumbdumb;
                    tempbullet.bulletSpeed = 1;
                }



                //꺼져있는 탄환이 없으면 생성
                remainShells--;
                remainShellsInBox--;
                playerScript.CallShellAmountInBox();
                savedTime1 = currentTime;//시간조정

                playerAnim.SetTrigger("Fire");

                break;

        }
        //빵야소리
        
    }
        //시작하면 리로드 진행게이지 표시
        //공격버튼이 눌렸고 공격이 가능하면 리로드가 취소된다.(나중에 구현)
        //게이지가 끝나면 탄창이 채워진다
        //궁금증하나 자식클래스에서 이 클래스를 작동시키면 재장전값은 인스턴스를 따르는가 아니면 이 클래스를 따르는가?
    public virtual void Reload()
    {
        currentTime = Time.time;
        if (currentTime - savedTime2 < reloadTime)
            return;
        reloadDisturb = false;
        mono.StartCoroutine(ReloadFnc());
        savedTime2 = currentTime;

    }
    public virtual IEnumerator ReloadFnc()
    {
        reloadImg.gameObject.SetActive(true);
        reloadImg.fillAmount = 0;
        while (true)
        {
            //공격을 감지하면 리턴으로 끝내고싶다
            //공격을 감지하는 방법
            reloadImg.fillAmount += Time.deltaTime / reloadTime;
            yield return new WaitForEndOfFrame();
            if (reloadImg.fillAmount >= 1)
            {
                playerAudioSource.clip = reloadSound;
                playerAudioSource.volume = 0.5f;
                playerAudioSource.time=0.4f;
                playerAudioSource.pitch = 1;
                playerAudioSource.Play();

                reloadImg.gameObject.SetActive(false);
                if (shellBoxSize < remainShells)
                {
                    remainShellsInBox = shellBoxSize;
                }
                else
                {
                    remainShellsInBox = remainShells;
                }

                playerScript.CallShellAmountInBox();

                break;
            }
            else if (reloadDisturb)
            {
                reloadImg.gameObject.SetActive(false);
                break;
            }
            
        }
        //재장전 ui 게이지

    }


}
public class Weapon_Pistol:Weapon
{
    public Weapon_Pistol()
    {
#region 개별파라미터
        damage = 1;
        attackSpeed = 1f;
        bulletSpeed = 1;
        knockBack = 1;
        shellBoxSize = 6;
        reloadTime = 1f;
        shellCollectionRate = 1;

        remainShellsTotal = 999;//기본딱총은 무한
        remainShells = remainShellsTotal;
        remainShellsInBox = shellBoxSize;

        bulletStatus = this.bullet.GetComponent<JW_Bullet>();
        bulletStatus.state = JW_Bullet.ShellType.dumbdumb;

        weaponSprite = Resources.Load<Sprite>("Sprites/WeaponSprite/Pistol");
        bulletSprite = Resources.Load<Sprite>("Sprites/WeaponSprite/Bullet");
        fireBlast = Resources.Load<AudioClip>("Sound/GunSound/Hand Gun 1");
        reloadSound = Resources.Load<AudioClip>("Sound/GunSound/ReloadWeapons7");
        weapon=GameObject.Instantiate( Resources.Load<GameObject>("Weapons/LaserPistol"));
        weapon.SetActive(false);
        weapon.transform.parent = playerScript.rightHand.transform;
        Vector3 dir;
        dir.x = -0.09f;
        dir.y = 0.035f;
        dir.z = 0.025f;
        Vector3 rot;
        rot.x = 5.578f;
        rot.y = 260f;
        rot.z = -68.5f;
        Vector3 scale = Vector3.one;
        weapon.transform.localPosition = dir;
        weapon.transform.localEulerAngles = rot;
        weapon.transform.localScale = scale;
#endregion
    }




    
    public override void Attack(MouseLeftClickState clickState)
    {
        DefaultAttackParamater();
        switch (clickState)
        {
            case MouseLeftClickState.Click:
                if ((currentTime - savedTime1 < attackSpeed) || remainShellsInBox <= 0)
                    return;

                for (int i = 0; i < bulletlist.Count; i++)
                {
                    if (!bulletlist[i].activeSelf)
                    {
                        GameObject temp = bulletlist[i];
                        temp.SetActive(true);



                        //Vector3 tempvec= GameManager.crosshair.position - firepos.position;
                        //tempvec.y = 0;
                        //포인터를 향해서 방향을 틀고싶다.
                        temp.transform.position = firepos.transform.position;
                        temp.transform.forward = GameManager.crosshair.position - firepos.position;
                        //좌우로 일정량만큼 흔들어주고싶다
                        float angle = Random.Range(-shellCollectionRate, shellCollectionRate);
                        temp.transform.rotation = Quaternion.AngleAxis(angle, temp.transform.up) * temp.transform.rotation;
                        isBulletMaked = true;
                        break;
                    }
                }
                if (!isBulletMaked)
                {
                    //탄환생성
                    GameObject temp = GameObject.Instantiate(bullet);
                    //탄환 위치
                    bulletlist.Add(temp);

                    //탄환의 데미지
                    JW_Bullet shell = temp.GetComponent<JW_Bullet>();
                    shell.damage = damage;


                    temp.transform.position = firepos.transform.position;
                    temp.transform.forward = GameManager.crosshair.position - firepos.position;
                    float angle = Random.Range(-shellCollectionRate, shellCollectionRate);
                    temp.transform.rotation = Quaternion.AngleAxis(angle, temp.transform.up) * temp.transform.rotation;


                    //탄환속도
                    //총알프리펩 설정에 접근한다(?)
                    //그 프리펩의 컴포넌트가저온다




                    JW_Bullet tempbullet = temp.GetComponent<JW_Bullet>();
                    tempbullet.state = JW_Bullet.ShellType.dumbdumb;
                    tempbullet.bulletSpeed = 1;
                }



                //꺼져있는 탄환이 없으면 생성

                remainShells--;
                remainShellsInBox--;
                playerScript.CallShellAmountInBox();
                savedTime1 = currentTime;//시간조정
                playerAudioSource.clip = fireBlast;
                playerAudioSource.volume = 0.5f;
                playerAudioSource.time = 0;
                playerAudioSource.pitch = 1;
                playerAudioSource.Play();

                playerAnim.SetTrigger("Fire");


                break;
            case MouseLeftClickState.EndClick:

                break;
            case MouseLeftClickState.Hold:
                if ((currentTime - savedTime1 < attackSpeed) || remainShellsInBox <= 0)
                    return;
                for (int i = 0; i < bulletlist.Count; i++)
                {
                    if (!bulletlist[i].activeSelf)
                    {
                        GameObject temp = bulletlist[i];
                        temp.SetActive(true);



                        //Vector3 tempvec= GameManager.crosshair.position - firepos.position;
                        //tempvec.y = 0;
                        //포인터를 향해서 방향을 틀고싶다.
                        temp.transform.position = firepos.transform.position;
                        temp.transform.forward = GameManager.crosshair.position - firepos.position;
                        //좌우로 일정량만큼 흔들어주고싶다
                        float angle = Random.Range(-shellCollectionRate, shellCollectionRate);
                        temp.transform.rotation = Quaternion.AngleAxis(angle, temp.transform.up) * temp.transform.rotation;
                        isBulletMaked = true;
                        break;
                    }
                }
                if (!isBulletMaked)
                {
                    //탄환생성
                    GameObject temp = GameObject.Instantiate(bullet);
                    //탄환 위치
                    bulletlist.Add(temp);

                    //탄환의 데미지
                    JW_Bullet shell = temp.GetComponent<JW_Bullet>();
                    shell.damage = damage;//총알에 데미지 값 전달


                    temp.transform.position = firepos.transform.position;
                    temp.transform.forward = GameManager.crosshair.position - firepos.position;
                    float angle = Random.Range(-shellCollectionRate, shellCollectionRate);
                    temp.transform.rotation = Quaternion.AngleAxis(angle, temp.transform.up) * temp.transform.rotation;


                    //탄환속도
                    //총알프리펩 설정에 접근한다(?)
                    //그 프리펩의 컴포넌트가저온다




                    JW_Bullet tempbullet = temp.GetComponent<JW_Bullet>();
                    tempbullet.state = JW_Bullet.ShellType.dumbdumb;
                    tempbullet.bulletSpeed = 1;
                }



                //꺼져있는 탄환이 없으면 생성
                remainShells--;
                remainShellsInBox--;
                playerScript.CallShellAmountInBox();
                savedTime1 = currentTime;//시간조정
                playerAudioSource.clip = fireBlast;
                playerAudioSource.volume = 0.5f;
                playerAudioSource.time = 0;
                playerAudioSource.pitch = 1;
                playerAudioSource.Play();
                playerAnim.SetTrigger("Fire");

                break;

        }

    }

}
public class Weapon_Granadeluncher : Weapon
{
    public Weapon_Granadeluncher()
    {
#region 개별파라미터
        damage = 5;
        attackSpeed = 1f;
        bulletSpeed = 1;
        knockBack = 1;
        shellBoxSize = 3;
        reloadTime = 3f;
        shellCollectionRate = 1;
        explosionRange = 3;

        remainShellsTotal = 20;
        remainShells = remainShellsTotal;
        remainShellsInBox = shellBoxSize;
        bulletStatus = bullet.GetComponent<JW_Bullet>();
        bulletStatus.state = JW_Bullet.ShellType.Grenade;
        bulletStatus.explosionRange = explosionRange;

        weaponSprite = Resources.Load<Sprite>("Sprites/WeaponSprite/GranadeLuncher");
        bulletSprite = Resources.Load<Sprite>("Sprites/WeaponSprite/Bullet");
        fireBlast = Resources.Load<AudioClip>("Sound/GunSound/Hand Gun 1");
        reloadSound = Resources.Load<AudioClip>("Sound/GunSound/ReloadWeapons7");
        weapon = GameObject.Instantiate(Resources.Load<GameObject>("Weapons/BombLauncher"));
        weapon.SetActive(false);
        weapon.transform.parent = playerScript.rightHand.transform;
        Vector3 dir;
        dir.x = -0.09f;
        dir.y = 0.035f;
        dir.z = 0.025f;
        Vector3 rot;
        rot.x = 5.578f;
        rot.y = 260f;
        rot.z = -68.5f;
        Vector3 scale=Vector3.one;
        weapon.transform.localPosition = dir;
        weapon.transform.localEulerAngles = rot;
        weapon.transform.localScale = scale;
#endregion




    }





    public override void Attack(MouseLeftClickState clickState)
    {

        DefaultAttackParamater();

        switch (clickState)
        {
            case MouseLeftClickState.Click:
                if ((currentTime - savedTime1 < attackSpeed)||remainShellsInBox<=0)
                    return;
                for (int i = 0; i < bulletlist.Count; i++)
                {
                    if (!bulletlist[i].activeSelf)
                    {
                        GameObject temp = bulletlist[i];
                        temp.SetActive(true);



                        //Vector3 tempvec= GameManager.crosshair.position - firepos.position;
                        //tempvec.y = 0;
                        //포인터를 향해서 방향을 틀고싶다.
                        temp.transform.position = firepos.transform.position;
                        temp.transform.forward = GameManager.crosshair.position - firepos.position;
                        //좌우로 일정량만큼 흔들어주고싶다
                        float angle = Random.Range(-shellCollectionRate, shellCollectionRate);
                        temp.transform.rotation = Quaternion.AngleAxis(angle, temp.transform.up) * temp.transform.rotation;
                        isBulletMaked = true;
                        break;
                    }
                }
                if (!isBulletMaked)
                {
                    //탄환생성
                    GameObject temp = GameObject.Instantiate(bullet);
                    //탄환 위치
                    bulletlist.Add(temp);

                    //탄환의 데미지
                    JW_Bullet shell = temp.GetComponent<JW_Bullet>();
                    shell.damage = damage;


                    temp.transform.position = firepos.transform.position;
                    temp.transform.forward = GameManager.crosshair.position - firepos.position;
                    float angle = Random.Range(-shellCollectionRate, shellCollectionRate);
                    temp.transform.rotation = Quaternion.AngleAxis(angle, temp.transform.up) * temp.transform.rotation;
                    shell.damage = 0;
                    shell.damage1 = damage;

                    //탄환속도
                    //총알프리펩 설정에 접근한다(?)
                    //그 프리펩의 컴포넌트가저온다




                    JW_Bullet tempbullet = temp.GetComponent<JW_Bullet>();
                    tempbullet.state = JW_Bullet.ShellType.Grenade;
                    tempbullet.bulletSpeed = 1;
                }



                //꺼져있는 탄환이 없으면 생성


                savedTime1 = currentTime;//시간조정
                remainShells--;
                remainShellsInBox--;
                playerScript.CallShellAmountInBox();
                playerAudioSource.clip = fireBlast;
                playerAudioSource.volume = 0.5f;
                playerAudioSource.time = 0;
                playerAudioSource.pitch = 1;
                playerAudioSource.Play();
                playerAnim.SetTrigger("Fire");

                break;
            case MouseLeftClickState.EndClick:

                break;
            case MouseLeftClickState.Hold:
                if ((currentTime - savedTime1 < attackSpeed)||remainShellsInBox<=0)
                    return;
                for (int i = 0; i < bulletlist.Count; i++)
                {
                    if (!bulletlist[i].activeSelf)
                    {
                        GameObject temp = bulletlist[i];
                        temp.SetActive(true);



                        //Vector3 tempvec= GameManager.crosshair.position - firepos.position;
                        //tempvec.y = 0;
                        //포인터를 향해서 방향을 틀고싶다.
                        temp.transform.position = firepos.transform.position;
                        temp.transform.forward = GameManager.crosshair.position - firepos.position;
                        //좌우로 일정량만큼 흔들어주고싶다
                        float angle = Random.Range(-shellCollectionRate, shellCollectionRate);
                        temp.transform.rotation = Quaternion.AngleAxis(angle, temp.transform.up) * temp.transform.rotation;
                        isBulletMaked = true;
                        break;
                    }
                }
                if (!isBulletMaked)
                {
                    //탄환생성
                    GameObject temp = GameObject.Instantiate(bullet);
                    //탄환 위치
                    bulletlist.Add(temp);

                    //탄환의 데미지
                    JW_Bullet shell = temp.GetComponent<JW_Bullet>();
                    shell.damage = damage;//총알에 데미지 값 전달


                    temp.transform.position = firepos.transform.position;
                    temp.transform.forward = GameManager.crosshair.position - firepos.position;
                    float angle = Random.Range(-shellCollectionRate, shellCollectionRate);
                    temp.transform.rotation = Quaternion.AngleAxis(angle, temp.transform.up) * temp.transform.rotation;
                    shell.damage = 0;
                    shell.damage1 = damage;

                    //탄환속도
                    //총알프리펩 설정에 접근한다(?)
                    //그 프리펩의 컴포넌트가저온다




                    JW_Bullet tempbullet = temp.GetComponent<JW_Bullet>();
                    tempbullet.state = JW_Bullet.ShellType.Grenade;
                    tempbullet.bulletSpeed = 1;
                }



                //꺼져있는 탄환이 없으면 생성


                savedTime1 = currentTime;//시간조정
                remainShells--;
                remainShellsInBox--;
                playerScript.CallShellAmountInBox();
                playerAudioSource.clip = fireBlast;
                playerAudioSource.volume = 0.5f;
                playerAudioSource.time = 0;
                playerAudioSource.pitch = 1;
                playerAudioSource.Play();
                playerAnim.SetTrigger("Fire");
                break;

        }

    }

}
public class Weapon_Minigun : Weapon
{
    ParticleSystem pBullet;
    ParticleSystem.MainModule psm;
    ParticleSystem.ShapeModule pss;
    JW_CollisionInstancer shell;
    public Weapon_Minigun()
    {
#region 개별파라미터
        damage = 1;
        attackSpeed = 0.1f;
        bulletSpeed = 1;
        knockBack = 1;
        shellBoxSize = 50;
        reloadTime = 1;
        shellCollectionRate = 4;

        remainShellsTotal = 999;
        remainShells = remainShellsTotal;
        remainShellsInBox = shellBoxSize;

        pBullet = GameObject.Instantiate(Resources.Load<ParticleSystem>("FX/P_Minigun"));
        shell = pBullet.gameObject.GetComponent<JW_CollisionInstancer>();
        psm = pBullet.main;
        pss = pBullet.shape;

        weaponSprite = Resources.Load<Sprite>("Sprites/WeaponSprite/MiniGun");
        bulletSprite = Resources.Load<Sprite>("Sprites/WeaponSprite/Bullet");
        fireBlast = Resources.Load<AudioClip>("Sound/GunSound/Hand Gun 1");
        reloadSound = Resources.Load<AudioClip>("Sound/GunSound/ReloadWeapons7");
        weapon = GameObject.Instantiate(Resources.Load<GameObject>("Weapons/ElectricSMG"));
        weapon.SetActive(false);
        weapon.transform.parent = playerScript.rightHand.transform;
        Vector3 dir;
        dir.x = -0.09f;
        dir.y = 0.035f;
        dir.z = 0.025f;
        Vector3 rot;
        rot.x = 5.578f;
        rot.y = 260f;
        rot.z = -68.5f;
        Vector3 scale = Vector3.one;
        weapon.transform.localPosition = dir;
        weapon.transform.localEulerAngles = rot;
        weapon.transform.localScale = scale;
#endregion

    }
    public override void Attack(MouseLeftClickState clickState)
    {
        shell.damage = damage;//뎀지
        DefaultAttackParamater();
        switch (clickState)
        {
            case MouseLeftClickState.Click:
                if ((currentTime - savedTime1 < attackSpeed)||remainShellsInBox<=0)
                    return;

                    //탄환생성
                //탄환 위치
                pBullet.gameObject.transform.position = firepos.position;
                pBullet.gameObject.transform.rotation = firepos.rotation;
                psm.startSpeed = 20 * bulletSpeed;
                pss.angle = shellCollectionRate;
                pBullet.Emit(1);

                        //탄환의 데미지



                        //탄환속도
                        //총알프리펩 설정에 접근한다(?)
                        //그 프리펩의 컴포넌트가저온다








                        //꺼져있는 탄환이 없으면 생성


                        savedTime1 = currentTime;//시간조정
                remainShells--;
                remainShellsInBox--;
                playerScript.CallShellAmountInBox();


                playerAudioSource.clip = fireBlast;
                playerAudioSource.volume = 0.5f;
                playerAudioSource.time = 0;
                playerAudioSource.pitch = 1;
                playerAudioSource.Play();
                playerAnim.SetTrigger("Fire");
                break;
            case MouseLeftClickState.EndClick:

                break;
            case MouseLeftClickState.Hold:
                if ((currentTime - savedTime1 < attackSpeed)||remainShellsInBox<=0)
                    return;

                //탄환생성
                //탄환속도
                //총알프리펩 설정에 접근한다(?)
                //그 프리펩의 컴포넌트가저온다

                pBullet.gameObject.transform.position = firepos.position;
                pBullet.gameObject.transform.rotation = firepos.rotation;
                psm.startSpeed = 20 * bulletSpeed;
                pBullet.Emit(1);




                //꺼져있는 탄환이 없으면 생성


                savedTime1 = currentTime;//시간조정
                remainShells--;
                remainShellsInBox--;
                playerScript.CallShellAmountInBox();

                playerAudioSource.clip = fireBlast;
                playerAudioSource.volume = 0.5f;
                playerAudioSource.time = 0;
                playerAudioSource.pitch = 1;
                playerAudioSource.Play();
                playerAnim.SetTrigger("Fire");
                break;

        }
        //총소리

    }

}
#endregion
public enum MouseRightClickState
{
    Click,
    Hold,
    EndClick
}
public class M2Action : Item
{
    //필요요소

    /// <summary>
    /// 구르기속도
    /// </summary>
    public float coolDown;
    /// <summary>
    /// 구르기 배율
    /// </summary>
    public float MulSpeed;
    /// <summary>
    /// 구르기 지속 시간
    /// </summary>
    public float rollDuration;



    bool threshold = false;
    public Collider playerCollider;


    //비공통변수?
    bool threshold2 = false;
    SkinnedMeshRenderer mr;
    ParticleSystem slashFx;
    Collider slashCol;
    AfterImage[] afterImages;
    int afterImageCnt = 10;
    float createTime = 0;
    ParticleSystem ps;
    bool stopRolling = false;
    bool isRolling { get { return ActionStateStatic.IsAction[0]; }set { ActionStateStatic.IsAction[0]=value; } }
    bool isSliding { get { return ActionStateStatic.IsAction[1]; } set { ActionStateStatic.IsAction[1] = value; } }
    AudioClip roll;
    AudioClip slash;
    public M2Action()
    {
        coolDown = 0.1f;
        MulSpeed = 1.2f;
        rollDuration = 0.9f;


        savedTime1 = Time.time;
        

        //비공통변수
        mr = playerBody.transform.Find("Body").GetComponent<SkinnedMeshRenderer>();
        afterImages = new AfterImage[afterImageCnt];
        GameObject a= GameObject.Instantiate(Resources.Load<GameObject>("Fx/Slash"));
        a.transform.parent = playerBody.transform;
        Vector3 tempv = Vector3.zero;
        tempv.y = 0.5f;
        a.transform.localPosition = tempv;
        a.transform.rotation = player.transform.rotation;
        slashFx = a.GetComponent<ParticleSystem>();
        slashCol = a.GetComponent<Collider>();
        slashCol.enabled = false;
        GameObject tempa = GameObject.Instantiate( Resources.Load<GameObject>("Fx/Distortion"));
        ps=tempa.GetComponent<ParticleSystem>();
        ParticleSystem.MainModule mm = ps.main;
        mm.startSize = 1;
        tempa.transform.parent = playerBody.transform;
        tempa.transform.position = Vector3.zero;
        ActionStateStatic.IsAction[1] = isRolling;
        roll = Resources.Load<AudioClip>("Sound/Roll");
        slash = Resources.Load<AudioClip>("Sound/Slash");
            

    }



    public MouseRightClickState clickState;
    public virtual void M2Act(MouseRightClickState clickState)
    {

        

        //if (!JW_PlayerAction.invincibleSelf && playerScript.observedTriggerObjects.Count > 0)
        //{
        //    //이상태에서 회피를 입력하면 반격가능 마우스 방향을 향해서 슬래시공격

        //    playerScript.exclamationMark.Emit(1);
        //    mono.StartCoroutine(CounterAttack(1));
            

        //}
        if (!JW_PlayerAction.invincibleSelf && playerScript.observedOBJ>0)
        {
            //이상태에서 회피를 입력하면 반격가능 마우스 방향을 향해서 슬래시공격

            playerScript.exclamationMark.Emit(1);
            mono.StartCoroutine(CounterAttack(1));

            MonoBehaviour.print(playerScript.observedOBJ);
        }
        stopRolling = false;

        currentTime = Time.time;

        switch (clickState)
        {
            case MouseRightClickState.Click:
                if (currentTime - savedTime1 < coolDown || threshold)
                    return;
                

                    mono.StartCoroutine(RollingDodge());


                    savedTime1 = currentTime;//시간조정



                break;
            case MouseRightClickState.EndClick:

                break;
            case MouseRightClickState.Hold:
                if (currentTime - savedTime1 < coolDown || threshold)
                    return;
                mono.StartCoroutine(RollingDodge());
                savedTime1 = currentTime;//시간조정


                break;

        }

    }
    public void Attack(MouseLeftClickState clickState)
    {
        
        switch (clickState)
        {

            case MouseLeftClickState.Click:
                playerAudioSource.clip = slash;
                playerAudioSource.volume = 0.5f;
                playerAudioSource.time = 0;
                playerAudioSource.pitch = 1f;
                playerAudioSource.Play();
                stopRolling = true;
                //방향은 마우스방향
                player.transform.forward = GameManager.crosshair.position - player.transform.position;
                playerAnim.SetTrigger("Slash");
                slashFx.Emit(1);
                slashCol.enabled = true;



                break;
            case MouseLeftClickState.EndClick:
                break;
            case MouseLeftClickState.Hold:
                slashCol.enabled = false;



                break;

        }

    }
    void SlideStartCo(MouseLeftClickState clickState)
    {
        switch (clickState)
        {

            case MouseLeftClickState.Click:
                if (threshold2)
                    return;
                    mono.StartCoroutine(Sliding());

                break;
            case MouseLeftClickState.EndClick:
                break;
            case MouseLeftClickState.Hold:

                break;

        }

    }
    IEnumerator Sliding()
    {
        threshold2 = true;
        playerAnim.SetTrigger("Slide");
        isSliding = true;
        yield return new WaitForSeconds(1.067f);
        isSliding = false;
        threshold2 = false;
    }
    IEnumerator RollingDodge()
    {

        playerAudioSource.clip = roll;
        playerAudioSource.volume = 0.5f;
        playerAudioSource.time = 0;
        playerAudioSource.pitch = 0.5f;
        playerAudioSource.Play();
        playerAnim.runtimeAnimatorController = GM.cache_Anims[1];
        //이게 실행중일때 플레이어는 마우스를 향해 틀지않는다
        //방향고정 플레이어에서 막기
        //여기에서 플레이어의 스크립트에 간섭하기
        playerAnim.SetTrigger("Roll");
        JW_PlayerAction.invincible = true;
        float tempspd = playerScript.moveSpeed * MulSpeed;
        threshold = true;
        yield return new WaitForFixedUpdate();
        playerScript.stopPlayerRot = true;
        playerScript.stopPlayerMove = true;
        Vector3 dir = playerScript.playerMoveDir;
        if (dir == Vector3.zero)
        {
            dir = player.transform.forward;
        }
        else
        {

            player.transform.forward = playerScript.playerMoveDir;
        }
        float time=0;
        JW_PlayerAction.Mydel tempdel = playerScript.Attack;
        playerScript.Attack = SlideStartCo;
        
        while (rollDuration > time)
        {
            isRolling = true;

            //이거하는동안 잔상만들거임
            AfterImgCreate();
            player.transform.position += dir * tempspd * Time.deltaTime;
            time += Time.deltaTime;

            if (stopRolling)
            {
                //카운터어택시 발동
                //일정시간후 이동속도가 돌아온다
                //한 방향으로 이동한다. 이때 이동 조작은멈춘다.
                threshold = false;
                JW_PlayerAction.invincible = false;
                isRolling = false;
                playerScript.Attack = tempdel;
                yield break;
            }
            yield return new WaitForFixedUpdate();

        }
        //일정시간후 이동속도가 돌아온다
        //한 방향으로 이동한다. 이때 이동 조작은멈춘다.
        threshold = false;
        JW_PlayerAction.invincible = false;
        playerScript.Attack = tempdel;
        isRolling = false;
        //닷지중 무적

    }
    IEnumerator CounterAttack(float waitDuration)
    {
        float temptime=0;
        
        while (waitDuration>temptime)
        {
            temptime += Time.deltaTime;

            //공격버튼을 누르면 반격행동 발사행동을 봉인하고 반격행동
            playerScript.Attack = Attack;
            yield return new WaitForEndOfFrame();
            
        }
        slashCol.enabled = false;
        playerScript.Attack = playerScript.weapon.Attack;
        
        //끝나면 원래대로
    }

    //잔상
    void AfterImgCreate()
    {
        //이 함수는 구르는동안 계속 호출
        //크리에이트타임에 지정된숫자를 넘어가면 생성
        if (Time.time- createTime >rollDuration/afterImageCnt+0.01f)
        {
            //AfterImage a = new AfterImage();
            //a.go = new GameObject("test");
            //MeshRenderer m = a.go.AddComponent<MeshRenderer>();
            //MeshFilter mf = a.go.AddComponent<MeshFilter>();
            //mr.BakeMesh(mf.mesh);
            //a.go.transform.position = mr.gameObject.transform.position;
            //a.go.transform.rotation = mr.gameObject.transform.rotation;
            //m.material = mr.material;
            //a.createdTime = Time.time; AfterImage a = new AfterImage();
            GameObject a = new GameObject("test");
            MeshRenderer m = a.AddComponent<MeshRenderer>();
            MeshFilter mf = a.AddComponent<MeshFilter>();
            JW_FadeOut fadeOut = a.AddComponent<JW_FadeOut>();
            fadeOut.mr = mr;
            fadeOut.mf = mf;
            mr.BakeMesh(mf.mesh);
            a.transform.position = mr.gameObject.transform.position;
            a.transform.rotation = mr.gameObject.transform.rotation;
            m.material = Resources.Load<Material>("Materials/AfterImage");






            /*로직
             *초기값으로 게임오브젝트를 생성한다.
             * 
             * 일정시간마다
             * 베이크매쉬를 생성하고 채워넣는다.
             * 생성된 매쉬는 일정시간후 없어진다.
             * 
            */


            createTime = Time.time;
        }
    }


}

#region 패시브아이템
class Passiveitem : Item
{

}


#endregion
#region 즉석소모성아이템(탄약,코인,체력 등등)

class consumableItems : Item
{

}
class ShallBox : consumableItems
{
    //이 아이템을 주우면 들고있는 무기들의 탄창안의 총알이 총량의 20퍼센트 만큼 찬다

    public ShallBox()
    {
        foreach(Weapon a in playerScript.weaponList)
        {
            a.remainShells = Mathf.Min(a.remainShells + (int)(a.remainShellsTotal * 0.2f),remainShellsTotal);
            
        }
    }
}
#endregion
public class AfterImage
{
    public GameObject go;
    public float createdTime;
    public MeshFilter mf;
    public MeshRenderer m;
}

