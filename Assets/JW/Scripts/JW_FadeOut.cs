﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JW_FadeOut : MonoBehaviour
{
    public float lifeTime=1;
    public SkinnedMeshRenderer mr;
    public MeshFilter mf;
    public Color color;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

        //메쉬가 흐려진다
        lifeTime -= 0.1f;
        if (lifeTime < 0)
        {
            OnDestroy();
        }
    }
    private void OnDestroy()
    {
        Destroy(gameObject);
    }
}
