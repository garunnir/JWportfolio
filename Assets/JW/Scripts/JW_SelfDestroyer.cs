﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JW_SelfDestroyer : MonoBehaviour
{
    public float lifeTime=2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, lifeTime);
    }
}
