# 3d탑뷰 로그라이크"가제:엔터더로그라이크"

"[엔터 더 건전](https://www.youtube.com/watch?v=hCVoVeAfnWw)"을 타겟으로 한 게임프로젝트

4인 팀 작업, 팀장의 역할을 수행.

플레이어 캐릭터 조작에 관련한 ui와 움직임과 공격, 무기아이템의 작동스크립트 등등 조작에 관련된 모든 기능을 전반제작.





## 어필 목표

- 유니티 프로그램을 얼마나 다룰 줄 알며 알고 있는지를 어필.
- 스크립트를 활용해서 어떤 기능이든 구현해 낼 수 있다는것을 어필. 
- 위 프로젝트에 포함되지 않았으나 4차 산업인재육성과정으로 학습한 주제는 ML-Agent(학습형인공지능), VR,AR.(모바일AR앱, VR 개발경험 있음)

## 스크립트 소개 

> 경로:Assets/JW/Scripts
>
> 캐릭터 스크립트 전반

#### JW_Item.cs

- 아이템을 총망라한 스크립트 아이템 후속추가를 염두에 두고 만든 클래스이기에 상속개념으로 패시브와 액티브 등으로 유형구분을 하였고 공통으로 공유하는 특성을 상위 하이어라키에 올려 반복작업을 줄였다. 해당 아이템의 소리와 모델등의 고유한 요소를 각 아이템 아래에 넣어 가독성과 독립성을 확고히 정립 했다. JW_PlayerAction.cs의 delegate chain인 Attack과 연결되어 있어서 함수 실행시 JW_Item.cs의 각 아이템의 오버라이드 Attack 함수가 동작하게 된다.(Attack은 마우스 왼쪽클릭으로 작동)

  

  ``` c#
    public override void Attack(MouseLeftClickState clickState)
      {
          DefaultAttackParamater();
          switch (clickState)
          {
              case MouseLeftClickState.Click:
  
  ```

  [참조링크](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/JW/Scripts/JW_Item.cs#L242)

  

  아이템을 모아놓는다는 개념으로 작성하였기에 Monobehaviour는 상속받지 않았다. 때문에 코루틴을 활용하기 위해 인스턴스할때 JW_PlayerAction.cs코드에 접근해 모노비헤비어를 가져와서 코루틴을 작동시켰다.

  [코루틴위치](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/JW/Scripts/JW_Item.cs#L1114)

  [Monobehaviour 를 받는 위치](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/JW/Scripts/JW_Item.cs#L87)
  
- 리소스 절약을 위해 총알은 타격시 제거되는것이 아닌 사라졌다가 재사용하게 만들었다.

  ```c#
  if ((currentTime - savedTime1 < attackSpeed)||remainShellsInBox<=0)
                      return;
  
                  for (int i = 0; i < bulletlist.Count; i++)
                  {
                      if (!bulletlist[i].activeSelf)
                      {
                          GameObject temp = bulletlist[i];
                          temp.SetActive(true);
  
  
  
                          //Vector3 tempvec= GameManager.crosshair.position - firepos.position;
                          //tempvec.y = 0;
                          //포인터를 향해서 방향을 틀고싶다.
                          temp.transform.position = firepos.transform.position;
                          temp.transform.forward = GameManager.crosshair.position - firepos.position;
                          //좌우로 일정량만큼 흔들어주고싶다
                          float angle = Random.Range(-shellCollectionRate, shellCollectionRate);
                          temp.transform.rotation = Quaternion.AngleAxis(angle, temp.transform.up) * temp.transform.rotation;
                          isBulletMaked = true;
                          break;
                      }
                  }
                  if (!isBulletMaked)
                  {
                      //탄환생성
                      GameObject temp = GameObject.Instantiate(bullet);
                      //탄환 위치
                      bulletlist.Add(temp);
  
                      //탄환의 데미지
                      JW_Bullet shell = temp.GetComponent<JW_Bullet>();
                      shell.damage = damage;
  
  
                      temp.transform.position = firepos.transform.position;
                      temp.transform.forward = GameManager.crosshair.position - firepos.position;
                      float angle = Random.Range(-shellCollectionRate, shellCollectionRate);
                      temp.transform.rotation = Quaternion.AngleAxis(angle, temp.transform.up) * temp.transform.rotation;
  
  
                      //탄환속도
                      //총알프리펩 설정에 접근한다(?)
                      //그 프리펩의 컴포넌트가저온다
  
  
  
  
                      JW_Bullet tempbullet = temp.GetComponent<JW_Bullet>();
                      tempbullet.state = JW_Bullet.ShellType.dumbdumb;
                      tempbullet.bulletSpeed = 1;
  
  ```

  [리스트위치](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/JW/Scripts/JW_Item.cs#L148)

  [코드 위치](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/JW/Scripts/JW_Item.cs#L242)

  [끄는 코드는 JW_bullet.cs](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/JW/Scripts/JW_Bullet.cs#L139)





#### JW_Bullet.cs

- 이 스크립트로 폭발탄,관통탄 등을 한번에 묶어 유니티 인스팩터 창에서 사용하기 쉽게 만드는것에 중점을 두고 제작하였다. 이 스크립트뿐아니라 다른스크립트들도 인스팩터 창에 올리는 것만으로 쉽게 사용할 수 있게 하는 것에 초점을 두고 만들었다.

```c#
    public enum ShellType
    {
        dumbdumb,
        Pierce,
        Grenade,
        Explosion,
        DamageOnly
    }

```

```c#
    void Update()
    {
        switch (state)
        {
            case ShellType.dumbdumb:
                ShellMove();
                break;

            case ShellType.Grenade:
                ShellMove();
                break;

            case ShellType.Pierce:
                ShellMove();
                break;

            case ShellType.Explosion:

                break;
        }

        
    }

```

[참고](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/JW/Scripts/JW_Bullet.cs#L36)



#### JW_PlayerAction.cs

- 델리게이트를 활용해 상황에 따라 공격버튼을 누를 때 작동하는것이 달라지게끔 하였다.

``` c#
 public delegate void Mydel(MouseLeftClickState state);
 public Mydel Attack;

```

[참고링크](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/JW/Scripts/JW_PlayerAction.cs#L82)

```C#
    private void PlayerWeaponSwitch(int intiger)
    {
        if (weaponList.Count == weaponIndex)
        {
            weaponIndex = 0;
        }
        if(weaponIndex == -1)
        {
            weaponIndex = weaponList.Count-1;
        }
        if (intiger == 0)
        {
            weapon.weapon.SetActive(false);
            weapon = weaponList[weaponIndex];
            Attack = weapon.Attack;
            weapon.weapon.SetActive(true);
            weaponIndex++;
            CallShellAmountInBox();
        }
        else if (intiger == 1)
        {
            weapon.weapon.SetActive(false);
            weapon = weaponList[weaponIndex];
            Attack = weapon.Attack;
            weapon.weapon.SetActive(true);
            weaponIndex--;
            CallShellAmountInBox();
        }
    }

```

[대리자 Attack()은 무기가 변경 될 때마다 재지정된다.](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/JW/Scripts/JW_PlayerAction.cs#L304)

``` c#
  private void PlayerKeyAction()//키액션 제어
    {
        if (stopPlayerAllActions)
            return;

        if (Input.GetButtonUp("Fire1"))
        {
            Attack(MouseLeftClickState.Click);
        }
        else if (Input.GetButton("Fire1"))
        {
            Attack(MouseLeftClickState.Hold);
            //마우스 지속
        }
        else if (Input.GetButtonDown("Fire1"))
        {
            Attack(MouseLeftClickState.Click);
        }

```

[키조작은 컨트롤러의 변경으로 바뀔 수 있기에 별도의 함수로 묶어놓은 메서드안에서 Attack()으로 작동한다. ](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/JW/Scripts/JW_PlayerAction.cs#L367)

- hp에 대한 ui는 지속적 호출로 퍼포먼스 저하가 되는것을 방지하기 위해 변경이 있을때만 호출하게 만들었다.

  ``` c#
   public static void CallPlayerHpUI()
      {
          for (int i = 0; i < playerHpUI.transform.childCount; i++)
              if (i < HP)
              {
                  playerHpUI.transform.GetChild(i).gameObject.SetActive(true);
              }
              else
              {
                  playerHpUI.transform.GetChild(i).gameObject.SetActive(false);
              }
      }
  
  
  ```

  

  [참조](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/JW/Scripts/JW_PlayerAction.cs#L286) 

  [사용예](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/JW/Scripts/JW_PlayerAction.cs#L212)

-  

### GameManager.cs

>  [경로 /Asset](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/GameManager.cs)

이 스크립트는 항상 씬내에 존재하는것을 가정하고 계획한 스크립트다. 따라서 편의성을 위해 싱글톤으로 만들었다.

```c#
    void Awake()
    {
        #region 싱글톤
        if (null == instance)
        {
            //이 클래스 인스턴스가 탄생했을 때 전역변수 instance에 게임매니저 인스턴스가 담겨있지 않다면, 자신을 넣어준다.
            instance = this;

            //씬 전환이 되더라도 파괴되지 않게 한다.
            //gameObject만으로도 이 스크립트가 컴포넌트로서 붙어있는 Hierarchy상의 게임오브젝트라는 뜻이지만, 
            //나는 헷갈림 방지를 위해 this를 붙여주기도 한다.
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            //만약 씬 이동이 되었는데 그 씬에도 Hierarchy에 GameMgr이 존재할 수도 있다.
            //그럴 경우엔 이전 씬에서 사용하던 인스턴스를 계속 사용해주는 경우가 많은 것 같다.
            //그래서 이미 전역변수인 instance에 인스턴스가 존재한다면 자신(새로운 씬의 GameMgr)을 삭제해준다.
            Destroy(this.gameObject);
        }
        #endregion
    }

```



프레임이 떨어지는것을 방지하기 위해서 느린작업을 미리 캐싱 하였다.

``` c#
        cache_AudioClips = new AudioClip[3];
        cache_AudioClips[0]= Resources.Load<AudioClip>("Sound/GunSound/Hit 1");
        cache_AudioClips[1]= Resources.Load<AudioClip>("Sound/GunSound/Hit 2"); 
        cache_AudioClips[2]= Resources.Load<AudioClip>("Sound/GunSound/Hit 3");
        cache_bulletImpacts = new ParticleSystem[1];
        cache_bulletImpacts[0]= Resources.Load<ParticleSystem>("Fx/P_Minigun_Impact");
        cache_Anims = new RuntimeAnimatorController[2];
        cache_Anims[0]= Resources.Load<RuntimeAnimatorController>("Anim/GunPlayAnim");
        cache_Anims[1]= Resources.Load<RuntimeAnimatorController>("Anim/PlayerAnim");

```

[참조링크](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/GameManager.cs#L128)

게임중에 항상 표시할 마우스 포인터의 기능메서드도 만들었다.

```c#
    private void CrossHair(CrossHairState state)
    {
        switch (state)
        {
            case CrossHairState.OnTheScreen:
                //스크린만을 받는다.
                //크로스해어의 위치 높이 0 마우스포지션
                plane = new Plane(Vector3.up, Vector3.zero);
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                float rayDistance;
                if (plane.Raycast(ray, out rayDistance))

```

[참조링크](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/GameManager.cs#L178)

## Unity Animator

![예제](https://gitlab.com/garunnir/JWportfolio/-/raw/master/ReadMeImg/1.PNG)

![예제](https://gitlab.com/garunnir/JWportfolio/-/raw/master/ReadMeImg/2.PNG)

- 블랜드 트리를 활용하여 자연스러운 무브모션을 제작

  ```c#
      private void PlayerMove()
      {
  
          // 대화가 진행중이면 플레이어 이동을 막는다.
          if(GM.isAction == true||stopPlayerMove)
              return;
  
          playerMoveDir.x = Input.GetAxis("Horizontal");
          playerMoveDir.z = Input.GetAxis("Vertical");
          playerMoveDir.y = 0;
          anim.SetFloat("Speed", playerMoveDir.magnitude);
          anim.SetFloat("forward", Vector3.Dot(playerMoveDir, playerBody.transform.forward));
          anim.SetFloat("right", Vector3.Dot(playerMoveDir, playerBody.transform.right));
              transform.position += playerMoveDir.normalized * moveSpeed * Time.deltaTime;
          
      }
  
  ```

  [speed,foward,right 세가지 변수로 제어한다.](https://gitlab.com/garunnir/JWportfolio/-/blob/master/Assets/JW/Scripts/JW_PlayerAction.cs#L486)

  

후속추가설명: 파티클시스템 오디오소스 코드 외적 유니티의 기능들

